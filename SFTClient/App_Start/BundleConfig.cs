﻿using System.Web;
using System.Web.Optimization;

namespace SFTClient
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/vendor/jquery-1.8.3.js",
                      "~/vendor/jquery.blockUI.js"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                      "~/vendor/modernizr-2.6.2.js"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/vendor").Include(
                      "~/vendor/plugins.js",
                      "~/vendor/icheck/icheck.js",
                      "~/vendor/keyboard/js/jquery.keyboard.js"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                      "~/Scripts/app/app.js",
                      "~/Scripts/app/util/ui.js",
                      "~/Scripts/app/util/common.js",
                      "~/Scripts/app/util/pagenav.js",
                      "~/Scripts/app/util/subpagehnd.js",
                      "~/Scripts/app/main.js"
           
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/normalize.css",
                      "~/Content/main.css",
                      "~/vendor/icheck/skins/all.css",
                      "~/vendor/keyboard/css/keyboard.css",
                      "~/Content/base.css"));

           
            BundleTable.EnableOptimizations = false;
        }
    }
}
