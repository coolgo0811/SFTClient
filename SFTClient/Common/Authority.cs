﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;

namespace SFTClient
{
    public class Authority : System.Web.UI.Page
    {
        public bool CheckSessionExist()
        {
            string ssProjName = Const.SESSION_PROJ_NAME;
            string ssNodeName = Const.SESSION_NODE_NAME;
            string ssWaPath = Const.SESSION_WAPATH;
            if ( Session[ssProjName] != null && Session[ssNodeName] != null && Session[ssWaPath] != null )
                return true;

            return false;
        }
    }
}