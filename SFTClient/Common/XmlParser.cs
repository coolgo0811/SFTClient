﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;

namespace SFTClient
{
    public class XmlParser
    {
        public static string Serialize( object obj )
        {
            XmlSerializer ser = new XmlSerializer( obj.GetType() );
            using ( Utf8StringWriter writer = new Utf8StringWriter() )
            {
                using ( XmlWriter xmlWriter = XmlWriter.Create( writer, new XmlWriterSettings { Indent = false } ) )
                {
                    ser.Serialize( xmlWriter, obj );
                }
                return writer.ToString(); 
            }
        }

        public static T Deserialize<T>( string s )
        {
            XmlDocument xdoc = new XmlDocument();

            try
            {
                xdoc.LoadXml( s );
                XmlNodeReader reader = new XmlNodeReader( xdoc.DocumentElement );
                XmlSerializer ser = new XmlSerializer( typeof( T ) );
                object obj = ser.Deserialize( reader );

                return ( T ) obj;
            }
            catch
            {
                return default( T );
            }
        }
    }

    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
}