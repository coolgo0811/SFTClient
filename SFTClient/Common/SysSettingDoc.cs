﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.IO;


namespace SFTClient
{
    public static class SysSettingDoc
    {
        public static XmlDocument doc = new XmlDocument();

        public static string LocalIP;
        public static string ServerIP;
        public static string ServerPort;
        public static string ServerTimeout;
        public static string UserName;
        public static string Password;
        public static string AutoLogoutTime;
        public static string CompanyId;
        public static string Equipment;
        public static string EquipDataCollection;
        public static string ServerFolder;

        public static bool Load( string filePath )
        {
            try
            {
                doc.Load( filePath );

                XmlElement root = doc["SystemSetting"];
                LocalIP = root.SelectSingleNode( "LocalIP" ).InnerText;
                ServerIP = root.SelectSingleNode( "ServerIP" ).InnerText;
                ServerPort = root.SelectSingleNode( "ServerPort" ).InnerText;
                ServerTimeout = root.SelectSingleNode( "ServerTimeout" ).InnerText;
                UserName = root.SelectSingleNode( "UserName" ).InnerText;
                Password = root.SelectSingleNode( "Password" ).InnerText;
                AutoLogoutTime = root.SelectSingleNode( "AutoLogoutTime" ).InnerText;
                CompanyId = root.SelectSingleNode( "CompanyId" ).InnerText;
                Equipment = root.SelectSingleNode( "Equipment" ).InnerText;
                EquipDataCollection = root.SelectSingleNode( "EquipDataCollection" ).InnerText;
                ServerFolder = root.SelectSingleNode( "ServerFolder" ).InnerText;
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static bool Save( string filePath )
        {
            try
            {
                doc.Load( filePath );

                XmlElement root = doc["SystemSetting"];
                root.SelectSingleNode( "LocalIP" ).InnerText = LocalIP;
                root.SelectSingleNode( "ServerIP" ).InnerText = ServerIP;
                root.SelectSingleNode( "ServerPort" ).InnerText = ServerPort;
                root.SelectSingleNode( "ServerTimeout" ).InnerText = ServerTimeout;
                root.SelectSingleNode( "UserName" ).InnerText = UserName;
                root.SelectSingleNode( "Password" ).InnerText = Password;
                root.SelectSingleNode( "AutoLogoutTime" ).InnerText = AutoLogoutTime;
                root.SelectSingleNode( "CompanyId" ).InnerText = CompanyId;
                root.SelectSingleNode( "Equipment" ).InnerText = Equipment;
                root.SelectSingleNode( "EquipDataCollection" ).InnerText = EquipDataCollection;
                root.SelectSingleNode( "ServerFolder" ).InnerText = ServerFolder;

                doc.Save( filePath );
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static string GetParamValue( string paramName )
        {
            string paramValue = string.Empty;
            if ( string.IsNullOrEmpty( paramName ) == false )
            {
                XmlElement root = doc["SystemSetting"];
                paramValue = root.SelectSingleNode( paramName ).InnerText;
            }

            return paramValue;
        }
    }
}