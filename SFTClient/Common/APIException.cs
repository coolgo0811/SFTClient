﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Net.Http;

namespace SFTClient
{
    public class APIException
    {
        public static HttpResponseException NotFound( string reason )
        {
            return CreateHttpResponseException( reason, HttpStatusCode.NotFound );
        }

        public static HttpResponseException BadRequest( string reason )
        {
            return CreateHttpResponseException( reason, HttpStatusCode.BadRequest );
        }

        private static HttpResponseException CreateHttpResponseException( string reason, HttpStatusCode code )
        {
            HttpResponseMessage response = new HttpResponseMessage
            {
                StatusCode = code,
                Content = new StringContent( reason )
            };
            throw new HttpResponseException( response );
        }
    }
}