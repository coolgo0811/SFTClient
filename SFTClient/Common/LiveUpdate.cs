﻿using Microsoft.Web.Administration;
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;

namespace SFTClient
{
    public class LiveUpdate
    {
        public const string SFTCLIENT_ZIP_FILE = "SFTClient.zip";


        private static Logger logger = NLog.LogManager.GetLogger( LoggerName.LIVEUPDATE );

        public static bool CheckVersion( ref string version, ref string filePath )
        {
            try
            {
                //logger.Info( "Check Version !" );
                Version currentVer, tmpVer;
                // load version
                Assembly assembly = Assembly.LoadFrom( System.Web.HttpContext.Current.Server.MapPath( @"~\bin\" ) + "SFTClient.dll" );
                currentVer = assembly.GetName().Version;

                string servFolder;

                if ( string.IsNullOrEmpty( MyConfig.ServerIP ) )
                {
                    logger.Info( "ServerIP setting is null !" );
                    return false;
                }

                servFolder = string.Format( "http://{0}/LiveUpdate", MyConfig.ServerIP );

                List<string> versionList = new List<string>();
                HttpWebRequest request = ( HttpWebRequest )WebRequest.Create( servFolder );
                using ( HttpWebResponse response = ( HttpWebResponse )request.GetResponse() )
                {
                    using ( StreamReader reader = new StreamReader( response.GetResponseStream() ) )
                    {
                        string html = reader.ReadToEnd();
                        Regex regex = new Regex( "<a href=\\\"\\S+\">(?<name>\\S+)</a>", RegexOptions.IgnoreCase );
                        MatchCollection matches = regex.Matches( html );
                        if ( matches.Count > 0 )
                        {
                            foreach ( Match match in matches )
                            {
                                if ( match.Success )
                                {
                                    if ( Version.TryParse( match.Groups["name"].ToString(), out tmpVer ) )
                                        versionList.Add( match.Groups["name"].ToString() );
                                }
                            }
                        }
                    }
                }

                if ( versionList.Count == 0 )  // no any version folder
                    return false;

                string versionName = versionList[versionList.Count - 1];
                Version newestVer = Version.Parse( versionName );

                if ( newestVer.CompareTo( currentVer ) <= 0 )   // current version is greater or equal to updated version
                    return false;

                // no file in newest folder
                filePath = servFolder.TrimEnd( '/' );
                filePath = string.Format( "{0}/{1}/{2}", filePath, versionName, SFTCLIENT_ZIP_FILE );
                version = newestVer.ToString();

                logger.Info( string.Format( "找到新版本 {0} !", version ) );
            }
            catch ( Exception ex )
            {
                logger.Error( ex.ToString() );
                return false;
            }
            return true;
        }

        public static bool DoUpdate( string version, string filePath )
        {
            /*ServerManager server = new ServerManager();
            Site site = server.Sites.FirstOrDefault( s => s.Name == IIS.DEFAULT_WEBSITE );
            if ( site == null )
            {
                logger.Error( string.Format( "IIS 站台{0}不存在，請聯繫系統管理員 !", IIS.DEFAULT_WEBSITE ) );
                return false;
            }*/
            string sftDirPath, backupDirPath, zipFilePath, zipDirPath, tmpDirPath;

            sftDirPath = Path.Combine( IIS.LOCALHOST_PATH, IIS.SFTCLIENT_DIRECTORY_NAME );
            backupDirPath = Path.Combine( IIS.LOCALHOST_PATH, IIS.SFTCLIENT_DIRECTORY_NAME + "_back" );
            zipFilePath = Path.Combine( IIS.LOCALHOST_PATH, SFTCLIENT_ZIP_FILE );
            zipDirPath = Path.Combine( IIS.LOCALHOST_PATH, IIS.SFTCLIENT_DIRECTORY_NAME + "_" );
            try
            {
                if ( string.IsNullOrEmpty( version ) || string.IsNullOrEmpty( filePath ) )
                    return false;

                logger.Info( "---LiveUpdate Start---" );

                // stop IIS
                /*logger.Info( "IIS Stop..." );
                site.Stop();

                if ( site.State != ObjectState.Stopped )
                {
                    logger.Error( "IIS停止失敗，請聯繫系統管理員 !" );
                    return false;
                }*/

                // download newest version
                logger.Info( string.Format( "Download Newest Version {0}...", filePath ) );
                WebClient wc = new WebClient();
                wc.DownloadFile( filePath, zipFilePath );
                logger.Info( "Download Newest Version Success!" );

                // generate backup folder to avoid unexpected exception
                logger.Info( "Backup SFTClient Folder..." );
                DirectoryCopy( sftDirPath, backupDirPath, true );
                logger.Info( "Backup SFTClient Folder Success!" );

                // delete SFTClient folder
                logger.Info( "Delete SFTClient Folder..." );
                foreach ( string path in Directory.GetDirectories( sftDirPath ) )
                {
                    if ( path.Contains( Const.CONFIG_DIRECTORY_NAME ) == false && path.Contains( Const.LOGS_DIRECTORY_NAME ) == false )
                        Directory.Delete( path, true );
                }
                foreach ( string path in Directory.GetFiles( sftDirPath ) )
                {
                    File.Delete( path );
                }
                logger.Info( "Delete SFTClient Folder Success!" );

                //unzip
                logger.Info( "Unzip file..." );
                ZipFile.ExtractToDirectory( zipFilePath, zipDirPath );
                logger.Info( "Unzip file Success!" );

                // copy new SFTClient folder to original path
                logger.Info( "Updating..." );
                foreach ( string path in Directory.GetDirectories( zipDirPath ) )
                {
                    if ( path.Contains( Const.CONFIG_DIRECTORY_NAME ) == false && path.Contains( Const.LOGS_DIRECTORY_NAME ) == false )
                    {
                        tmpDirPath = Path.Combine( sftDirPath, Path.GetFileName( path ) );
                        DirectoryCopy( path, tmpDirPath, true );
                    }
                }
                foreach ( string path in Directory.GetFiles( zipDirPath ) )
                {
                    tmpDirPath = Path.Combine( sftDirPath, Path.GetFileName( path ) );
                    File.Copy( path, tmpDirPath, true );
                }
                logger.Info( "Updating Success!" );

                // delete backup folder
                Directory.Delete( backupDirPath, true );
                // delete unzip folder
                Directory.Delete( zipDirPath, true );
                // delete zip file
                File.Delete( zipFilePath );

                // clear cache in IIS
                logger.Info( "Clear Cache..." );
                foreach ( DictionaryEntry entry in HttpContext.Current.Cache )
                {
                    HttpContext.Current.Cache.Remove( ( string )entry.Key );
                }
                logger.Info( "Clear Cache Success!" );

                //restart IIS
                /*logger.Info( "IIS Restart..." );
                site.Start();
                if ( site.State != ObjectState.Started )
                {
                    logger.Error( "IIS啟動失敗，請聯繫系統管理員 !" );
                    return false;
                }*/

                logger.Info( "LiveUpdate End---" );
            }
            catch ( Exception ex )
            {
                // recover to original version
                RecoverVersion();

                logger.Error( "LiveUpdate 更新異常 ! {0}", ex.ToString() );
            }
            return true;
        }


        public static bool RecoverVersion()
        {
            string sftDirPath, backupDirPath;

            sftDirPath = Path.Combine( IIS.LOCALHOST_PATH, IIS.SFTCLIENT_DIRECTORY_NAME );
            backupDirPath = Path.Combine( IIS.LOCALHOST_PATH, IIS.SFTCLIENT_DIRECTORY_NAME + "_back" );

            try
            {
                if ( Directory.Exists( backupDirPath ) == false )
                    return false;

                if ( Directory.Exists( sftDirPath ) )
                {
                    Directory.Delete( sftDirPath, true );
                }

                // backup folder rename to original folder
                Directory.Move( backupDirPath, sftDirPath );
            }
            catch ( Exception ex )
            {
                logger.Error( "RecoverVersion 更新異常 ! {0}", ex.ToString() );
                return false;
            }

            return true;
        }

        private static void DirectoryCopy( string sourceDirName, string destDirName, bool copySubDirs )
        {
            DirectoryInfo dir = new DirectoryInfo( sourceDirName );
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the source directory does not exist, throw an exception.
            if ( !dir.Exists )
            {
                throw new DirectoryNotFoundException( "Source directory does not exist or could not be found: " + sourceDirName );
            }

            // If the destination directory does not exist, create it.
            if ( !Directory.Exists( destDirName ) )
            {
                Directory.CreateDirectory( destDirName );
            }

            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach ( FileInfo file in files )
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine( destDirName, file.Name );
                // Copy the file.
                file.CopyTo( temppath, true );
            }

            // If copySubDirs is true, copy the subdirectories.
            if ( copySubDirs )
            {
                foreach ( DirectoryInfo subdir in dirs )
                {
                    // Create the subdirectory.
                    string temppath = Path.Combine( destDirName, subdir.Name );
                    // Copy the subdirectories.
                    DirectoryCopy( subdir.FullName, temppath, copySubDirs );
                }
            }
        }
    }
}