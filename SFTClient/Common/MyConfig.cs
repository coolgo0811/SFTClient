﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.IO;


namespace SFTClient
{
    public static class MyConfig
    {
        public static XmlDocument doc = new XmlDocument();

        public static string LocalIP;
        public static string ServerIP;
        public static string ServerPort;
        public static string ServerTimeout;
        public static string UserName;
        public static string Password;
        public static string AutoLogoutTime;
        public static string CompanyId;
        public static string Equipment;
        public static int EquipDataCollection;
        public static string SystemModel;

        public static bool LiveUpdate;
        public static bool EquipTrackinoutModel;

        public static bool Load( string filePath )
        {
            try
            {
                doc.Load( filePath );

                XmlElement root = doc["SystemSetting"];
                if ( root.SelectSingleNode( "LocalIP" ) != null )
                    LocalIP = root.SelectSingleNode( "LocalIP" ).InnerText;
                if ( root.SelectSingleNode( "ServerIP" ) != null )
                    ServerIP = root.SelectSingleNode( "ServerIP" ).InnerText;
                if ( root.SelectSingleNode( "ServerPort" ) != null )
                    ServerPort = root.SelectSingleNode( "ServerPort" ).InnerText;
                if ( root.SelectSingleNode( "ServerTimeout" ) != null )
                    ServerTimeout = root.SelectSingleNode( "ServerTimeout" ).InnerText;
                if ( root.SelectSingleNode( "UserName" ) != null )
                    UserName = root.SelectSingleNode( "UserName" ).InnerText;
                if ( root.SelectSingleNode( "Password" ) != null )
                    Password = root.SelectSingleNode( "Password" ).InnerText;
                if ( root.SelectSingleNode( "AutoLogoutTime" ) != null )
                    AutoLogoutTime = root.SelectSingleNode( "AutoLogoutTime" ).InnerText;
                if ( root.SelectSingleNode( "CompanyId" ) != null )
                    CompanyId = root.SelectSingleNode( "CompanyId" ).InnerText;
                if ( root.SelectSingleNode( "Equipment" ) != null )
                    Equipment = root.SelectSingleNode( "Equipment" ).InnerText;
                if ( root.SelectSingleNode( "EquipDataCollection" ) != null )
                    EquipDataCollection = Parser.ParseInt( root.SelectSingleNode( "EquipDataCollection" ).InnerText );
                if ( root.SelectSingleNode( "SystemModel" ) != null )
                    SystemModel = root.SelectSingleNode( "SystemModel" ).InnerText;

                if ( root.SelectSingleNode( "LiveUpdate" ) != null )
                    LiveUpdate = Parser.ParseBoolean( root.SelectSingleNode( "LiveUpdate" ).InnerText );
                if ( root.SelectSingleNode( "EquipTrackinoutModel" ) != null )
                    EquipTrackinoutModel = Parser.ParseBoolean( root.SelectSingleNode( "EquipTrackinoutModel" ).InnerText );
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static bool Save( string filePath )
        {
            try
            {
                doc.Load( filePath );

                XmlElement root = doc["SystemSetting"];
                
                if ( root.SelectSingleNode( "LocalIP" ) == null )
                {
                    XmlElement element = doc.CreateElement( "LocalIP" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "LocalIP" ).InnerText = LocalIP;

                if ( root.SelectSingleNode( "ServerIP" ) == null )
                {
                    XmlElement element = doc.CreateElement( "ServerIP" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "ServerIP" ).InnerText = ServerIP;

                if ( root.SelectSingleNode( "ServerPort" ) == null )
                {
                    XmlElement element = doc.CreateElement( "ServerPort" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "ServerPort" ).InnerText = ServerPort;

                if ( root.SelectSingleNode( "ServerTimeout" ) == null )
                {
                    XmlElement element = doc.CreateElement( "ServerTimeout" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "ServerTimeout" ).InnerText = ServerTimeout;

                if ( root.SelectSingleNode( "UserName" ) == null )
                {
                    XmlElement element = doc.CreateElement( "UserName" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "UserName" ).InnerText = UserName;

                if ( root.SelectSingleNode( "Password" ) == null )
                {
                    XmlElement element = doc.CreateElement( "Password" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "Password" ).InnerText = Password;

                if ( root.SelectSingleNode( "AutoLogoutTime" ) == null )
                {
                    XmlElement element = doc.CreateElement( "AutoLogoutTime" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "AutoLogoutTime" ).InnerText = AutoLogoutTime;

                if ( root.SelectSingleNode( "CompanyId" ) == null )
                {
                    XmlElement element = doc.CreateElement( "CompanyId" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "CompanyId" ).InnerText = CompanyId;

                if ( root.SelectSingleNode( "Equipment" ) == null )
                {
                    XmlElement element = doc.CreateElement( "Equipment" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "Equipment" ).InnerText = Equipment;

                if ( root.SelectSingleNode( "EquipDataCollection" ) == null )
                {
                    XmlElement element = doc.CreateElement( "EquipDataCollection" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "EquipDataCollection" ).InnerText = EquipDataCollection.ToString();

                if ( root.SelectSingleNode( "SystemModel" ) == null )
                {
                    XmlElement element = doc.CreateElement( "SystemModel" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "SystemModel" ).InnerText = SystemModel;

                if ( root.SelectSingleNode( "LiveUpdate" ) == null )
                {
                    XmlElement element = doc.CreateElement( "LiveUpdate" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "LiveUpdate" ).InnerText = LiveUpdate.ToString();

                if ( root.SelectSingleNode( "EquipTrackinoutModel" ) == null )
                {
                    XmlElement element = doc.CreateElement( "EquipTrackinoutModel" );
                    root.AppendChild( element );
                }
                root.SelectSingleNode( "EquipTrackinoutModel" ).InnerText = EquipTrackinoutModel.ToString();

                doc.Save( filePath );
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static string GetParamValue( string paramName )
        {
            string paramValue = string.Empty;
            if ( string.IsNullOrEmpty( paramName ) == false )
            {
                XmlElement root = doc["SystemSetting"];
                if ( root.SelectSingleNode( paramName ) != null )
                    paramValue = root.SelectSingleNode( paramName ).InnerText;
            }

            return paramValue;
        }
    }
}