﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace SFTClient
{
	public class Parser
	{
		public static int ParseInt( string value, int defVal )
		{
            CultureInfo culture = new CultureInfo("en-US");
            int output = ( int.TryParse( value, NumberStyles.AllowDecimalPoint, culture.NumberFormat, out output ) ) ? output : defVal;

			return output;
		}

		public static int ParseInt( string value )
		{
			return ParseInt( value, 0 );
		}

		public static bool ParseBoolean( string value )
		{
			return "true".Equals( value, StringComparison.OrdinalIgnoreCase );
		}

		/// <summary>
		/// Parse Float with string value
		/// </summary>
		/// <param name="value">string value</param>
		/// <returns>return default value if fail</returns>
		public static float ParseFloat( string value, float defVal )
		{
			float output;
			return ( float.TryParse( value, out output ) ) ? output : defVal;
		}

		/// <summary>
		/// Parse Float with string value
		/// </summary>
		/// <param name="value">string value</param>
		/// <returns>return float.NaN if fail</returns>
		public static float ParseFloat( string value )
		{
			return ParseFloat( value, float.NaN );
		}

		public static double ParseDouble( string value, double defVal )
		{
			double output;
			return ( double.TryParse( value, out output ) ) ? output : defVal;
		}

		public static double ParseDouble( string value )
		{
			return ParseDouble( value, 0 );
		}

		public static object ParseEnum( string value, Enum defValue )
		{
			if ( string.IsNullOrEmpty( value ) )
				return defValue;

			Type etype = defValue.GetType();
			string[] names = Enum.GetNames( etype );

			for ( int i = names.Length - 1 ; i >= 0 ; i-- )
			{
				if ( names[i].Equals( value, StringComparison.OrdinalIgnoreCase ) )
					return Enum.GetValues( etype ).GetValue( i );
			}

			return defValue;
		}

		public static T ParseEnum<T>( string value, T defValue )
		{
			try
			{
				return ( T ) Enum.Parse( typeof( T ), value, true );
			}
			catch
			{
				return defValue;
			}
		}

		public static Enum ParseEnum( Type enumType, string value )
		{
			if ( enumType == null )
				return null;

			try
			{
				return ( Enum ) Enum.Parse( enumType, value, true );
			}
			catch
			{
				return null;
			}
		}

		public static Enum ParseEnum( string enumTypeString, string value )
		{
			return ParseEnum( Type.GetType( enumTypeString, false ), value );
		}

		public static DateTime ParseDateTime( string value, DateTime defVal )
		{
			DateTime output = ( DateTime.TryParse( value, out output ) ) ? output : defVal;
			return output;
		}

		public static DateTime ParseExactDateTime( string value, string format, DateTime defVal )
		{
			DateTime output = ( DateTime.TryParseExact( value, format, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out output ) ) ? output : defVal;
			return output;
		}

		public static DateTime ParseDateTime( string value )
		{
			return ParseDateTime( value, DateTime.MinValue );
		}

		public static DateTime ParseExactDateTime( string value, string format )
		{
			return ParseExactDateTime( value, format, DateTime.MinValue );
		}

		public static decimal ParseDecimal( string value, decimal defVal )
		{
			decimal output = ( decimal.TryParse( value, out output ) ) ? output : defVal;

			return output;
		}

		public static decimal ParseDecimal( string value )
		{
			return ParseDecimal( value, 0 );
		}
	}
}
