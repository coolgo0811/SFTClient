﻿using NLog;
using SFTClient.WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Web;

namespace SFTClient
{
    public class SFTSoapService : SFTService
    {
        private const int defaultTimeout = 5000;
        private const int pingTimeout = 1000;

        private Logger _logger;

        public bool IsConnected
        {
            get
            {
                try
                {
                    Ping ping = new Ping();
                    PingReply reply = ping.Send( MyConfig.ServerIP, pingTimeout );
                    if ( reply.Status == IPStatus.Success )
                        return true;
                    else
                        return false;

                    /*var myRequest = ( HttpWebRequest ) WebRequest.Create( base.Url );
                    myRequest.Timeout = 1000;
                    var response = ( HttpWebResponse ) myRequest.GetResponse();
                    if ( response.StatusCode == HttpStatusCode.OK )
                        return true;
                    else
                        return false;*/
                }
                catch ( Exception ex )
                {
                    return false;
                }
            }
        }

        public SFTSoapService( Logger logger )
        {
            _logger = logger;
            IPAddress address;
            if ( IPAddress.TryParse( MyConfig.ServerIP, out address ) && Parser.ParseInt( MyConfig.ServerPort ) != 0 )
            {
                base.Url = string.Format( "http://{0}:{1}/{2}/services/SFTService?wsdl", MyConfig.ServerIP, MyConfig.ServerPort, MyConfig.SystemModel );
            }

            int timeout = Parser.ParseInt( MyConfig.ServerTimeout );
            if ( timeout != 0 )
                base.Timeout = Parser.ParseInt( MyConfig.ServerTimeout ) * 1000;
            else
                base.Timeout = defaultTimeout;
        }

        public T DoService<T>( object msgIn )
        {
            string xml = XmlParser.Serialize( msgIn );
            _logger.Info( "Request: " + xml );
            string result = base.HMI_DoSFTAction( xml );
            _logger.Info( "Response: " + result );
            return XmlParser.Deserialize<T>( result );
        }
    }
}