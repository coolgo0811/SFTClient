﻿using System;
using System.Web.Caching;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 擴充 System.Web.Caching 命名空間的 Extension Methods
/// </summary>
namespace SFTClient
{
    public static class Extension
    {
        public static void Clear( this Cache x )
        {
            List<string> cacheKeys = new List<string>();
            IDictionaryEnumerator cacheEnum = x.GetEnumerator();
            while ( cacheEnum.MoveNext() )
            {
                cacheKeys.Add( cacheEnum.Key.ToString() );
            }
            foreach ( string cacheKey in cacheKeys )
            {
                x.Remove( cacheKey );
            }
        }
    }
}

