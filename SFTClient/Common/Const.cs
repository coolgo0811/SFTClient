﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class Const
    {
        public const bool CHECK_AUTHORITY = false;

        public const string SYSTEM_SETTING_FILE = "sys_setting.xml";

        public const string SIGNIN_PAGE = @"/broadweb/user/signinonly.asp";
        public const string SESSION_WAPATH = "ssWaPath";
        public const string SESSION_PROJ_NAME = "ssProjName";
        public const string SESSION_NODE_NAME = "ssNodeName";
        public const string COOKIE_NAME = "RptCookie";

        public const string DEFAULT_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

        public const string SUCCESS = "success";
        public const string FAIL = "fail";

        public const string LIVEUPDATE_EXE = "LiveUpdate.exe";
        public const string CONFIG_DIRECTORY_NAME = "config";
        public const string LOGS_DIRECTORY_NAME = "logs";
    }

    public class ServiceMsg
    {
        public const string DEFAULT_SYSMSG = "系統錯誤 !";
        public const string NETWORK_FAIL_SYSMSG = "伺服器連線異常 !";
    }

    public class Page
    {
        public const string TRACKINOUT = "Trackinout";
        public const string MOINFO = "MoInfo";
        public const string SIMPLETRACK = "SimpleTrack";
        public const string REWORK = "Rework";
        public const string WIPHOLD = "WipHold";
        public const string EQUIPSTATUS = "EquipStatus";
        public const string EQUIPINTEGRATE = "EquipIntegrate";
    }

    public class IIS
    {
        public const string DEFAULT_WEBSITE = "Default Web Site";
        public const string LOCALHOST_PATH = @"C:\Inetpub\wwwroot\broadweb\";
        public const string SFTCLIENT_DIRECTORY_NAME = "SFTClient";
    }

    public class LoggerName
    {
        public const string NORMAL = "Normal";
        public const string LIVEUPDATE = "LiveUpdate";
    }
}