﻿using Microsoft.Web.Administration;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Caching;

namespace SFTClient
{
    public class LiveUpdate
    {
        private static Logger logger = NLog.LogManager.GetLogger( LoggerName.LIVEUPDATE );

        public static bool CheckVersion( ref string version )
        {
            Version currentVer;
            // load version
            Assembly assembly = Assembly.LoadFrom( System.Web.HttpContext.Current.Server.MapPath( @"~\bin\" ) + "SFTClient.dll" );
            currentVer = assembly.GetName().Version;

            // get the newest version on server
            if ( Directory.Exists( SysSettingDoc.ServerFolder ) == false )
            {
                return false;
            }

            string[] versionList = Directory.GetDirectories( SysSettingDoc.ServerFolder );
            if ( versionList.Length == 0 )  // no any version folder
            {
                return false;
            }

            string path = versionList[versionList.Length - 1];
            string folderName = Path.GetFileName( path );

            Version newestVer = Version.Parse( folderName );

            if ( newestVer.CompareTo( currentVer ) == 0 )
            {
                return false;
            }

            // no file in newest folder
            path = Path.Combine( path, IIS.SFTCLIENT_DIRECTORY_NAME );
            if ( Directory.Exists( path ) == false )
            {
                return false;
            }

            version = newestVer.ToString();
            logger.Info( string.Format( "找到新版本 {0} !", version ) );
            return true;
        }

        public static bool DoUpdate( string version )
        {
            ServerManager server = new ServerManager();
            Site site = server.Sites.FirstOrDefault( s => s.Name == IIS.DEFAULT_WEBSITE );
            if ( site == null )
            {
                logger.Error( string.Format( "IIS 站台{0}不存在，請聯繫系統管理員 !", IIS.DEFAULT_WEBSITE ) );
                return false;
            }

            try
            {
                if ( string.IsNullOrEmpty( version ) )
                    return false;

                logger.Info( "---LiveUpdate Start---" );

                string sftDirPath, backupDirPath, servDirPath, tmpDirPath;
                
                servDirPath = Path.Combine( SysSettingDoc.ServerFolder, version, IIS.SFTCLIENT_DIRECTORY_NAME );
                sftDirPath = Path.Combine( IIS.LOCALHOST_PATH, IIS.SFTCLIENT_DIRECTORY_NAME );
                backupDirPath = Path.Combine( IIS.LOCALHOST_PATH, IIS.SFTCLIENT_DIRECTORY_NAME + "_back" );

                // stop IIS
                /*logger.Info( "IIS Stop..." );
                site.Stop();

                if ( site.State != ObjectState.Stopped )
                {
                    logger.Error( "IIS停止失敗，請聯繫系統管理員 !" );
                    return false;
                }*/

                // generate backup folder to avoid unexpected exception
                logger.Info( "Backup SFTClient Folder..." );
                DirectoryCopy( sftDirPath, backupDirPath, true );
                logger.Info( "Backup SFTClient Folder Success!" );

                // delete SFTClient folder
                logger.Info( "Delete SFTClient Folder..." );
                foreach ( string path in Directory.GetFiles( sftDirPath ) )
                {
                    if ( path.Contains( Const.CONFIG_DIRECTORY_NAME ) == false && path.Contains( Const.LOGS_DIRECTORY_NAME ) == false )
                        Directory.Delete( path, true );
                }
                logger.Info( "Delete SFTClient Folder Success!" );

                // copy new SFTClient folder to original path
                logger.Info( "Copy New SFTClient Folder..." );
                foreach ( string path in Directory.GetFiles( servDirPath ) )
                {
                    DirectoryCopy( path, sftDirPath, true );
                }
                logger.Info( "Copy New SFTClient Folder Success!" );

                // clear cache in IIS
                logger.Info( "Clear Cache..." );
                Cache cache = new Cache();
                cache.Clear();
                logger.Info( "Clear Cache Success!" );

                //restart IIS
                /*logger.Info( "IIS Restart..." );
                site.Start();
                if ( site.State != ObjectState.Started )
                {
                    logger.Error( "IIS啟動失敗，請聯繫系統管理員 !" );
                    return false;
                }*/

                logger.Info( "LiveUpdate End---" );
            }
            catch ( Exception ex )
            {
                if ( site.State != ObjectState.Started )
                    site.Start();

                logger.Error( "LiveUpdate 更新異常 ! {0}", ex.ToString() );
            }
            return true;
        }


        private static void DirectoryCopy( string sourceDirName, string destDirName, bool copySubDirs )
        {
            DirectoryInfo dir = new DirectoryInfo( sourceDirName );
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the source directory does not exist, throw an exception.
            if ( !dir.Exists )
            {
                throw new DirectoryNotFoundException( "Source directory does not exist or could not be found: " + sourceDirName );
            }

            // If the destination directory does not exist, create it.
            if ( !Directory.Exists( destDirName ) )
            {
                Directory.CreateDirectory( destDirName );
            }

            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach ( FileInfo file in files )
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine( destDirName, file.Name );
                // Copy the file.
                file.CopyTo( temppath, true );
            }

            // If copySubDirs is true, copy the subdirectories.
            if ( copySubDirs )
            {
                foreach ( DirectoryInfo subdir in dirs )
                {
                    // Create the subdirectory.
                    string temppath = Path.Combine( destDirName, subdir.Name );
                    // Copy the subdirectories.
                    DirectoryCopy( subdir.FullName, temppath, copySubDirs );
                }
            }
        }
    }
}