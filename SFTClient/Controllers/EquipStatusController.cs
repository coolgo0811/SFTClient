﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SFTClient.Controllers
{
    public class EquipStatusController : Controller
    {
        // GET: EquipStatus
        public ActionResult Index( string equipment )
        {
            if ( Const.CHECK_AUTHORITY )
            {
                Authority authority = new Authority();
                if ( authority.CheckSessionExist() == false )
                    return Redirect( Const.SIGNIN_PAGE );
            }

            if ( HttpContext.Session["UserName"] != null )
                ViewData["UserName"] = Session["UserName"];

            if ( string.IsNullOrEmpty( equipment ) )
            {
                ViewData["Equipment"] = MyConfig.Equipment;
                TempData["MainPage"] = true;
            }
            else
            {
                ViewData["Equipment"] = equipment;
                TempData["MainPage"] = false;   // means current page is a subpage within equipment integration
            }

            Session.Add( "Page", Page.EQUIPSTATUS );

            var auth = Session["Auth"];
            if ( auth != null && Convert.ToBoolean( auth ) == true )
            {
                return View();
            }

            return RedirectToAction( "Login", "Home" );
        }
    }
}