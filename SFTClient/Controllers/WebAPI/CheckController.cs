﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SFTClient.Controllers
{
    public class CheckController : ApiController
    {
        private static Logger logger = NLog.LogManager.GetLogger( LoggerName.NORMAL );

        [HttpPost]
        [ActionName( "EmployeeCheck" )]
        public EmployeeCheckOutput PostEmployeeCheck( EmployeeCheckInput input )
        {
            logger.Info( "EmployeeCheck Start" );
            EmployeeCheckOutput output = new EmployeeCheckOutput();
            MsgEmployeeCheckIn msgIn = new MsgEmployeeCheckIn();
            try
            {
                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();

                msgIn.Data.FormBody.RecordList.Code = input.Code;

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgEmployeeCheckOut msgOut = ( MsgEmployeeCheckOut ) service.DoService<MsgEmployeeCheckOut>( msgIn );
                    if ( msgOut != null )
                    {
                        output.Name = msgOut.Data.RecodeList.Name;
                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "EmployeeCheck End" );
            return output;
        }

        [HttpPost]
        [ActionName( "MoCheck" )]
        public MoCheckOutput PostMoCheck( MoCheckInput input )
        {
            logger.Info( "MoCheck Start" );
            MoCheckOutput output = new MoCheckOutput();
            MsgMoCheckIn msgIn = new MsgMoCheckIn();
            try
            {
                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                var equipment = HttpContext.Current.Session["Equipment"];
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();
                if ( equipment != null )
                    msgIn.Data.FormBody.RecordList.Equipment = equipment.ToString();

                string[] buff = input.MoId.Split( ';' );
                if ( buff.Length != 4 )
                {
                    output.SysMsg = "製令格式錯誤 !";
                    return output;
                }

                msgIn.Data.FormBody.RecordList.MoId = buff[0];
                msgIn.Data.FormBody.RecordList.OpSeq = buff[1];
                msgIn.Data.FormBody.RecordList.OpId = buff[2];
                msgIn.Data.FormBody.RecordList.WsId = buff[3];
                msgIn.Data.FormBody.RecordList.CheckQtyType = input.CheckQtyType;

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgMoCheckOut msgOut = ( MsgMoCheckOut ) service.DoService<MsgMoCheckOut>( msgIn );
                    if ( msgOut != null )
                    {
                        output.CheckQtyType = msgOut.Data.RecodeList.CheckQtyType;
                        output.CheckQty = msgOut.Data.RecodeList.CheckQty;
                        output.BeforeOpSeq = msgOut.Data.RecodeList.BeforeOpSeq;
                        output.Qty = msgOut.Data.RecodeList.Qty;
                        output.WorkTime = ( Math.Round( Parser.ParseDouble( msgOut.Data.RecodeList.WorkTime ) / 60, 2, MidpointRounding.AwayFromZero ) ).ToString();
                        output.ManWorkTime = ( Math.Round( Parser.ParseDouble( msgOut.Data.RecodeList.ManWorkTime ) / 60, 2, MidpointRounding.AwayFromZero ) ).ToString();

                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "MoCheck End" );
            return output;
        }

        [HttpPost]
        [ActionName( "TrackMoCheck" )]
        public TrackMoCheckOutput PostTrackMoCheck( TrackMoCheckInput input )
        {
            logger.Info( "TrackMoCheck Start" );
            TrackMoCheckOutput output = new TrackMoCheckOutput();
            MsgTrackMoCheckIn msgIn = new MsgTrackMoCheckIn();
            try
            {
                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                var equipment = HttpContext.Current.Session["Equipment"];
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();
                if ( equipment != null )
                    msgIn.Data.FormBody.RecordList.Equipment = equipment.ToString();

                string[] buff = input.MoId.Split( ';' );
                if ( buff.Length != 4 )
                {
                    output.SysMsg = "製令格式錯誤 !";
                    return output;
                }

                msgIn.Data.FormBody.RecordList.MoId = buff[0];
                msgIn.Data.FormBody.RecordList.OpSeq = buff[1];
                msgIn.Data.FormBody.RecordList.OpId = buff[2];
                msgIn.Data.FormBody.RecordList.WsId = buff[3];

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgTrackMoCheckOut msgOut = ( MsgTrackMoCheckOut ) service.DoService<MsgTrackMoCheckOut>( msgIn );
                    if ( msgOut != null )
                    {
                        output.CheckInQty = msgOut.Data.RecodeList.CheckInQty;
                        output.CheckOutQty = msgOut.Data.RecodeList.CheckOutQty;
                        output.BeforeOpSeq = msgOut.Data.RecodeList.BeforeOpSeq;
                        output.Qty = msgOut.Data.RecodeList.Qty;
                        output.WorkTime = ( Math.Round( Parser.ParseDouble( msgOut.Data.RecodeList.WorkTime ) / 60, 2, MidpointRounding.AwayFromZero ) ).ToString();
                        output.ManWorkTime = ( Math.Round( Parser.ParseDouble( msgOut.Data.RecodeList.ManWorkTime ) / 60, 2, MidpointRounding.AwayFromZero ) ).ToString();

                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "TrackMoCheck End" );
            return output;
        }


        [HttpPost]
        [ActionName( "ExceptionCheck" )]
        public ExceptionCheckOutput PostExceptionCheck( ExceptionCheckInput input )
        {
            logger.Info( "ExceptionCheck Start" );
            ExceptionCheckOutput output = new ExceptionCheckOutput();
            MsgExceptionCheckIn msgIn = new MsgExceptionCheckIn();
            try
            {
                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();

                msgIn.Data.FormBody.RecordList.CheckType = input.CheckType;
                msgIn.Data.FormBody.RecordList.Code = input.Code;

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgExceptionCheckOut msgOut = ( MsgExceptionCheckOut ) service.DoService<MsgExceptionCheckOut>( msgIn );
                    if ( msgOut != null )
                    {
                        output.Type = msgOut.Data.RecodeList.Type;
                        output.Name = msgOut.Data.RecodeList.Name;

                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "ExceptionCheck End" );
            return output;
        }
    }
}
