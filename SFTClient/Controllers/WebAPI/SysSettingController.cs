﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SFTClient.Controllers
{
    public class SysSettingController : ApiController
    {
        private const string WMI_NAMESPACE = "Win32_NetworkAdapterConfiguration";
        private const string WMI_CLASS = "root\\CIMV2";
        private static Logger logger = NLog.LogManager.GetLogger( LoggerName.NORMAL );

        [HttpGet]
        [ActionName( "Get" )]
        public SysSettingOutput Get()
        {
            SysSettingOutput output = new SysSettingOutput();
            try
            {
                string strQuery = "Select * from Win32_NetworkAdapterConfiguration where IPEnabled = True";
                ManagementObjectSearcher searcher = new ManagementObjectSearcher( strQuery );
                ManagementObjectCollection collection = searcher.Get();
                var queryCollection = from ManagementObject x in searcher.Get()
                                      select x;

                ManagementObject objManagement = queryCollection.FirstOrDefault();
                if ( objManagement != null )
                {
                    bool dhcp = Convert.ToBoolean( objManagement["DHCPEnabled"] );

                    string[] address = ( string[] )objManagement["IPAddress"];
                    string[] subnets = ( string[] )objManagement["IPSubnet"];
                    string[] gateways = ( string[] )objManagement["DefaultIPGateway"];

                    if ( address != null && address.Length > 0 )
                        output.LocalIP = address[0];
                    if ( subnets != null && subnets.Length > 0 )
                        output.LocalMask = subnets[0];
                    if ( gateways != null && gateways.Length > 0 )
                        output.LocalGateway = gateways[0];

                    output.IsDHCP = dhcp;
                }

                if ( MyConfig.Load( HttpContext.Current.Server.MapPath( @"~\config\" ) + Const.SYSTEM_SETTING_FILE ) == false )
                {
                    output.SysMsg = "讀取Config檔案失敗 !";
                    return output;
                }

                MyConfig.LocalIP = output.LocalIP;
                MyConfig.Save( HttpContext.Current.Server.MapPath( @"~\config\" ) + Const.SYSTEM_SETTING_FILE );

                output.ServerIP = MyConfig.ServerIP;
                output.ServerPort = MyConfig.ServerPort;
                output.ServerTimeout = MyConfig.ServerTimeout;
                output.UserName = MyConfig.UserName;
                output.Password = MyConfig.Password;
                output.AutoLogoutTime = MyConfig.AutoLogoutTime;
                output.CompanyId = MyConfig.CompanyId;
                output.Equipment = MyConfig.Equipment;
                output.EquipDataCollection = MyConfig.EquipDataCollection;
                if ( MyConfig.SystemModel == "SFT" )
                    output.SystemModel = 0;
                else if ( MyConfig.SystemModel == "WIP" )
                    output.SystemModel = 1;
                else
                    output.SystemModel = 0;

                output.LiveUpdate = MyConfig.LiveUpdate;
                output.EquipTrackinoutModel = MyConfig.EquipTrackinoutModel;
            }
            catch ( WebException webEx )
            {
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }
            return output;
        }

        [HttpPost]
        [ActionName( "Set" )]
        public bool Post( SysSettingInput input )
        {
            try
            {
                if ( input.IsDHCP )
                {
                    setAutoGetIP();
                }
                else
                {
                    setStaticIP( input );
                }

                MyConfig.LocalIP = input.LocalIP;
                MyConfig.ServerIP = input.ServerIP;
                MyConfig.ServerPort = input.ServerPort;
                MyConfig.ServerTimeout = input.ServerTimeout;
                MyConfig.UserName = input.UserName;
                MyConfig.Password = input.Password;
                MyConfig.AutoLogoutTime = input.AutoLogoutTime;
                MyConfig.CompanyId = input.CompanyId;
                MyConfig.Equipment = input.Equipment;
                MyConfig.EquipDataCollection = input.EquipDataCollection;
                
                if ( input.SystemModel == 0 )
                    MyConfig.SystemModel = "SFT";
                else if ( input.SystemModel == 1 )
                    MyConfig.SystemModel = "WIP";
                else
                    MyConfig.SystemModel = "SFT";

                MyConfig.LiveUpdate = input.LiveUpdate;
                MyConfig.EquipTrackinoutModel = input.EquipTrackinoutModel;

                MyConfig.Save( HttpContext.Current.Server.MapPath( @"~\config\" ) + Const.SYSTEM_SETTING_FILE );

                return true;
            }
            catch ( Exception ex )
            {
                return false;
            }
        }

        [HttpGet]
        [ActionName( "ServStatus" )]
        public bool GetServStatus()
        {
            using ( SFTSoapService service = new SFTSoapService( logger ) )
            {
                return service.IsConnected;
            }
        }

        [HttpGet]
        [ActionName( "SysParamValue" )]
        public string GetSysParamValue( string id )
        {
            return MyConfig.GetParamValue( id );
        }

        [HttpGet]
        [ActionName( "LiveUpdate" )]
        public string SysLiveUpdate()
        {
            if ( MyConfig.LiveUpdate == false )
                return "";

            // check if need to update
            string version = string.Empty;
            string filePath = string.Empty;
            if ( LiveUpdate.CheckVersion( ref version, ref filePath ) )
            {
                if ( LiveUpdate.DoUpdate( version, filePath ) == false )
                    return string.Empty;
            }

            return version;
        }

        [HttpGet]
        [ActionName( "Shutdown" )]
        public string Shutdown()
        {
            string output = string.Empty;
            try
            {
                /*string strQuery = "Select * from Win32_OperatingSystem";
                ManagementObjectSearcher searcher = new ManagementObjectSearcher( strQuery );
                ManagementObjectCollection collection = searcher.Get();
                var queryCollection = from ManagementObject x in searcher.Get()
                                      select x;

                ManagementObject objManagement = queryCollection.FirstOrDefault();

                //winClass.Scope.Options.EnablePrivileges = true;
                if ( objManagement != null )
                {
                    objManagement.InvokeMethod( "Shutdown", null );
                }*/

                output = ExecuteCmd( "shutdown -s" );
            }
            catch ( Exception ex )
            {
                output = "關機程序異常 !";
            }

            return output;
        }

        #region >>> Private Field <<<

        private void setAutoLogoutTime( int timeout )
        {
            HttpContext.Current.Session.Timeout = timeout;
        }

        private void setAutoGetIP()
        {
            try
            {
                string strQuery = "Select * from Win32_NetworkAdapterConfiguration where IPEnabled = True";
                ManagementObjectSearcher searcher = new ManagementObjectSearcher( strQuery );
                ManagementObjectCollection collection = searcher.Get();
                var queryCollection = from ManagementObject x in searcher.Get()
                                      select x;
                string index = queryCollection.FirstOrDefault()["Index"].ToString();
                if ( index != null || index != string.Empty )
                {
                    using ( ManagementObject objManagement = new ManagementObject( WMI_CLASS, WMI_NAMESPACE + ".INDEX=" + index, null ) )
                    {
                        objManagement.InvokeMethod( "EnableDHCP", null ); // set DHCP
                        objManagement.InvokeMethod( "ReleaseDHCPLease", null ); // release IP
                        objManagement.InvokeMethod( "RenewDHCPLease", null ); // reget IP
                        objManagement.InvokeMethod( "SetDNSServerSearchOrder", null ); // set auto get DNS server
                    }
                }
            }
            catch ( Exception ex )
            {
                logger.Error( string.Format( "DHCP網路設定失敗 ! {0}", ex.ToString() ) );
            }
        }

        private void setStaticIP( SysSettingInput input )
        {
            try
            {
                string strQuery = "Select * from Win32_NetworkAdapterConfiguration where IPEnabled = True";
                ManagementObjectSearcher searcher = new ManagementObjectSearcher( strQuery );
                ManagementObjectCollection collection = searcher.Get();
                var queryCollection = from ManagementObject x in searcher.Get()
                                      select x;
                string index = queryCollection.FirstOrDefault()["Index"].ToString();
                if ( index != null || index != string.Empty )
                {
                    using ( ManagementObject objManagement = new ManagementObject( WMI_CLASS, WMI_NAMESPACE + ".INDEX=" + index, null ) )
                    {
                        ManagementBaseObject objPara = objManagement.GetMethodParameters( "EnableStatic" );
                        objPara["IPAddress"] = new string[] { input.LocalIP }; // set IP
                        objPara["SubnetMask"] = new string[] { input.LocalMask }; // set mask
                        objManagement.InvokeMethod( "EnableStatic", objPara, null );

                        objPara = objManagement.GetMethodParameters( "SetGateways" );
                        objPara["DefaultIPGateway"] = new string[] { input.LocalGateway }; // set gateway
                        objManagement.InvokeMethod( "SetGateways", objPara, null );

                        //objPara = objManagement.GetMethodParameters( "SetDNSServerSearchOrder" );
                        //objPara["DNSServerSearchOrder"] = new string[] { "back" }; // set DNS
                        objManagement.InvokeMethod( "SetDNSServerSearchOrder", null );
                    }
                }
            }
            catch ( Exception ex )
            {
                logger.Error( string.Format( "固定IP網路設定失敗 ! {0}", ex.ToString() ) );
            }
        }

        private static string ExecuteCmd( string command )
        {
            string output = "";
            if ( !string.IsNullOrEmpty( command ) )
            {
                Process process = new Process();
                ProcessStartInfo startInfo = new ProcessStartInfo();

                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/C " + command;
                startInfo.UseShellExecute = false;
                startInfo.RedirectStandardInput = false;
                startInfo.RedirectStandardOutput = true;
                startInfo.CreateNoWindow = true;
                process.StartInfo = startInfo;
                process.Start();

                output = process.StandardOutput.ReadToEnd();
                process.WaitForExit();

            }
            return output;
        }

        #endregion
    }
}
