﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SFTClient.Controllers
{
    public class AuthorityController : ApiController
    {
        private static Logger logger = NLog.LogManager.GetLogger( LoggerName.NORMAL );

        [HttpPost]
        [ActionName( "Login" )]
        public LoginOutput PostLogin( LoginInput input )
        {
            logger.Info( "Login Start" );
            MsgLoginIn msgIn = new MsgLoginIn();
            LoginOutput output = new LoginOutput();
            try
            {
                var company = HttpContext.Current.Session["CompanyId"];
                if ( company != null )
                    msgIn.CompanyId = company.ToString();

                msgIn.UserId = input.UserName;
                msgIn.Data.FormHead.RecordList.EqMode = "N";
                msgIn.Data.FormHead.RecordList.Password = input.Password;
                msgIn.Data.FormHead.RecordList.IP = MyConfig.LocalIP;

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgLoginOut msgOut = service.DoService<MsgLoginOut>( msgIn );
                    if ( msgOut != null )
                    {
                        if ( msgOut.Result == Const.SUCCESS )
                        {
                            HttpContext.Current.Session.Add( "Auth", true );
                            HttpContext.Current.Session.Add( "UserName", msgIn.UserId );
                        }

                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "Login End" );
            return output;
        }

        [HttpPost]
        [ActionName( "Logout" )]
        public LoginOutput PostLogout()
        {
            logger.Info( "Logout Start" );
            MsgLogoutIn msgIn = new MsgLogoutIn();
            LoginOutput output = new LoginOutput();
            try
            {
                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();

                msgIn.Data.FormHead.RecordList.EqMode = "N";
                msgIn.Data.FormHead.RecordList.IP = MyConfig.LocalIP;

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgLogoutOut msgOut = ( MsgLogoutOut ) service.DoService<MsgLogoutOut>( msgIn );
                    if ( msgOut != null )
                    {
                        if ( msgOut.Result == Const.SUCCESS )
                        {
                            HttpContext.Current.Session.Add( "Auth", false );
                            HttpContext.Current.Session.Add( "UserName", string.Empty );
                        }

                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "Logout End" );
            return output;
        }
    }
}