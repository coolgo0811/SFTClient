﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SFTClient.Controllers
{
    public class StatusChangeController : ApiController
    {
        private static Logger logger = NLog.LogManager.GetLogger( LoggerName.NORMAL );

        [HttpPost]
        [ActionName( "Equipment" )]
        public EquipStatusChangeOutput PostEquipment( EquipStatusChangeInput input )
        {
            logger.Info( "EquipmentStatusChange Start" );
            EquipStatusChangeOutput output = new EquipStatusChangeOutput();
            MsgEquipStatusChangeIn msgIn = new MsgEquipStatusChangeIn();
            try
            {
                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();

                msgIn.Data.FormHead.RecordList.Equipment = input.Equipment;
                msgIn.Data.FormHead.RecordList.Status = input.Status;

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgEmployeeCheckOut msgOut = ( MsgEmployeeCheckOut ) service.DoService<MsgEmployeeCheckOut>( msgIn );
                    if ( msgOut != null )
                    {
                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "EquipmentStatusChange End" );
            return output;
        }

        [HttpPost]
        [ActionName( "Mo" )]
        public MoStatusChangeOutput PostMo( MoStatusChangeInput input )
        {
            logger.Info( "MoStatusChange Start" );
            MoStatusChangeOutput output = new MoStatusChangeOutput();
            MsgMoStatusChangeIn msgIn = new MsgMoStatusChangeIn();
            try
            {
                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();

                msgIn.Data.FormHead.RecordList.Status = input.Status;
                msgIn.Data.FormHead.RecordList.ChangeType = input.ChangeType;
                msgIn.Data.FormHead.RecordList.Reason = input.Reason;

                // Mo info
                MoStatusChangeRecord record;
                MoStatusChangeBodyRecord bodyRecord;
                for ( int i = 0 ; i < input.RecordList.Count ; i++ )
                {
                    record = input.RecordList[i];
                    string[] buff = record.MoId.Split( ';' );

                    bodyRecord = new MoStatusChangeBodyRecord();
                    //bodyRecord.Equipment = equipment;
                    bodyRecord.MoId = buff[0];
                    bodyRecord.OpSeq = buff[1];
                    bodyRecord.OpId = buff[2];
                    bodyRecord.WsId = buff[3];

                    msgIn.Data.FormBody.RecordList.Add( bodyRecord );
                }

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgMoStatusChangeOut msgOut = ( MsgMoStatusChangeOut ) service.DoService<MsgMoStatusChangeOut>( msgIn );
                    if ( msgOut != null )
                    {
                        output.MoId = string.Join( ";", msgOut.Data.RecodeList.MoId, msgOut.Data.RecodeList.OpSeq, 
                                                    msgOut.Data.RecodeList.OpId, msgOut.Data.RecodeList.WsId );

                        output.ChangeResult = msgOut.Data.RecodeList.Result;
                        output.ChangeReason = msgOut.Data.RecodeList.Reason;

                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "MoStatusChange End" );
            return output;
        }
    }
}