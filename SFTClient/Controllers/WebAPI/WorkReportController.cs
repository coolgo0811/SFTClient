﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SFTClient.Controllers
{
    public class WorkReportController : ApiController
    {
        private static Logger logger = NLog.LogManager.GetLogger( LoggerName.NORMAL );

        [HttpPost]
        [ActionName( "Trackin" )]
        public TrackinOutput PostTrackin( TrackinInput input )
        {
            logger.Info( "Trackin Start" );
            TrackinOutput output = new TrackinOutput();
            MsgTrackinIn msgIn = new MsgTrackinIn();

            try
            {
                if ( input.RecordList.Count == 0 )
                {
                    output.SysMsg = "請至少刷入一筆製令 !";
                    return output;
                }

                TrackinRecord record;
                for ( int i = 0 ; i < input.RecordList.Count ; i++ )
                {
                    record = input.RecordList[i];
                    if ( string.IsNullOrEmpty( record.MoId ) )
                    {
                        output.SysMsg = "製令不可為空 !";
                        return output;
                    }
                    string[] buff = record.MoId.Split( ';' );
                    if ( buff.Length != 4 )
                    {
                        output.SysMsg = "製令格式錯誤 !";
                        return output;
                    }
                }

                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                string equipment = string.Empty;
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();
                if ( HttpContext.Current.Session["Equipment"] != null )
                    equipment = HttpContext.Current.Session["Equipment"].ToString();

                string nowTime = DateTime.Now.ToString( Const.DEFAULT_TIME_FORMAT );

                TrackinInBodyRecord bodyRecord;
                for ( int i = 0 ; i < input.RecordList.Count ; i++ )
                {
                    string[] buff = input.RecordList[i].MoId.Split( ';' );

                    bodyRecord = new TrackinInBodyRecord();
                    bodyRecord.UpdateTime = nowTime;
                    bodyRecord.Equipment = equipment;
                    bodyRecord.MoId = buff[0];
                    bodyRecord.OpSeq = buff[1];
                    bodyRecord.OpId = buff[2];
                    bodyRecord.WsId = buff[3];
                    bodyRecord.Qty = input.RecordList[i].Qty;
                    msgIn.Data.FormBody.RecordList.Add( bodyRecord );
                }

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgTrackinOut msgOut = ( MsgTrackinOut ) service.DoService<MsgTrackinOut>( msgIn );
                    if ( msgOut != null )
                    {
                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "Trackin End" );
            return output;
        }

        [HttpPost]
        [ActionName( "Trackout" )]
        public TrackoutOutput PostTrackOut( TrackoutInput input )
        {
            logger.Info( "Trackout Start" );
            TrackoutOutput output = new TrackoutOutput();
            MsgTrackoutIn msgIn = new MsgTrackoutIn();
            try
            {
                TrackoutRecord record;

                if ( input.RecordList.Count == 0 )
                {
                    output.SysMsg = "請至少刷入一筆製令 !";
                    return output;
                }
                for ( int i = 0 ; i < input.RecordList.Count ; i++ )
                {
                    record = input.RecordList[i];
                    if ( string.IsNullOrEmpty( record.MoId ) )
                    {
                        output.SysMsg = "製令不可為空 !";
                        return output;
                    }
                    string[] buff = record.MoId.Split( ';' );
                    if ( buff.Length != 4 )
                    {
                        output.SysMsg = "製令格式錯誤 !";
                        return output;
                    }
                }

                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                string equipment = string.Empty;
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();
                if ( HttpContext.Current.Session["Equipment"] != null )
                    equipment = HttpContext.Current.Session["Equipment"].ToString();
                string nowTime = DateTime.Now.ToString( Const.DEFAULT_TIME_FORMAT );

                msgIn.Data.FormBody.CheckOutMode = input.CheckOutMode;
                msgIn.Data.FormBody.CheckOutType = input.CheckOutType;

                // Mo info
                TrackoutInBodyRecord bodyRecord;
                for ( int i = 0 ; i < input.RecordList.Count ; i++ )
                {
                    record = input.RecordList[i];
                    string[] buff = record.MoId.Split( ';' );

                    bodyRecord = new TrackoutInBodyRecord();
                    bodyRecord.Equipment = equipment;
                    bodyRecord.MoId = buff[0];
                    bodyRecord.OpSeq = buff[1];
                    bodyRecord.OpId = buff[2];
                    bodyRecord.WsId = buff[3];
                    bodyRecord.Qty = record.Qty;
                    bodyRecord.UpdateOutTime = nowTime;
                    bodyRecord.ManWorkTime = ( Math.Round( Parser.ParseDouble( record.ManWorkTime ) * 60, 0, MidpointRounding.AwayFromZero ) ).ToString();
                    bodyRecord.WorkTime = ( Math.Round( Parser.ParseDouble( record.WorkTime ) * 60, 0, MidpointRounding.AwayFromZero ) ).ToString();

                    // exception info
                    TrackoutInExceptionRecord exceptionRecord;
                    for ( int j = 0 ; j < record.Exception.Count ; j++ )
                    {
                        exceptionRecord = new TrackoutInExceptionRecord();
                        exceptionRecord.Code = record.Exception[j].Code;
                        exceptionRecord.Qty = record.Exception[j].Qty;
                        bodyRecord.Exception.ExRecordList.Add( exceptionRecord );
                    }

                    //work info
                    if ( record.Work.Count > 0 )
                        bodyRecord.Work.Number = record.Work.Count.ToString();
                    TrackoutInWorkRecord workRecord;
                    for ( int j = 0 ; j < record.Work.Count ; j++ )
                    {
                        workRecord = new TrackoutInWorkRecord();
                        workRecord.User = record.Work[j].User;
                        workRecord.Times = ( Math.Round( Parser.ParseDouble( record.Work[j].Times ) * 60, 0, MidpointRounding.AwayFromZero ) ).ToString();
                        bodyRecord.Work.WorkRecordList.Add( workRecord );
                    }

                    msgIn.Data.FormBody.RecordList.Add( bodyRecord );
                }

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgTrackoutOut msgOut = ( MsgTrackoutOut ) service.DoService<MsgTrackoutOut>( msgIn );
                    if ( msgOut != null )
                    {
                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "Trackout End" );
            return output;
        }


        [HttpPost]
        [ActionName( "Rework" )]
        public ReworkOutput PostRework( ReworkInput input )
        {
            logger.Info( "Rework Start" );
            ReworkOutput output = new ReworkOutput();
            MsgReworkIn msgIn = new MsgReworkIn();
            try
            {
                ReworkRecord record;

                if ( input.RecordList.Count == 0 )
                {
                    output.SysMsg = "請至少刷入一筆製令 !";
                    return output;
                }
                for ( int i = 0 ; i < input.RecordList.Count ; i++ )
                {
                    record = input.RecordList[i];
                    if ( string.IsNullOrEmpty( record.MoId ) )
                    {
                        output.SysMsg = "製令不可為空 !";
                        return output;
                    }
                    string[] buff = record.MoId.Split( ';' );
                    if ( buff.Length != 4 )
                    {
                        output.SysMsg = "製令格式錯誤 !";
                        return output;
                    }
                }

                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                string equipment = string.Empty;
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();
                if ( HttpContext.Current.Session["Equipment"] != null )
                    equipment = HttpContext.Current.Session["Equipment"].ToString();
                string nowTime = DateTime.Now.ToString( Const.DEFAULT_TIME_FORMAT );

                msgIn.Data.FormBody.ReworkType = input.ReworkType;

                // Mo info
                ReworkInBodyRecord bodyRecord;
                for ( int i = 0 ; i < input.RecordList.Count ; i++ )
                {
                    record = input.RecordList[i];
                    string[] buff = record.MoId.Split( ';' );

                    bodyRecord = new ReworkInBodyRecord();
                    bodyRecord.Equipment = equipment;
                    bodyRecord.MoId = buff[0];
                    bodyRecord.OpSeq = buff[1];
                    bodyRecord.OpId = buff[2];
                    bodyRecord.WsId = buff[3];
                    bodyRecord.Qty = record.Qty;
                    bodyRecord.UpdateTime = nowTime;
                    bodyRecord.ReworkWorkTime = ( Math.Round( Parser.ParseDouble( record.ReworkWorkTime ) * 60, 0, MidpointRounding.AwayFromZero ) ).ToString();

                    // exception info
                    ReworkInExceptionRecord exceptionRecord;
                    for ( int j = 0 ; j < record.Exception.Count ; j++ )
                    {
                        exceptionRecord = new ReworkInExceptionRecord();
                        exceptionRecord.Code = record.Exception[j].Code;
                        exceptionRecord.Qty = record.Exception[j].Qty;
                        bodyRecord.Exception.ExRecordList.Add( exceptionRecord );
                    }

                    msgIn.Data.FormBody.RecordList.Add( bodyRecord );
                }

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgTrackoutOut msgOut = ( MsgTrackoutOut ) service.DoService<MsgTrackoutOut>( msgIn );
                    if ( msgOut != null )
                    {
                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "Rework End" );
            return output;
        }


        [HttpPost]
        [ActionName( "Precheck" )]
        public PrecheckOutput PostPrecheck( PrecheckInput input )
        {
            logger.Info( "Precheck Start" );
            PrecheckOutput output = new PrecheckOutput();
            MsgPrecheckIn msgIn = new MsgPrecheckIn();
            try
            {
                PrecheckInRecord inRecord;

                if ( input.RecordList.Count == 0 )
                {
                    output.SysMsg = "請至少刷入一筆製令 !";
                    return output;
                }
                for ( int i = 0 ; i < input.RecordList.Count ; i++ )
                {
                    inRecord = input.RecordList[i];
                    if ( string.IsNullOrEmpty( inRecord.MoId ) )
                    {
                        output.SysMsg = "製令不可為空 !";
                        return output;
                    }
                    string[] buff = inRecord.MoId.Split( ';' );
                    if ( buff.Length != 4 )
                    {
                        output.SysMsg = "製令格式錯誤 !";
                        return output;
                    }
                }

                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                string equipment = string.Empty;
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();
                if ( HttpContext.Current.Session["Equipment"] != null )
                    equipment = HttpContext.Current.Session["Equipment"].ToString();

                msgIn.Data.FormBody.CheckType = input.CheckType;

                // Mo info
                PrecheckInBodyRecord bodyRecord;
                for ( int i = 0 ; i < input.RecordList.Count ; i++ )
                {
                    inRecord = input.RecordList[i];
                    string[] buff = inRecord.MoId.Split( ';' );

                    bodyRecord = new PrecheckInBodyRecord();
                    bodyRecord.Equipment = equipment;
                    bodyRecord.MoId = buff[0];
                    bodyRecord.OpSeq = buff[1];
                    bodyRecord.OpId = buff[2];
                    bodyRecord.WsId = buff[3];
                    bodyRecord.Qty = inRecord.Qty;

                    msgIn.Data.FormBody.RecordList.Add( bodyRecord );
                }

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgPrecheckOut msgOut = ( MsgPrecheckOut ) service.DoService<MsgPrecheckOut>( msgIn );
                    if ( msgOut != null )
                    {
                        PrecheckOutRecord outRecord;
                        for ( int i = 0 ; i < msgOut.Data.RecodeList.Count ; i++ )
                        {
                            outRecord = new PrecheckOutRecord();
                            outRecord.CheckType = msgOut.Data.RecodeList[i].CheckType;
                            outRecord.CheckMsg = msgOut.Data.RecodeList[i].CheckMsg;

                            output.RecordList.Add( outRecord );
                        }

                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "Precheck End" );
            return output;
        }
    }
}
