﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SFTClient.Controllers
{
    public class GetDataController : ApiController
    {
        private static Logger logger = NLog.LogManager.GetLogger( LoggerName.NORMAL );

        [HttpPost]
        [ActionName( "EquipmentStatus" )]
        public EquipStatusOutput PostEquipmentStatus( EquipStatusInput input )
        {
            logger.Info( "GetEquipmentStatus Start" );
            EquipStatusOutput output = new EquipStatusOutput();
            MsgEquipStatusIn msgIn = new MsgEquipStatusIn();
            try
            {
                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();

                msgIn.Data.FormHead.RecordList.Equipment = input.Equipment;

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgEquipStatusOut msgOut = ( MsgEquipStatusOut ) service.DoService<MsgEquipStatusOut>( msgIn );
                    if ( msgOut != null )
                    {
                        output.Status = msgOut.Data.RecodeList.Status;

                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "GetEquipmentStatus End" );
            return output;
        }

        [HttpPost]
        [ActionName( "MoStatus" )]
        public MoStatusOutput PostMoStatus( MoStatusInput input )
        {
            logger.Info( "GetMoStatus Start" );
            MoStatusOutput output = new MoStatusOutput();
            MsgMoStatusIn msgIn = new MsgMoStatusIn();
            try
            {
                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                var equipment = HttpContext.Current.Session["Equipment"];
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();
                if ( equipment != null )
                    msgIn.Data.FormHead.RecordList.Equipment = equipment.ToString();

                string[] buff = input.MoId.Split( ';' );
                if ( buff.Length != 4 )
                {
                    output.SysMsg = "製令格式錯誤 !";
                    return output;
                }

                msgIn.Data.FormHead.RecordList.MoId = buff[0];
                msgIn.Data.FormHead.RecordList.OpSeq = buff[1];
                msgIn.Data.FormHead.RecordList.OpId = buff[2];
                msgIn.Data.FormHead.RecordList.WsId = buff[3];

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgMoStatusOut msgOut = ( MsgMoStatusOut ) service.DoService<MsgMoStatusOut>( msgIn );
                    if ( msgOut != null )
                    {
                        output.Status = msgOut.Data.RecodeList.Status;

                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "GetMoStatus End" );
            return output;
        }


        [HttpGet]
        [ActionName( "EmployeeBatchInfo" )]
        public EmployeeBatchInfoOutput GetEmployeeBatchInfo()
        {
            logger.Info( "GetEmployeeBatchInfo Start" );
            EmployeeBatchInfoOutput output = new EmployeeBatchInfoOutput();
            MsgEmployeeBatchInfoIn msgIn = new MsgEmployeeBatchInfoIn();
            try
            {
                var company = HttpContext.Current.Session["CompanyId"];
                var user = HttpContext.Current.Session["UserName"];
                if ( company != null )
                    msgIn.CompanyId = company.ToString();
                if ( user != null )
                    msgIn.UserId = user.ToString();

                using ( SFTSoapService service = new SFTSoapService( logger ) )
                {
                    MsgEmployeeBatchInfoOut msgOut = ( MsgEmployeeBatchInfoOut ) service.DoService<MsgEmployeeBatchInfoOut>( msgIn );
                    if ( msgOut != null )
                    {
                        EmployeeBatchInfoRecord record;
                        for ( int i = 0 ; i < msgOut.Data.RecodeList.Count ;i++ )
                        {
                            record = new EmployeeBatchInfoRecord();
                            record.Code = msgOut.Data.RecodeList[i].Code;
                            record.Name = msgOut.Data.RecodeList[i].Name;

                            output.RecordList.Add( record );
                        }

                        output.Result = msgOut.Result;
                        output.Code = msgOut.Exception.Code;
                        output.SysMsg = msgOut.Exception.SysMsg;
                        output.MesMsg = msgOut.Exception.MesMsg;
                        output.Stack = msgOut.Exception.Stack;
                    }
                }
            }
            catch ( WebException webEx )
            {
                output.MesMsg = webEx.Message;
                output.SysMsg = webEx.Message;
                logger.Error( webEx.ToString() );
            }
            catch ( Exception ex )
            {
                output.MesMsg = ServiceMsg.DEFAULT_SYSMSG;
                output.SysMsg = ServiceMsg.DEFAULT_SYSMSG;
                logger.Error( ex.ToString() );
            }

            logger.Info( "GetEmployeeBatchInfo End" );
            return output;
        }
    }
}