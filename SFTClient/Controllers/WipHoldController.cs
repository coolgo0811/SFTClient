﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SFTClient.Controllers
{
    public class WipHoldController : Controller
    {
        // GET: WipHold
        public ActionResult Index( string moid )
        {
            if ( Const.CHECK_AUTHORITY )
            {
                Authority authority = new Authority();
                if ( authority.CheckSessionExist() == false )
                    return Redirect( Const.SIGNIN_PAGE );
            }

            if ( HttpContext.Session["UserName"] != null )
                ViewData["UserName"] = Session["UserName"];

            if ( string.IsNullOrEmpty( moid ) )
            {
                TempData["MainPage"] = true;
            }
            else
            {
                ViewData["Moid"] = moid;
                TempData["MainPage"] = false;   // means current page is a subpage within equipment integration
            }

            Session.Add( "Page", Page.WIPHOLD );

            var auth = Session["Auth"];
            if ( auth != null && Convert.ToBoolean( auth ) == true )
            {
                return View();
            }

            return RedirectToAction( "Login", "Home" );
        }
    }
}