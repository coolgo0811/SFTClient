﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace SFTClient.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index( string hwnd )
        {
            if ( Const.CHECK_AUTHORITY )
            {
                if ( checkValidate() == false )
                    return Redirect( Const.SIGNIN_PAGE );
            }

            HttpContext.Session["Auth"] = false;
            HttpContext.Session["UserName"] = string.Empty;
            HttpContext.Session["CompanyId"] = string.Empty;
            HttpContext.Session["Page"] = string.Empty;
            HttpContext.Session["Equipment"] = string.Empty;

            if ( string.IsNullOrEmpty( hwnd ) == false )
                HttpContext.Session["Hwnd"] = hwnd;

            MyConfig.Load( System.Web.HttpContext.Current.Server.MapPath( @"~\config\" ) + Const.SYSTEM_SETTING_FILE );
            HttpContext.Session["CompanyId"] = MyConfig.CompanyId;

            // load version
            Assembly assembly = Assembly.LoadFrom( System.Web.HttpContext.Current.Server.MapPath( @"~\bin\" ) + "SFTClient.dll" );
            ViewData["Version"] = assembly.GetName().Version.ToString();

            string version = string.Empty;
            string filePath = string.Empty;
            if ( MyConfig.LiveUpdate )  // when setting is enabled, redirect to LiveUpdate
            {
                if ( LiveUpdate.CheckVersion( ref version, ref filePath ) )
                    return RedirectToAction( "Update", "Home" );
            }

            // enabled/disabled homepage buttons by ststem setting
            // Model (0: Normal, 1: Equipment Integration, ...)
            if ( MyConfig.EquipTrackinoutModel )
                ViewData["Model"] = 1;
            else
                ViewData["Model"] = 0;

            return View();
        }

        public ActionResult Settings()
        {
            if ( Const.CHECK_AUTHORITY )
            {
                Authority auth = new Authority();
                if ( auth.CheckSessionExist() == false )
                    return Redirect( Const.SIGNIN_PAGE );
            }

            return View();
        }

        public ActionResult Login()
        {
            if ( Const.CHECK_AUTHORITY )
            {
                Authority auth = new Authority();
                if ( auth.CheckSessionExist() == false )
                    return Redirect( Const.SIGNIN_PAGE );
            }

            string version = string.Empty;
            string filePath = string.Empty;

            if ( MyConfig.LiveUpdate )  // when setting is enabled, redirect to LiveUpdate
            {
                if ( LiveUpdate.CheckVersion( ref version, ref filePath ) )
                    return RedirectToAction( "Update", "Home" );
            }

            string userName = MyConfig.UserName;
            string password = MyConfig.Password;

            ViewData["UserName"] = userName;
            ViewData["Password"] = password;

            return View();
        }

        public ActionResult Update()
        {
            if ( Const.CHECK_AUTHORITY )
            {
                Authority auth = new Authority();
                if ( auth.CheckSessionExist() == false )
                    return Redirect( Const.SIGNIN_PAGE );
            }

            return View();
        }

        #region >>> private Method <<<

        private bool checkValidate()
        {
            bool bIsPass = false;
            string szProjName, szNodeName, szWaPath;

            NameValueCollection nvc = Request.Form;
            string codepage = nvc["codePage"];

            if ( !string.IsNullOrEmpty( nvc["projName"] ) && !string.IsNullOrEmpty( nvc["nodeName"] )
                && !string.IsNullOrEmpty( nvc["waPath"] ) )
            {
                NameValueCollection parsedNvc = HttpUtility.ParseQueryString(
                Request.RawUrl.Substring( Request.RawUrl.IndexOf( '?' ) + 1 ), Encoding.GetEncoding( Convert.ToInt32( codepage ) ) );
                string decProjName = parsedNvc["proj"];
                string decNodeName = parsedNvc["node"];

                szProjName = decProjName;
                szNodeName = decNodeName;
                szWaPath = nvc["waPath"];

                Session.Add( Const.SESSION_PROJ_NAME, szProjName );
                Session.Add( Const.SESSION_NODE_NAME, szNodeName );
                Session.Add( Const.SESSION_WAPATH, szWaPath );
                bIsPass = true;
            }
            else if ( Session[Const.SESSION_PROJ_NAME] != null && Session[Const.SESSION_NODE_NAME] != null
                && Session[Const.SESSION_WAPATH] != null )
            {
                szProjName = Convert.ToString( Session[Const.SESSION_PROJ_NAME] );
                szNodeName = Convert.ToString( Session[Const.SESSION_NODE_NAME] );
                szWaPath = Convert.ToString( Session[Const.SESSION_WAPATH] );

                bIsPass = true;
            }
            else
            {
                bIsPass = false;
            }
            return bIsPass;
        }

        #endregion
    }
}
