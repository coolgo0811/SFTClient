﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SFTClient.Controllers
{
    public class SimpleTrackController : Controller
    {
        // GET: SimpleWork
        public ActionResult Index()
        {
            if ( Const.CHECK_AUTHORITY )
            {
                Authority authority = new Authority();
                if ( authority.CheckSessionExist() == false )
                    return Redirect( Const.SIGNIN_PAGE );
            }

            if ( HttpContext.Session["UserName"] != null )
                ViewData["UserName"] = Session["UserName"];

            ViewData["Equipment"] = MyConfig.Equipment;

            Session.Add( "Page", Page.SIMPLETRACK );

            var auth = Session["Auth"];
            if ( auth != null && Convert.ToBoolean( auth ) == true )
            {
                return View();
            }

            return RedirectToAction( "Login", "Home" );
        }

        public ActionResult SimpleTrackForm( string equipment )
        {
            if ( Const.CHECK_AUTHORITY )
            {
                Authority authority = new Authority();
                if ( authority.CheckSessionExist() == false )
                    return Redirect( Const.SIGNIN_PAGE );
            }

            var auth = Session["Auth"];
            if ( auth != null && Convert.ToBoolean( auth ) == true )
            {
                Session.Add( "Equipment", equipment );
                return View();
            }

            return RedirectToAction( "Login", "Home" );
        }
    }
}