﻿/**
 * Created by seven on 15/5/21.
 */
$(function (wa, global) {

  var ui =  wa.ui;
  var common = wa.common;

  var $tableList = $('.table-list');
  var $listRow = $('.list-row');
  var listRowSize = $listRow.size();
  var $deletBtn = $('a.delete');

  var pageIndex = 0; //current page
  var visibleLines = [];
  var itemPerPage = 5;
  var maxPage = Math.ceil(listRowSize/itemPerPage) - 1;

  var checknum = 0;

  var maxQty = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  var trackObj = {
        RecordList:[]
  };
  var recodeList = trackObj.RecordList;
  var checkObjs = [];
  var $prevPage = $('#prevPage');
  var $nextPage = $('#nextPage');
  var $trackContent = $(".track-content");
  var $trackQty = $('.track-num');

  var barCodeStr = "";
  var focusPage = "TrackinMoid";
  var moidIndex = 0;

  init();
  bindEvent();


  function init() {
    common.autoLogout();

    pageBtnDisplay();

    //show hide page
    visibleLines = ui.showTablePage(pageIndex, $tableList, visibleLines, itemPerPage, listRowSize);

    $tableList.find('.list-row').find('input:checkbox').each(function(index, elm){

      checkObjs.push($(elm));
      $(elm).iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
      });


      //add or remove selected class on row
      $(elm).on('ifChecked', function(event){
        checknum = checknum + 1;
        $(this).closest('tr').addClass("selected");
        if($deletBtn.hasClass('hide')){
          $deletBtn.removeClass('hide');
        }
      });
      $(elm).on('ifUnchecked', function(event){
        checknum = checknum - 1;
        $(this).closest('tr').removeClass("selected");
        if(checknum===0){
          $deletBtn.addClass('hide');
        }
      });
    });

    //disable all moid --autofocus
    $trackContent.prop('disabled', true);
  }

  function bindEvent(){
    //--autofocus
      $(document).on("keypress", function (e) {
      var keyCode = e.keyCode || e.which;
      var saveNextIndex = 0;
      /*if(keyCode === 186) keyCode = 59;
      if(keyCode === 189) keyCode = 45;
      if(keyCode === 187) keyCode = 61;*/
      if(keyCode === 13){
        //fill value in input
        switch (focusPage){
          case "TrackinMoid":
            var listRow = $('.table-list').find('.list-row');
            listRow.each(function(index,elm) {
              var $e_id = $(elm).find(".track-content");
              if($e_id.val()!==""){
                saveNextIndex+=1;
              }
            });
            if(saveNextIndex<listRowSize) {
              $('.track-content').eq(saveNextIndex).val(barCodeStr).change();
            }
            barCodeStr = "";
            break;
          default :
            break;
        }

      }else{
        barCodeStr = barCodeStr + String.fromCharCode(keyCode);
      }
    });

    $trackQty.on('mousedown', function(){
      var self = $(this);
      var list = $(this).closest('.list-row');
      var $singleMoid = list.find('.track-content');

      if($singleMoid.val()=="") {
        self.prop('disabled', true);
      }else{
        self.prop('disabled', false);
        self.keyboard({
          layout: 'custom',
          restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
          preventPaste : true,  // prevent ctrl-v and right click
          autoAccept : true,
          display: {
            'b': '\u2190:Backspace',
            'bksp': 'Bksp:Backspace'
          },
          customLayout: {
            'default': [
              '{clear} {dec} {b}',
              '7 8 9',
              '4 5 6',
              '1 2 3',
              '0 {a} {c}'
            ]
          }
        });
      }

    });

    $nextPage.on('click', function(e){
      e.preventDefault();
      pageIndex+=1;
      pageIndex = Math.min(pageIndex, maxPage);
      visibleLines = ui.showTablePage(pageIndex, $tableList, visibleLines, itemPerPage, listRowSize);
      pageBtnDisplay();
    });

    $prevPage.on('click', function(e){
      e.preventDefault();
      pageIndex-=1;
      if(pageIndex<0){
        pageIndex = 0;
      }
      visibleLines = ui.showTablePage(pageIndex, $tableList, visibleLines, itemPerPage, listRowSize);
      pageBtnDisplay();
    });

    //----- Submit
    $("#trackinBtn").on('click',function(){
      var submitObj = {
        RecordList:[]
      };
      var tmpRecordList = submitObj.RecordList;


      $listRow.each(function(index,elm) {
        var $r_content = $(elm).find(".track-content");
        var $r_num = $(elm).find(".track-num");

        if($r_content.val()!==""||$r_num.val()!=="")
        {
          var trackFullStr = $r_content.val();
          var item = {
            Moid:trackFullStr,
            Qty:$r_num.val()
          };
          tmpRecordList.push(item);
        }

      });


      console.log('submitObj', submitObj);

      $.ajax({
          type: "POST",
          dataType: "json",
          url: "/SFTClient/api/WorkReport/Trackin/",
          data: JSON.stringify(submitObj),
          contentType: "application/json; charset=utf-8",
          beforeSend: function () { WA.common.ajaxBlockUI(); },
          complete: function () { $.unblockUI(); },
          success: function (output) {
              if (output.Result == "success") {
                  ui.modalView({
                      modalHeader: "系統訊息",
                      modalBody: output.SysMsg,
                      modalFooter: "msgMode",
                      onClose: function(){common.logout();},
                      size: { w: 350, h: 200 }
                  });
              }
              else {
                  ui.modalView({
                      modalHeader: "系統訊息",
                      modalBody: output.SysMsg,
                      modalFooter: "msgMode",
                      alert: true,
                      size: { w: 350, h: 200 }
                  });
              }
          },
          error: function (error) {
              ui.modalView({
                  modalHeader: "系統訊息",
                  modalBody: error.statusText,
                  modalFooter: "msgMode",
                  alert: true,
                  size: { w: 350, h: 200 }
              });
          }
      });
    });

    curdAction();

  }

  function curdAction(){

    //----- Add  row
    $trackContent.on('change', function(){
      //get row index
      var self = $(this);
      var list = $(this).closest('.list-row');
      var listIndex = $tableList.find('.list-row').index(list);
      var moidStr = self.val();
      var $moid = list.find('.track-content');
      var $qty = list.find('.track-num');
      var $maxQty = list.find('.track-maxnum');
      //recodeList[listIndex].Moid = moidStr;

      // check format
      if (common.checkMoIdFormat(self) == false)
          return;

      // check duplicate item
      var repeatFlag = true;
      var moidBuff;
      $listRow.each(function (index, elm) {
        if(index !== listIndex){
          var $moid = $(elm).find(".track-content");
          if ($moid.val() == "")
              return false;
          moidBuff = moidStr.split(";");
          if (moidBuff[0] === $moid.val().split(";")[0]) {
            self.val("");
            ui.modalView({
              modalHeader:"系統訊息",
              modalBody: "重複的製令 !",
              modalFooter:"msgMode",
              alert:true,
              size:{w:350,h:200}
            });
            repeatFlag = false;
            return false;
          }
        }
      });
      if(!repeatFlag){
        return;
      }
      var source = {
          Moid: moidStr,
          CheckQtyType: '1'
      }
      $.ajax({
        type: "POST",
        dataType: "json",
        url: "/SFTClient/api/Check/MoCheck/",
        data: JSON.stringify(source),
        contentType: "application/json; charset=utf-8",
        beforeSend: function () { common.ajaxBlockUI(); },
        complete: function () { $.unblockUI(); },
        success: function (output) {
            if (output.Result == "success") {
                if (parseFloat(output.CheckQty) == 0) {
                    ui.modalView({
                        modalHeader:"系統訊息",
                        modalBody:'檢查失敗, 無待進批量 !',
                        modalFooter:"msgMode",
                        alert:true,
                        size:{w:350,h:200}
                    });

                    self.val("");
                }
                else {
                  recodeList[listIndex] = {};
                  recodeList[listIndex].Moid = moidStr;
                  recodeList[listIndex].Qty = output.CheckQty;

                  //auto jump page
                  jumpPageByIndex(listIndex);

                  $qty.val(output.CheckQty);
                  $maxQty.val(output.CheckQty);

                  if($moid.val()!=="") {
                    $qty.prop('disabled', false);
                  }
                }
            }
            else {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: output.SysMsg,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });

              if(global.debuggerMode) {
                recodeList[listIndex] = {};
                recodeList[listIndex].Moid = moidStr;
                recodeList[listIndex].Qty = 100;
                console.log(recodeList, "recodeList push item");


                //auto jump page
                jumpPageByIndex(listIndex);

                $qty.val(100);
                $maxQty.val(100);

                if($moid.val()!=="") {
                  $qty.prop('disabled', false);
                }

              }else {
                self.val("");
              }
            }
        },
        error: function (error) {
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: error.statusText,
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
            self.val("");
        }
      });
    });

    //----- Delete checked row
    $deletBtn.on('click',function(){
      //copy original recordList
      var recordListTmp = recodeList.slice(0);
      var recordListTmpNew = [];
      console.log(recodeList, "recodeList")

      //----- Record non-checked item and Delete
      $listRow.each(function(index,elm) {
        var $r_moid = $(elm).find(".track-content");
        var $r_qty = $(elm).find(".track-num");
        var $r_maxnum = $(elm).find(".track-maxnum");

        if ($r_moid.val() !== "") {
          if (recordListTmp[index]) {
            recordListTmp[index].Moid = $r_moid.val();
            recordListTmp[index].Qty = $r_qty.val();
            recordListTmp[index].MaxQty = $r_maxnum.val();
            recordListTmp[index].Delete = $(elm).hasClass('selected');
          }
        }
        //clear all content and num value
        $r_moid.val("");
        $r_qty.val("");
        $r_maxnum.val("");
      });

      for(var i= 0, len=recordListTmp.length;i<len;i+=1){
        if(recordListTmp[i]){
          if(!recordListTmp[i].Delete) {
            recordListTmpNew.push(recordListTmp[i]);
          }
        }
      }
      console.log(recordListTmpNew, "recordListTmpNew");


      var max_num = recordListTmpNew.length;
      //auto jump to correct page by max_num
      jumpPageByIndex(max_num-1);

      //---- Fill in table
      $listRow.each(function(index,elm){
        var $r_content = $(elm).find(".track-content");
        var $r_num = $(elm).find(".track-num");
        var $r_maxnum = $(elm).find(".track-maxnum");

        if(index < max_num) {
          $r_content.val(recordListTmpNew[index].Moid);
          $r_num.val(recordListTmpNew[index].Qty);
          $r_maxnum.val(recordListTmpNew[index].MaxQty);
        }
      });

      recodeList = recordListTmpNew;

      checknum = 0;
      $deletBtn.addClass('hide');
      for(var i = 0; i<listRowSize; i+=1){
        checkObjs[i].iCheck('uncheck');
      }

    });

  };

  $('#btnLogout').on('click', common.logout);

  $('.track-num').on('change', checkQty);
  function checkQty() {
      if (parseFloat($(this).val()) == 0)
          return;

      var list = $(this).closest('.list-row');
      var $maxQty = list.find('.track-maxnum');
      if (parseFloat($(this).val()) > parseFloat($maxQty.val())) {
          ui.modalView({
              modalHeader: "系統訊息",
              modalBody: "欲入站數量不可大於製令的可入站數量 !",
              modalFooter: "msgMode",
              alert: true,
              size: { w: 350, h: 200 }
          });
          $(this).val($maxQty.val());
      }
  }


  function pageBtnDisplay(){
    if(pageIndex >= maxPage){
      $nextPage.hide();
    }else{
      $nextPage.show();
    }
    if(pageIndex <= 0){
      $prevPage.hide();
    }else{
      $prevPage.show();
    }
  }
  function jumpPageByIndex(index){
    var currentPage = parseInt(index/itemPerPage);
    if(currentPage>maxPage) currentPage = maxPage;
    pageIndex = currentPage;
    visibleLines = ui.showTablePage(pageIndex, $tableList, visibleLines, itemPerPage, listRowSize);
    pageBtnDisplay();
  }



}(WA, this));