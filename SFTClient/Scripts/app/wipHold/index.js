﻿/**
 * Created by seven on 15/6/26.
 */
$(function (wa, global) {
  var ui = wa.ui;
  var common = wa.common;

  var $stopStatus = $('.stop-status');
  var $moid = $('.moid');
  var $btnCancel = $('#cancelStopBtn');
  var $btnAccept = $('#acceptStopBtn');
  var barCodeStr = "";


  init();
  bindEvent();

  if ($moid.val() != "") {
      $moid.change();
  }

  function init() {
    common.autoLogout();

    $stopStatus.hide();
    $btnCancel.hide();
    $btnAccept.hide();
  }
  function bindEvent(){
    //--autofocus
    $(document).on("keypress",function(e){
      var keyCode = e.keyCode || e.which;
      if(keyCode === 13){
        $moid.val("");
        $moid.val(barCodeStr);
        barCodeStr = "";
        $moid.change();
      }else{
        barCodeStr = barCodeStr + String.fromCharCode(keyCode);
      }
    });

    $moid.on('change', function () {
        var self = $(this);
        if (common.checkMoIdFormat(self) == false)
            return;
        getMoStatus(self.val());
    });
    $btnAccept.on('click', function () {
        var type;
        if ( $("#fullMoid").attr('checked') )   
            type = '1';
        else
            type = '2';

        setMoStatus('1', type);
    });
    $btnCancel.on('click', function () {
        var type;
        if ( $("#fullMoid").attr('checked') )   
            type = '1';
        else
            type = '2';
        setMoStatus('2', type);
    });

    $("#btnLogout").on('click', common.logout);

    // for go back to main page
    $("#btnBack").on('click', function () {
        //Clear All Content
        $('.content.subpage').html("");
        $('.content.main').show();
    });
  }

  function getMoStatus(moid) {
      var source = { MoId: moid };
      $.ajax({
          type: "POST",
          dataType: "json",
          url: "/SFTClient/api/GetData/MoStatus/",
          data: JSON.stringify(source),
          contentType: "application/json; charset=utf-8",
          //async: false,
          beforeSend: function () { WA.common.ajaxBlockUI(); },
          complete: function () { $.unblockUI(); },
          success: function (output) {
              if (output.Result == "success") {
                  switch(parseInt(output.Status)){
                      case 1:   // hold
                          $stopStatus.show();
                          $btnCancel.show();
                          $btnAccept.hide();
                          break;
                      case 2:   // normal
                          $stopStatus.hide();
                          $btnCancel.hide();
                          $btnAccept.show();
                          break;
                  }
              }
              else {
                  ui.modalView({
                      modalHeader: "系統訊息",
                      modalBody: output.SysMsg,
                      modalFooter: "msgMode",
                      alert: true,
                      size: { w: 350, h: 200 }
                  });
                  $moid.val("");
              }
          },
          error: function (error) {
              ui.modalView({
                  modalHeader: "系統訊息",
                  modalBody: error.statusText,
                  modalFooter: "msgMode",
                  alert: true,
                  size: { w: 350, h: 200 }
              });
              $moid.val("");
          }
      });
  }

  function setMoStatus(status, type) {
      if ($moid.val() == "")
          return;

      var source = { Status: status, ChangeType: type, RecordList: [{ MoId: $moid.val() }] };
      $.ajax({
          type: "POST",
          dataType: "json",
          url: "/SFTClient/api/StatusChange/Mo/",
          data: JSON.stringify(source),
          contentType: "application/json; charset=utf-8",
          beforeSend: function () { WA.common.ajaxBlockUI(); },
          complete: function () { $.unblockUI(); },
          success: function (output) {
              if (output.Result == "success") {
                  getMoStatus($moid.val());
              }
              else {
                  ui.modalView({
                      modalHeader: "系統訊息",
                      modalBody: output.ChangeReason,
                      modalFooter: "msgMode",
                      alert: true,
                      size: { w: 350, h: 200 }
                  });
              }
          },
          error: function (error) {
              ui.modalView({
                  modalHeader: "系統訊息",
                  modalBody: error.statusText,
                  modalFooter: "msgMode",
                  alert: true,
                  size: { w: 350, h: 200 }
              });
          }
      });
  }

}(WA, this));