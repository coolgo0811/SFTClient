﻿/**
 * Created by seven on 15/6/12.
 */
$(function (wa, global) {

  var ui = wa.ui;
  var common = wa.common;

  var $trackMoid = $('.trackout-moid');
  var barCodeStr = "";

  init();
  bindEvent();

  function init(){

  }
  function bindEvent(){
    //--autofocus
      $(document).on("keypress", function (e) {
      var keyCode = e.keyCode || e.which;
      /*if(keyCode === 186) keyCode = 59;
      if(keyCode === 189) keyCode = 45;
      if(keyCode === 187) keyCode = 61;*/
      if(keyCode === 13){
        $trackMoid.val("");
        $trackMoid.val(barCodeStr);
        barCodeStr = "";
        $trackMoid.change();
      }else{
        barCodeStr = barCodeStr + String.fromCharCode(keyCode);
        console.log(barCodeStr,"barCodeStr");
      }
    });

    $trackMoid.on('change', function () {
      var self = $(this);
      if (common.checkMoIdFormat(self) == false)
          return;

      var moidStr = self.val();
     
      var source = {
        Moid: moidStr
      };

      $.ajax({
        type: "POST",
        dataType: "json",
        url: "/SFTClient/api/Check/TrackMoCheck/",
        data: JSON.stringify(source),
        contentType: "application/json; charset=utf-8",
        beforeSend: function () { common.ajaxBlockUI(); },
        complete: function () { $.unblockUI(); },
        success: function (output) {
          if (output.Result == "success") {
            $("#qtyIn").val(output.CheckInQty);
            $("#qtyOut").val(output.CheckOutQty);
          }
          else {
            ui.modalView({
              modalHeader: "系統訊息",
              modalBody: output.SysMsg,
              modalFooter: "msgMode",
              alert: true,
              size: { w: 350, h: 200 }
            });
            clear();
          }
        },
        error: function (error) {
          ui.modalView({
            modalHeader: "系統訊息",
            modalBody: error.statusText,
            modalFooter: "msgMode",
            alert: true,
            size: { w: 350, h: 200 }
          });
          clear();
        }
      });
    });

    $("#btnLogout").on('click', common.logout);
  }

  function clear() {
      $trackMoid.val("");
      $("#qtyIn").val("");
      $("#qtyOut").val("");
  }

  }(WA, this));