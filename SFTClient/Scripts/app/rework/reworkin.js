﻿/**
 * Created by seven on 15/6/29.
 */
$(function (wa, global) {
  var ui = wa.ui;
  var common = wa.common;

  var $reworkMoid = $('.rework-moid');
  var $reworkTime = $('.rework-time');

  var focusPage = "moid";
  var barCodeStr = "";
  var recordList = {};

  var listRowSize = 10;
  var itemPerPage = 5;



  init();
  bindEvent();

  function init(){
      common.autoLogout();

  }
  function bindEvent(){
    //--autofocus
      $(document).on("keypress", function (e) {
      var keyCode = e.keyCode || e.which;
      var saveNextIndex = 0;

      /*if(keyCode === 186) keyCode = 59;
      if(keyCode === 189) keyCode = 45;
      if(keyCode === 187) keyCode = 61;*/
      if(keyCode === 13){
        switch (focusPage){
          case "moid":
            $reworkMoid.val("");
            $reworkMoid.val(barCodeStr).change();
            barCodeStr = "";
            break;
          case "exception":
            var listExRow = $('.table-list.exception').find('.list-row');
            listExRow.each(function(index,elm) {
              var $exCode = $(elm).find(".exception-code");
              if($exCode.val()!==""){
                saveNextIndex+=1;
              }
            });
            if(saveNextIndex<listRowSize) {
              $('.exception-code').eq(saveNextIndex).val(barCodeStr).change();
            }
            barCodeStr = "";
            break;
          default :
            break;
        }
      }else{
          barCodeStr = barCodeStr + String.fromCharCode(keyCode);
      }
    });

    $reworkTime.on('mousedown', function(){
      var self = $(this);

      if($reworkMoid.val()!==""){
        self.keyboard({
          layout: 'custom',
          restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
          preventPaste : true,  // prevent ctrl-v and right click
          autoAccept : true,
          display: {
            'b': '\u2190:Backspace',
            'bksp': 'Bksp:Backspace'
          },
          customLayout: {
            'default': [
              '{clear} {dec} {b}',
              '7 8 9',
              '4 5 6',
              '1 2 3',
              '0 {a} {c}'
            ]
          }
        });
      }else{
        self.prop('disabled', true);
      }
    });


    //----- Get  row
    $reworkMoid.on('change', function(){
      //console.log("change");

      //get row index
      var self = $(this);
      var moidStr =  self.val();
      //var $maxQty = list.find('.track-maxnum');
      //recordList[listIndex].Moid = moidStr;

      // check format
      if(!debuggerMode) {
        if (common.checkMoIdFormat(self) == false)
          return;
      }

      var source = {
        Moid: moidStr,
        CheckQtyType: '1'
      };
      $.ajax({
        type: "POST",
        dataType: "json",
        url: "/SFTClient/api/Check/MoCheck/",
        data: JSON.stringify(source),
        contentType: "application/json; charset=utf-8",
        beforeSend: function () { WA.common.ajaxBlockUI(); },
        complete: function () { $.unblockUI(); },
        success: function (output) {
          if (output.Result == "success")
          {
            if (parseInt(output.CheckQty) == 0) {
              ui.modalView({
                modalHeader:"系統訊息",
                modalBody:'檢查失敗, 無待進批量 !',
                modalFooter:"msgMode",
                alert:true,
                size:{w:350,h:200}
              });

              self.val("");
              $reworkTime.val("");
              $('#exceptionSumQty').val("");
              $('#exceptionListNum').val("");
              recordList = {};

            }
            else {

              recordList.Moid = moidStr;
              recordList.ReworkWorkTime = 0;
              recordList.Exception = [];

              $reworkTime.val(recordList.ReworkWorkTime);
              $reworkTime.prop("disabled", false);

            }
          }
          else
          {

            ui.modalView({
              modalHeader:"系統訊息",
              modalBody:output.SysMsg,
              modalFooter:"msgMode",
              alert:true,
              size:{w:350,h:200}
            });

            if(global.debuggerMode) {

              recordList.Moid = moidStr;
              recordList.ReworkWorkTime = 60;
              recordList.Exception = [];

              $reworkTime.val(recordList.ReworkWorkTime);
              $reworkTime.prop("disabled", false);

            }else{
              self.val("");
              $reworkTime.val("");
              $('#exceptionSumQty').val("");
              $('#exceptionListNum').val("");
              recordList = {};
            }
          }
        },
        error: function (error) {
          ui.modalView({
            modalHeader:"系統訊息",
            modalBody: error.statusText,
            modalFooter:"msgMode",
            alert:true,
            size:{w:350,h:200}
          });
          self.val("");
          $reworkTime.val("");
          $('#exceptionSumQty').val("");
          $('#exceptionListNum').val("");
          recordList = {};
        }
      });
    });

    //click detail Btn
    $('#exDetailBtn').on('click',function(){
      if($reworkMoid.val() === ""){
        return;
      }

      $.ajax({
          url: "/SFTClient/Trackinout/TableListException",
          method: "GET",
          dataType: "html",
          success: function (data) {

            $('.content.main').hide();
            $('.content.subpage').html(data);
            creatTableListException();
          },
          error: function (error) {
            ui.modalView({
              modalHeader: "系統訊息",
              modalBody: error.statusText,
              modalFooter: "msgMode",
              alert: true,
              size: { w: 350, h: 200 }
            });
          }
        }
      );
    });

    $('#btnRework').on('click', function () {
        recordList.Qty = $('#exceptionSumQty').val();
        recordList.ReworkWorkTime = $reworkTime.val();
        var source = {
            ReworkType: '1',
            RecordList: [recordList]
        };

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/SFTClient/api/WorkReport/Rework/",
            data: JSON.stringify(source),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () { common.ajaxBlockUI(); },
            complete: function () { $.unblockUI(); },
            success: function (output) {
                if (output.Result == "success") {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: output.SysMsg,
                        modalFooter: "msgMode",
                        onClose: function () { common.logout(); },
                        size: { w: 350, h: 200 }
                    });
                }
                else {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: output.SysMsg,
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                }
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
            }
        });
    });

    $('#btnLogout').on('click', common.logout);
  }

  function creatTableListException(){
    //debugger;
    var exCodeList = [];
    /// load default data to workerList

    exCodeList = recordList.hasOwnProperty("Exception") ?recordList.Exception:[];

    var visibleExLines = [];
    var checkExObjs = [];
    var $tableExList = $('.table-list.exception');
    var $listExRow = $tableExList.find('.list-row');
    var $prevExceptionPage = $('#prevExceptionPage');
    var $nextExceptionPage = $('#nextExceptionPage');
    var $saveExCloseBtn = $('#saveExCloseBtn');
    var $exDeleteBtn = $('#exDeleteBtn');

    var $exceptionCode = $('.exception-code');
    var $exceptionName = $('.exception-name');
    var $exceptionQty = $('.exception-qty');

    var pagenavException =  new wa.pagenav($tableExList, $exDeleteBtn, $nextExceptionPage, $prevExceptionPage, visibleExLines, itemPerPage, listRowSize);

    //clear focus barcode string
    barCodeStr = "";
    focusPage = "exception";


    ui.bindInputStyle();
    checkExObjs = pagenavException.bindCheckEvent(checkExObjs);

    //--- init Obj- show hide row
    visibleExLines = pagenavException.showTablePage();
    pagenavException.navBtnDisplay();
    bindPageBtnEvent(pagenavException, $nextExceptionPage, $prevExceptionPage);

    //--- init Obj- load recorded Data
    if(exCodeList.length!==0) {
      //loadEmployeeData();
      $listExRow.each(function(index,elm) {
        var $exCode = $(elm).find(".exception-code");
        var $exName = $(elm).find(".exception-name");
        var $exQty = $(elm).find(".exception-qty");
        if(exCodeList[index]) {
          $exCode.val(exCodeList[index].Code);
          $exName.val(exCodeList[index].Name);
          $exQty.val(exCodeList[index].Qty);
        }else{
          $exCode.add($exName).add($exQty).prop("disabled", true);
        }
      });
    }else{
      $exceptionCode.add($exceptionName).add($exceptionQty).prop("disabled", true);
    }

    //dynamic init keyboard when click input
    $exceptionQty.on('mousedown', function(){
      var self = $(this);
      var list = $(this).closest('.list-row');
      var $singleCode = list.find('.exception-code');
      if($singleCode.val()!==""){
        self.keyboard({
          layout: 'custom',
          restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
          preventPaste : true,  // prevent ctrl-v and right click
          autoAccept : true,
          display: {
            'b': '\u2190:Backspace',
            'bksp': 'Bksp:Backspace'
          },
          customLayout: {
            'default': [
              '{clear} {dec} {b}',
              '7 8 9',
              '4 5 6',
              '1 2 3',
              '0 {a} {c}'
            ]
          }
        });
      }else{
        self.prop('disabled', true);
      }
    });


    //----- employee Delete checked row
    $exDeleteBtn.on('click',function(){

      var exCodeListTmp = [];
      var exCodeListTmpNew = [];
      //----- Record non-checked item and delete
      $listExRow.each(function(index,elm){
        var $exCode = $(elm).find(".exception-code");
        var $exName = $(elm).find(".exception-name");
        var $exQty = $(elm).find(".exception-qty");

        if($exCode.val()!=="") {
          exCodeListTmp[index] = {};
          exCodeListTmp[index].Code = $exCode.val();
          exCodeListTmp[index].Name = $exName.val();
          exCodeListTmp[index].Qty = $exQty.val();
          exCodeListTmp[index].Delete = $(elm).hasClass('selected');
        }

        //clear item value
        $exCode.val("");
        $exName.val("");
        $exQty.val("0");
      });
      // --- Delete selected object, select item Delete property is false
      for(var i= 0, len=exCodeListTmp.length;i<len;i+=1){
        if(exCodeListTmp[i]){
          if(!exCodeListTmp[i].Delete) {
            exCodeListTmpNew.push(exCodeListTmp[i]);
          }
        }
      }

      var max_num = exCodeListTmpNew.length;
      //auto jump to correct page by max_num
      pagenavException.jumpPageByIndex(max_num-1);

      //---- Refill in table
      $listExRow.each(function(index,elm){
        var $exCode = $(elm).find(".exception-code");
        var $exName = $(elm).find(".exception-name");
        var $exQty = $(elm).find(".exception-qty");

        if(index < max_num) {
          $exCode.val(exCodeListTmpNew[index].Code);
          $exName.val(exCodeListTmpNew[index].Name);
          $exQty.val(exCodeListTmpNew[index].Qty);
        }
      });

      exCodeList = exCodeListTmpNew;


      pagenavException.checknum = 0;
      pagenavException.deleteBtn.addClass('hide');
      for(var i = 0; i<listRowSize; i+=1){
        checkExObjs[i].iCheck('uncheck');
      }
    });

    // ----Update, hide exception page
    // and save data to front page
    $saveExCloseBtn.on('click', function(){
      var exQtySum = 0;
      //clear
      exCodeList = [];

      $listExRow.each(function(index,elm) {
        var $exCode = $(elm).find(".exception-code");
        var $exName = $(elm).find(".exception-name");
        var $exQty = $(elm).find(".exception-qty");

        if($exCode.val()!==""){
          var ex = {
            Code:$exCode.val(),
            Name:$exName.val(),
            Qty: $exQty.val()
          };
          exCodeList.push(ex);
          exQtySum += parseInt($exQty.val());
        }
      });

      //Update
      //if(exCodeList.length>0) {
        recordList.Exception = exCodeList;
        $('#exceptionSumQty').val(exQtySum);
        $('#exceptionListNum').val(exCodeList.length);
      //}

      pagenavException.checknum = 0;
      pagenavException.deleteBtn.addClass('hide');
      for(var i = 0; i<listRowSize; i+=1){
        checkExObjs[i].iCheck('uncheck');
      }

      //Clear All Content
      $('.content.subpage').html("");
      $('.content.main').show();

      //clear focus barcode string
      barCodeStr = "";
      focusPage = "moid";

      //Rebind iCheck Event for main form
      //checkObjs = pagenavMain.bindCheckEvent(checkObjs);
    });

    // ------ Get exception name
    $exceptionCode.on('change', function () {
      //get row index
      var self = $(this);
      var list = $(this).closest('.list-row');
      var exIndex = $tableExList.find('.list-row').index(list);
      var $singleExName = list.find('.exception-name');
      var $exQty = list.find('.exception-qty');

      if (common.checkExceptionFormat(self, "rework") == false)
          return;

      var repeatFlag = true;
      $listExRow.each(function(index,elm){
        if(index !== exIndex){
          var $excode = $(elm).find(".exception-code");
          if (self.val() === $excode.val()) {
            self.val("");
            ui.modalView({
              modalHeader:"系統訊息",
              modalBody:"重複資料，請重新輸入 !",
              modalFooter:"msgMode",
              alert:true,
              size:{w:350,h:200}
            });
            repeatFlag = false;
          }
        }
      });
      if (!repeatFlag) {
          return;
      }

      var source = { CheckType: '003', Code: self.val() };

      $.ajax({
        type: "POST",
        dataType: "json",
        url: "/SFTClient/api/Check/ExceptionCheck/",
        data: JSON.stringify(source),
        contentType: "application/json; charset=utf-8",
        beforeSend: function () { common.ajaxBlockUI(); },
        complete: function () { $.unblockUI(); },
        success: function (data) {
          if (data.Result == "success") {
            //auto jump to correct page by Index
            pagenavException.jumpPageByIndex(exIndex);

            $singleExName.val(data.Name);
            $exceptionQty.prop("disabled", false);

          }
          else {

            ui.modalView({
              modalHeader: "系統訊息",
              modalBody: data.SysMsg,
              modalFooter: "msgMode",
              alert: true,
              size: { w: 350, h: 200 }
            });
            if(debuggerMode){
              //auto jump to correct page by Index
              pagenavException.jumpPageByIndex(exIndex);
              $singleExName.val("冷卻系統故障");
              $exceptionQty.prop("disabled", false);
            }else {
              self.val("");
            }
          }
        },
        error: function (error) {
          ui.modalView({
            modalHeader: "系統訊息",
            modalBody: error.statusText,
            modalFooter: "msgMode",
            alert: true,
            size: { w: 350, h: 200 }
          });
          self.val("");
        }
      });
    });
  }

  function bindPageBtnEvent(pagenavObj ,nextEmPage, prevEmPage){
    nextEmPage.on('click', {navObj:pagenavObj}, onNextPage);
    prevEmPage.on('click', {navObj:pagenavObj}, onPrevPage);
  }
  function onNextPage(event){
    event.preventDefault();
    var pagenavObj = event.data.navObj;
    pagenavObj.nextPageHandlr();
  }
  function onPrevPage(event){
    event.preventDefault();
    var pagenavObj = event.data.navObj;
    pagenavObj.prevPageHandlr();
  }


}(WA, this));