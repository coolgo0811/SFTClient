﻿
$(function (wa, global) {
    var ui = wa.ui;
    var common = wa.common;

    var $txtEquipment = $('#txtEquipment');
    var barCodeStr = "";

    init();
    bindEvent();

    function init(){
        common.autoLogout();
    }
    function bindEvent(){
        //--autofocus
        $(document).on("keypress", function (e) {
            var keyCode = e.keyCode || e.which;
            /*if(keyCode === 186) keyCode = 59;
            if(keyCode === 189) keyCode = 45;
            if(keyCode === 187) keyCode = 61;*/
            if(keyCode === 13){
                $txtEquipment.val("");
                $txtEquipment.val(barCodeStr);
                barCodeStr = "";
                $txtEquipment.change();
            }else{
                barCodeStr = barCodeStr + String.fromCharCode(keyCode);
            }
        });

        $txtEquipment.on('change', function () { common.checkEquipIdFormat($(this)); });
        $("#btnReworkIn").on('click', function () {
            location.href = "/SFTClient/Rework/ReworkIn/?equipment=" + $txtEquipment.val();
        });
        $("#btnReworkOut").on('click', function () {
            location.href = "/SFTClient/Rework/ReworkOut/?equipment=" + $txtEquipment.val();
        });

        $("#btnLogout").on('click', common.logout);
    }

}(WA, this));
