﻿/**
 * Created by seven on 15/6/29.
 */

$(function (wa, global) {
  var ui = wa.ui;
  var common = wa.common;

  var $deviceCode = $('#device-code');
  var $deviceStatus = $('.device-status');
  var $status = $('.status');
  var barCodeStr = "";
  var activeStatus = "normal";
  var statusMap = {
    normal:"正常",
    offWork:"下班",
    setup: "設置",
    breakdown:"故障",
    pause: "暫停",
    unused: "閒置"
  };
  var statusCodeMap = {
      normal: 99,
      offWork: 3,
      setup: 2,
      breakdown: 1,
      pause: 4,
      unused: 0
  };

  init();
  bindEvent();

  function init() {
      common.autoLogout();

      clearStatus();

      if ($deviceCode.val() != "") {
          getEquipStatus($deviceCode);
      }
  }
  function bindEvent() {
    //--autofocus
    $(document).on("keypress",function(e){
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) {
          $deviceCode.val("");
          $deviceCode.val(barCodeStr);
          barCodeStr = "";
          $deviceCode.change();
      } else {
          barCodeStr = barCodeStr + String.fromCharCode(keyCode);
      }
    });

    $deviceCode.on('change', function () {
        if(common.checkEquipIdFormat($(this)))
            getEquipStatus($deviceCode);
        else
            clearStatus();
    });


    $status.on('click',function(){
        if(checkValidStatus($(this)))
            setEquipStatus($(this));
    });

    $("#btnLogout").on('click', common.logout);
    
    // for go back to main page
    $("#btnBack").on('click', function () {
        //Clear All Content
        $('.content.subpage').html("");
        $('.content.main').show();
    });
  }

  function clearStatus() {
      if ($status.hasClass('active')) {
          $status.removeClass('active');
      }
      $deviceStatus.text("閒置");
  }

  function checkValidStatus(statusCtrl) {
      if ($deviceCode.val() == "")
          return false;
      var new_activeStatus = statusCtrl.get(0).id;
      if (activeStatus == "unused") {
          if (new_activeStatus == "normal") {
              return false;
          }
      }

      activeStatus = new_activeStatus;

      return true;
  }

  //show status on page
  function showEquipStatus(status) {
      for (var prop in statusMap) {
          if (prop === status) {
              $deviceStatus.text(statusMap[prop]);
          }
      }
  }

  function getEquipStatus(equip) {
      if (equip.val() == "")
          return;

      var source = { Equipment: equip.val() };
      $.ajax({
          type: "POST",
          dataType: "json",
          url: "/SFTClient/api/GetData/EquipmentStatus/",
          data: JSON.stringify(source),
          contentType: "application/json; charset=utf-8",
          beforeSend: function () { WA.common.ajaxBlockUI(); },
          complete: function () { $.unblockUI(); },
          success: function (output) {
              if (output.Result == "success") {
                  if ($status.hasClass('active'))
                      $status.removeClass('active');
                  switch (parseInt(output.Status)) {
                      case 0:     // unused
                          activeStatus = "unused";
                          break;
                      case 1:     // repair, breakdown
                          activeStatus = "breakdown";
                          $('#breakdown').addClass('active');
                          break;
                      case 2:     // setup
                          activeStatus = "setup";
                          $('#setup').addClass('active');
                          break;
                      case 3:     // off work
                          activeStatus = "offWork";
                          $('#offWork').addClass('active');
                          break;
                      case 4:     // pause
                          activeStatus = "pause";
                          $('#pause').addClass('active');
                          break;
                      case 99:     // normal
                          activeStatus = "normal";
                          $('#normal').addClass('active');
                          break;
                  }
                  showEquipStatus(activeStatus);
              }
              else {
                  ui.modalView({
                      modalHeader: "系統訊息",
                      modalBody: output.SysMsg,
                      modalFooter: "msgMode",
                      alert: true,
                      size: { w: 350, h: 200 }
                  });
                  equip.val("");
                  clearStatus();
              }
          },
          error: function (error) {
              ui.modalView({
                  modalHeader: "系統訊息",
                  modalBody: error.statusText,
                  modalFooter: "msgMode",
                  alert: true,
                  size: { w: 350, h: 200 }
              });
              equip.val("");
              clearStatus();
          }
      });
  }

  function setEquipStatus() {
      for (var prop in statusCodeMap) {
          if (prop === activeStatus) {
              statusCode = statusCodeMap[prop];
          }
      }

      var source = { Equipment: $deviceCode.val(), Status: statusCode };
      $.ajax({
          type: "POST",
          dataType: "json",
          url: "/SFTClient/api/StatusChange/Equipment/",
          data: JSON.stringify(source),
          contentType: "application/json; charset=utf-8",
          beforeSend: function () { WA.common.ajaxBlockUI(); },
          complete: function () { $.unblockUI(); },
          success: function (output) {
              if (output.Result == "success") {
                  getEquipStatus($deviceCode);
              }
              else {
                  ui.modalView({
                      modalHeader: "系統訊息",
                      modalBody: output.SysMsg,
                      modalFooter: "msgMode",
                      alert: true,
                      size: { w: 350, h: 200 }
                  });
              }
          },
          error: function (error) {
              ui.modalView({
                  modalHeader: "系統訊息",
                  modalBody: error.statusText,
                  modalFooter: "msgMode",
                  alert: true,
                  size: { w: 350, h: 200 }
              });
          }
      });
  }

}(WA, this));