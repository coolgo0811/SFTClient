﻿/**
 * Created by seven on 15/5/29.
 */
$(function (wa, global) {

  global.debuggerMode = false;
  var ui = wa.ui;
  var common = wa.common;

  var $dhcp = $('#dhcp'),
    $tc_lang = $('#tc-lang'),
    $sc_lang = $('#sc-lang'),
    $en_lang = $('#en-lang'),
    $gstate1 = $('#gstate1'),
    $gstate2 = $('#gstate2'),
    $gstate3 = $('#gstate3'),
    $gstate4 = $('#gstate4');

  //debugger;
  ui.bindVirtualKB();

  ui.bindInputStyle();

  $tc_lang.iCheck('check');
  $sc_lang.iCheck('disable');
  $en_lang.iCheck('disable');

  $('.iradio_square-blue.disabled').next().css({ 'color': '#999' });

  Number.prototype.padLeft = function (base, chr) {
      var len = (String(base || 10).length - String(this).length) + 1;
      return len > 0 ? new Array(len).join(chr || '0') + this : this;
  }

  var now, hours, minutes, seconds, timeValue;
  function showtime() {
      now = new Date();
      hours = now.getHours();
      minutes = now.getMinutes();
      seconds = now.getSeconds();
      timeValue = now.getFullYear() + "-" + (now.getMonth() + 1).padLeft() + "-" + now.getDate().padLeft() + " ";
      timeValue += (hours >= 12) ? "PM " : "AM ";
      timeValue += ((hours > 12) ? (hours - 12).padLeft() : hours.padLeft()) + ":";
      timeValue += minutes.padLeft() + ":";
      timeValue += seconds.padLeft();
      $("#clock").text(timeValue);
      setTimeout(showtime, 1000);
  }
  showtime();

}(WA, this));