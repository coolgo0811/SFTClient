﻿/**
 * Created by seven on 15/5/21.
 */
$(function (wa, global) {

  var $tableList = $('.table-list.main');
  var $prevPage = $('#prevPage');
  var $nextPage = $('#nextPage');
  var $mainDeleteBtn = $('#mainDeleteBtn');
  var $listRow = $tableList.find('.list-row');
  var listRowSize = $listRow.size();
  var itemPerPage = 5;
  var visibleLines = [];
  var recordDetailTotal = {};
  var barCodeStr = "";
  var focusPage = "moid";
  var moidIndex = 0;
  var $listRowFirst = $tableList.find('.list-row').eq(0);

  var checkObjs = [];
  var checknum = 0;

  var trackObj = {
    RecordList:[]
  };
  var recordList = trackObj.RecordList;


  var $trackMoid = $(".trackout-content");
  var $trackAvrTime = $(".trackout-person-time");
  var $trackQty = $('.trackout-num');
  var $trackDeviceTime = $(".trackout-device-time");
  var $trackManNum = $('.trackout-person-num');

  var $trackAvrTimeAll = $('#personTimeBtnTop');
  var $deviceTimeAll = $('#deviceTimeBtnTop');
  var $trackManNumAll = $('#personNumBtnTop');

  var ui = wa.ui;
  var common = wa.common;
  var pagenavMain =  new wa.pagenav($tableList, $mainDeleteBtn, $nextPage, $prevPage, visibleLines, itemPerPage, listRowSize);

    // sub-page setting /////////
  var subPageHnd = new wa.subpagehnd();
  var mainPageinfo = {
      recordList: recordList,
      bindPageBtnEvent: bindPageBtnEvent,
      clearBarCodeStr: clearBarCodeStr,
      setFocusPage: setFocusPage,
      itemPerPage: itemPerPage,
      listRowSize: listRowSize,
      pagenavMain: pagenavMain,
      checkObjs: checkObjs,
      enableProp: enableProp,
      saveRecordDetailTotal: saveRecordDetailTotal
  };
  function saveRecordDetailTotal(r) { recordDetailTotal = r; }
  function clearBarCodeStr() { barCodeStr = ""; }
  function setFocusPage(val) { focusPage = val; }
  function enableProp(enable) { $trackAvrTime.add($trackManNum).prop('disabled', !enable); }
  function employeePageSubmit() {
      $listRow.each(function (index, elm) {

          var $totalManWorkTime = $(elm).find('.trackout-person-time-sum');
          var $avrWorkTime = $(elm).find('.trackout-person-time');
          var $manNum = $(elm).find('.trackout-person-num');

          //debugger;

          if (recordList[index]) {
              $totalManWorkTime.val(recordList[index].ManWorkTime);
              $avrWorkTime.val(recordList[index].AvrWorkTime);
              $manNum.val(recordList[index].ManNum);
          }
      });
  }
    // sub-page setting /////////

  init();
  bindEvent();


  function init() {
    common.autoLogout();

    pagenavMain.navBtnDisplay();

    //show hide page
    visibleLines = pagenavMain.showTablePage();
    //bind iCheck Event for main form
    checkObjs = pagenavMain.bindCheckEvent(checkObjs);

    //disable all moid --autofocus
    $trackMoid.prop('disabled', true);

  }

  function bindEvent(){

    //--autofocus
    $(document).on("keypress",function(e){
      var keyCode = e.keyCode || e.which;
      var saveNextIndex = 0;
      if(keyCode === 13){
        //fill value in input
        switch (focusPage){
          case "moid":
            for(var i= 0;i<listRowSize;i+=1){
              if(recordList[i]){
                if(recordList[i].Moid !== ""){
                  saveNextIndex+=1;
                }
              }
            }
            if(saveNextIndex<listRowSize) {
              $trackMoid.eq(saveNextIndex).val(barCodeStr).change();
            }
            barCodeStr = "";
            break;
          case "worktime":
            var listEmRow = $('.table-list.employee').find('.list-row');
            listEmRow.each(function(index,elm) {
              var $e_id = $(elm).find(".employee-id");
              if($e_id.val()!==""){
                saveNextIndex+=1;
              }
            });
            if(saveNextIndex<listRowSize) {
              $('.employee-id').eq(saveNextIndex).val(barCodeStr).change();
            }
            barCodeStr = "";
            break;
          case "exception":
            var listExRow = $('.table-list.exception').find('.list-row');
            listExRow.each(function(index,elm) {
              var $exCode = $(elm).find(".exception-code");
              if($exCode.val()!==""){
                saveNextIndex+=1;
              }
            });
            if(saveNextIndex<listRowSize) {
              $('.exception-code').eq(saveNextIndex).val(barCodeStr).change();
            }
            barCodeStr = "";
            break;
          default :
            break;
        }

      }else{
          barCodeStr = barCodeStr + String.fromCharCode(keyCode);
      }
    });

    bindInputForm();

    bindPageBtnEvent(pagenavMain, $nextPage, $prevPage);

    // the man time value changed event
    $trackAvrTime.on('change', function () {
        var self = $(this);
        var $list = self.closest('.list-row');
        var listIndex = $tableList.find('.list-row').index($list);
        var time = parseFloat(self.val());
        if (time == 0) {
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: "不可為0 !",
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
            self.val(recordList[listIndex].AvrWorkTime);   // recover value
            return;
        }

        var $manWorkSumTime = $list.find(".trackout-person-time-sum");

        recordList[listIndex].ManWorkTime = time * recordList[listIndex].ManNum;
        recordList[listIndex].AvrWorkTime = time;
        $manWorkSumTime.val(recordList[listIndex].ManWorkTime);
    });

    // the man number value changed event
    $trackManNum.on('change', function () {
        var self = $(this);
        var numStr = self.val();
        var $list = self.closest('.list-row');
        var listIndex = $tableList.find('.list-row').index($list);
        if (numStr.indexOf(".") > -1)
            self.val(parseInt(numStr));
        var num = parseInt(self.val());
        if (num == 0) {
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: "不可為0 !",
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
            self.val(recordList[listIndex].ManNum);   // recover value
            return;
        }

        var $manWorkSumTime = $list.find(".trackout-person-time-sum");

        recordList[listIndex].ManWorkTime = recordList[listIndex].AvrWorkTime * num;
        recordList[listIndex].ManNum = num;
        $manWorkSumTime.val(recordList[listIndex].ManWorkTime);
    });

    /// ---- Click Work Time Detail Button
    $('.detailBtn').on('click',function(){
      var $list = $(this).closest('.list-row');
      var $moid = $list.find(".trackout-content");
      var $qty = $list.find(".trackout-num");
      var $manWorkTime = $list.find(".trackout-person-time-sum");
      var $avrWorkTime = $list.find(".trackout-person-time");
      var $deviceTime = $list.find(".trackout-device-time");
      var $manNum = $list.find(".trackout-person-num");
      if($list.find('.trackout-content').val() == ""){
        return;
      }
      var listIndex = $tableList.find('.list-row').index($list);
      moidIndex = listIndex;

      $.ajax({
          url: "/SFTClient/Trackinout/TableListEmployee",
          method: "GET",
          dataType: "html",
          success: function (data) {

            pagenavMain.checknum = 0;
            pagenavMain.deleteBtn.addClass('hide');
            for(var i = 0; i<listRowSize; i+=1){
              checkObjs[i].iCheck('uncheck');
            }

            $('.content.main').hide();
            $('.content.subpage').html(data);

            //update currently trackform each value
            recordList[listIndex].Moid = $moid.val();
            recordList[listIndex].Qty = $qty.val();
            recordList[listIndex].ManWorkTime = $manWorkTime.val();
            recordList[listIndex].AvrWorkTime = $avrWorkTime.val();
            recordList[listIndex].WorkTime = $deviceTime.val();
            recordList[listIndex].ManNum = $manNum.val();
            recordList[listIndex].Work = recordList[listIndex].hasOwnProperty("Work") ?recordList[listIndex].Work:[];            
            subPageHnd.creatTableListEmployee(listIndex, mainPageinfo, employeePageSubmit);
          },
          error: function (error) {
            ui.modalView({
              modalHeader:"系統訊息",
              modalBody: error.statusText,
              modalFooter:"msgMode",
              alert:true,
              size:{w:350,h:200}
            });
          }
        }
      );
    });

    /// ---- Click Work Time Detail Button
    $('#detailBtnTop').on('click',function(){
      var listRowFirstVal = $listRowFirst.find('.trackout-content').val();
      if(listRowFirstVal == ""){
        return;
      }

      $.ajax({
          url: "/SFTClient/Trackinout/TableListEmployee",
          method: "GET",
          dataType: "html",
          success: function (data) {
            pagenavMain.checknum = 0;
            pagenavMain.deleteBtn.addClass('hide');
            for(var i = 0; i<listRowSize; i+=1){
              checkObjs[i].iCheck('uncheck');
            }
            $('.content.main').hide();
            $('.content.subpage').html(data);            
            subPageHnd.creatTableListAllEmployee(mainPageinfo, employeePageSubmit);
          },
          error: function (error) {
            ui.modalView({
              modalHeader:"系統訊息",
              modalBody: error.statusText,
              modalFooter:"msgMode",
              alert:true,
              size:{w:350,h:200}
            });
          }
        }
      );
    });

    /// ---- Click Work Time Detail Button
    $('.errorDetailBtn').on('click',function(){
      var $list = $(this).closest('.list-row');
      if($list.find('.trackout-content').val() == ""){
        return;
      }
      var listIndex = $tableList.find('.list-row').index($list);

      $.ajax({
          url: "/SFTClient/Trackinout/TableListException",
          method: "GET",
          dataType: "html",
          success: function (data) {
            pagenavMain.checknum = 0;
            pagenavMain.deleteBtn.addClass('hide');
            for(var i = 0; i<listRowSize; i+=1){
              checkObjs[i].iCheck('uncheck');
            }
            $('.content.main').hide();
            $('.content.subpage').html(data);
            subPageHnd.creatTableListException(listIndex, mainPageinfo);
          },
          error: function (error) {
              ui.modalView({
                  modalHeader: "系統訊息",
                  modalBody: error.statusText,
                  modalFooter: "msgMode",
                  alert: true,
                  size: { w: 350, h: 200 }
              });
          }
        }
      );

    });

    /// ----- Submit
    $("#trackoutFinishBtn").on('click', function () {

      $listRow.each(function(index,elm){
        var $moid = $(elm).find(".trackout-content");
        var $qty = $(elm).find(".trackout-num");
        var $manWorkTime = $(elm).find(".trackout-person-time-sum");
        var $avrWorkTime = $(elm).find(".trackout-person-time");
        var $deviceTime = $(elm).find(".trackout-device-time");
        var $manNum = $(elm).find(".trackout-person-num");

        if(recordList[index]) {
        //recordListTmp[index] = {};
          recordList[index].Moid = $moid.val();
          recordList[index].Qty = $qty.val();
          recordList[index].ManWorkTime = $manWorkTime.val();
          recordList[index].AvrWorkTime = $avrWorkTime.val();
          recordList[index].WorkTime = $deviceTime.val();
          recordList[index].ManNum = $manNum.val();
          recordList[index].Exception = recordList[index].hasOwnProperty("Exception") ?recordList[index].Exception:[];
          recordList[index].Work = recordList[index].hasOwnProperty("Work") ?recordList[index].Work:[];
        }
      });

      var source = {
        CheckOutMode: '1',
        CheckOutType: '1',
        RecordList:[]
      };

      source.RecordList = recordList;

      $.ajax({
          type: "POST",
          dataType: "json",
          url: "/SFTClient/api/WorkReport/Trackout/",
          data: JSON.stringify(source),
          contentType: "application/json; charset=utf-8",
          beforeSend: function () { WA.common.ajaxBlockUI(); },
          complete: function () { $.unblockUI(); },
          success: function (data) {
              if (data.Result == "success") {
                  ui.modalView({
                      modalHeader: "系統訊息",
                      modalBody: data.SysMsg,
                      modalFooter: "msgMode",
                      onClose: function () { common.logout(); },
                      size: { w: 350, h: 200 }
                  });
              }
              else {
                  ui.modalView({
                      modalHeader: "系統訊息",
                      modalBody: data.SysMsg,
                      modalFooter: "msgMode",
                      alert: true,
                      size: { w: 350, h: 200 }
                  });
              }
          },
          error: function (error) {
              ui.modalView({
                  modalHeader: "系統訊息",
                  modalBody: error.statusText,
                  modalFooter: "msgMode",
                  alert: true,
                  size: { w: 350, h: 200 }
              });
          }
      });

    });

    $("#stockinBtn").on('click', function () {
        $listRow.each(function(index,elm){
            var $moid = $(elm).find(".trackout-content");
            var $qty = $(elm).find(".trackout-num");
            var $manWorkTime = $(elm).find(".trackout-person-time-sum");
            var $avrWorkTime = $(elm).find(".trackout-person-time");
            var $deviceTime = $(elm).find(".trackout-device-time");
            var $manNum = $(elm).find(".trackout-person-num");

            if(recordList[index]) {
                recordList[index].Moid = $moid.val();
                recordList[index].Qty = $qty.val();
                recordList[index].ManWorkTime = $manWorkTime.val();
                recordList[index].AvrWorkTime = $avrWorkTime.val();
                recordList[index].WorkTime = $deviceTime.val();
                recordList[index].ManNum = $manNum.val();
                recordList[index].Exception = recordList[index].hasOwnProperty("Exception") ?recordList[index].Exception:[];
                recordList[index].Work = recordList[index].hasOwnProperty("Work") ?recordList[index].Work:[];
            }
        });

      var source = {
        CheckOutMode: '1',
        CheckOutType: '2',
        RecordList:[]
      };

      source.RecordList = recordList;

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/SFTClient/api/WorkReport/Trackout/",
            data: JSON.stringify(source),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () { WA.common.ajaxBlockUI(); },
            complete: function () { $.unblockUI(); },
            success: function (data) {
                if (data.Result == "success") {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: data.SysMsg,
                        modalFooter: "msgMode",
                        onClose: function () { common.logout(); },
                        size: { w: 350, h: 200 }
                    });
                }
                else {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: data.SysMsg,
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                }
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
            }
        });
    });

    $('.trackout-num').on('change', checkQty);

    $('#btnLogout').on('click', common.logout);


    curdAction();

  }

  function bindInputForm(){
    $trackQty.add($trackDeviceTime).on('mousedown', function(){
      var self = $(this);
      var list = $(this).closest('.list-row');
      var $singleMoid = list.find('.trackout-content');
      if($singleMoid.val()!==""){
        //self.prop('disabled', false);
        self.keyboard({
          layout: 'custom',
          restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
          preventPaste : true,  // prevent ctrl-v and right click
          autoAccept : true,
          display: {
            'b': '\u2190:Backspace',
            'bksp': 'Bksp:Backspace'
          },
          customLayout: {
            'default': [
              '{clear} {dec} {b}',
              '7 8 9',
              '4 5 6',
              '1 2 3',
              '0 {a} {c}'
            ]
          }
        });
      }else{
        self.prop('disabled', true);
      }
    });

    $trackAvrTime.add($trackManNum).on('mousedown', function(){
      var self = $(this);
      var list = $(this).closest('.list-row');
      var $singleMoid = list.find('.trackout-content');
      var listIndex = $tableList.find('.list-row').index(list);
      if($singleMoid.val()!=="") {
        //for first time
        if(recordList.length === 0){
          $(this).prop('disabled', false);
          $(this).keyboard({
            layout: 'custom',
            restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
            preventPaste : true,  // prevent ctrl-v and right click
            autoAccept : true,
            display: {
              'b': '\u2190:Backspace',
              'bksp': 'Bksp:Backspace'
            },
            customLayout: {
              'default': [
                '{clear} {dec} {b}',
                '7 8 9',
                '4 5 6',
                '1 2 3',
                '0 {a} {c}'
              ]
            }
          });
        }

    //    for(var i= 0, len=recordList.length;i<len;i+=1){
        if (recordList[listIndex].Work.length > 0) {
           console.log("disable");
            $(this).prop('disabled', true);
          }else{
            //for delete all items
            $(this).prop('disabled', false);
            $(this).keyboard({
              layout: 'custom',
              restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
              preventPaste : true,  // prevent ctrl-v and right click
              autoAccept : true,
              display: {
                'b': '\u2190:Backspace',
                'bksp': 'Bksp:Backspace'
              },
              customLayout: {
                'default': [
                  '{clear} {dec} {b}',
                  '7 8 9',
                  '4 5 6',
                  '1 2 3',
                  '0 {a} {c}'
                ]
              }
            });
          }
 //       }
      }else{
        self.prop('disabled', true);
      }

    });

    $trackAvrTimeAll.on('click', function(){

      //check if already save detail employee list
      //if it don't save detail employee list, show modal
      for(var i= 0, len=recordList.length;i<len;i+=1){
        if(recordList[i].Work.length>0){
          return;
        }
      }

      var modalheader = "時間設定";
      var modalbody = '<div class="input-group">'+
        '<span class="input-group-title cell">請輸入人時:</span>'+
        '<input class="modal-form form-control w60 cell last qwerty-num" id="serverPort" type="text" value="0" />'+
        '</div>';
      var modalfooter = "submitMode";

      var onsubmit = function(){
        var avrWorkTime = $('.modal-form').val();
        for(var i= 0, len=recordList.length;i<len;i+=1){
          recordList[i].AvrWorkTime = avrWorkTime;
        }
        $listRow.each(function(index,elm) {
          var $moid = $(elm).find(".trackout-content");
          var $avrWorkTime = $(elm).find(".trackout-person-time");
          var $manWorkTime = $(elm).find(".trackout-person-time-sum");
          var $manNum = $(elm).find(".trackout-person-num");
          if($moid.val()!==""){
            var manWorkTimeVal = avrWorkTime*$manNum.val();
            $avrWorkTime.val(avrWorkTime);
            $manWorkTime.val(manWorkTimeVal);
          }
        });
      };
      var modalObj = {
        modalHeader:modalheader,
        modalBody:modalbody,
        modalFooter:modalfooter,
        onSubmit:onsubmit
      };
      ui.modalView(modalObj);
    });


    $deviceTimeAll.on('click', function(){

      var modalheader = "機時設定";
      var modalbody = '<div class="input-group">'+
        '<span class="input-group-title cell">請輸入機時:</span>'+
        '<input class="modal-form form-control w60 cell last qwerty-num" id="serverPort" type="text" value="0" />'+
        '</div>';
      var modalfooter = "submitMode";

      var onsubmit = function(){
        var deviceTime = $('.modal-form').val();
        for(var i= 0, len=recordList.length;i<len;i+=1){
          recordList[i].WorkTime = deviceTime;
        }
        $listRow.each(function(index,elm) {
          var $moid = $(elm).find(".trackout-content");
          var $deviceTime = $(elm).find(".trackout-device-time");
          if($moid.val()!==""){
            $deviceTime.val(deviceTime);
          }
        });
      };
      var modalObj = {
        modalHeader:modalheader,
        modalBody:modalbody,
        modalFooter:modalfooter,
        onSubmit:onsubmit
      };
      ui.modalView(modalObj);
    });


    $trackManNumAll.on('click', function(){

      //check if already save detail employee list
      //if it don't save detail employee list, show modal
      for(var i= 0, len=recordList.length;i<len;i+=1){
        if(recordList[i].Work.length>0){
          return;
        }
      }

      var modalheader = "人數設定";
      var modalbody = '<div class="input-group">'+
        '<span class="input-group-title cell">請輸入人數:</span>'+
        '<input class="modal-form form-control w60 cell last qwerty-num" id="serverPort" type="text" value="0" />'+
        '</div>';
      var modalfooter = "submitMode";
      var onsubmit = function(){
        var manNum = $('.modal-form').val();
        for(var i= 0, len=recordList.length;i<len;i+=1){
          recordList[i].ManNum = manNum;
        }
        $listRow.each(function(index,elm) {
          var $moid = $(elm).find(".trackout-content");
          var $avrWorkTime = $(elm).find(".trackout-person-time");
          var $manWorkTime = $(elm).find(".trackout-person-time-sum");
          var $manNum = $(elm).find(".trackout-person-num");
          if($moid.val()!==""){
            var manWorkTimeVal = $avrWorkTime.val()*manNum;
            $manWorkTime.val(manWorkTimeVal);
            $manNum.val(manNum);
          }
        });
      };
      var modalObj = {
        modalHeader:modalheader,
        modalBody:modalbody,
        modalFooter:modalfooter,
        onSubmit:onsubmit
      };
      ui.modalView(modalObj);
    });

  }

  function curdAction() {
    //----- Get  row
    $trackMoid.on('change', function(){
      //get row index
      var self = $(this);
      var list = $(this).closest('.list-row');
      var listIndex = $tableList.find('.list-row').index(list);
      var $trackoutManWorkTimeSum = list.find('.trackout-person-time-sum');
      var $trackoutAvrWorkTime = list.find('.trackout-person-time');
      var $deviceTime = list.find('.trackout-device-time');
      var $trackoutQty = list.find('.trackout-num');
      var $trackoutManNum = list.find('.trackout-person-num');
      var moidStr =  self.val();
      var $maxQty = list.find('.track-maxnum');
      //recordList[listIndex].Moid = moidStr;

      // check format
      if (common.checkMoIdFormat(self) == false)
          return;

      // check duplicate item
      var repeatFlag = true;
      var moidBuff;
      $listRow.each(function(index,elm){
        if(index !== listIndex){
          var $moid = $(elm).find(".trackout-content");
          if ($moid.val() == "")
              return false;
          moidBuff = moidStr.split(";");
          if (moidBuff[0] === $moid.val().split(";")[0]) {
            self.val("");
            ui.modalView({
              modalHeader:"系統訊息",
              modalBody:"重複的製令 !",
              modalFooter:"msgMode",
              alert:true,
              size:{w:350,h:200}
            });
            repeatFlag = false;
            return false;
          }
        }
      });
      if(!repeatFlag){
        return;
      }

      var source = {
          Moid: moidStr,
          CheckQtyType: '1'
      };
      $.ajax({
        type: "POST",
        dataType: "json",
        url: "/SFTClient/api/Check/MoCheck/",
        data: JSON.stringify(source),
        contentType: "application/json; charset=utf-8",
        beforeSend: function () { WA.common.ajaxBlockUI(); },
        complete: function () { $.unblockUI(); },
        success: function (output) {
          if (output.Result == "success")
          {
            if (parseFloat(output.CheckQty) == 0) {
              ui.modalView({
                modalHeader:"系統訊息",
                modalBody:'檢查失敗, 無待出批量 !',
                modalFooter:"msgMode",
                alert:true,
                size:{w:350,h:200}
              });

              self.val("");
            }
            else {
              if(recordDetailTotal.Work){
                recordList[listIndex] = {};
                recordList[listIndex].Moid = moidStr;
                recordList[listIndex].Qty = output.CheckQty;
                recordList[listIndex].ManWorkTime = recordDetailTotal.ManWorkTime;
                recordList[listIndex].AvrWorkTime = recordDetailTotal.AvrWorkTime;
                recordList[listIndex].WorkTime = 0;
                recordList[listIndex].ManNum = recordDetailTotal.ManNum;
                recordList[listIndex].Exception = [];
                recordList[listIndex].Work = recordDetailTotal.Work;
              }else{
                recordList[listIndex] = {};
                recordList[listIndex].Moid = moidStr;
                recordList[listIndex].Qty = output.CheckQty;
                recordList[listIndex].ManWorkTime = 0;
                recordList[listIndex].AvrWorkTime = 0;//the same as ManWorkTime
                recordList[listIndex].WorkTime = 0;
                recordList[listIndex].ManNum = 1;
                recordList[listIndex].Exception = [];
                recordList[listIndex].Work = [];
              }

                //auto jump to correct page by listIndex
                pagenavMain.jumpPageByIndex(listIndex);

              $trackoutQty.val(recordList[listIndex].Qty);
              $deviceTime.val(recordList[listIndex].WorkTime);
              $trackoutAvrWorkTime.val(recordList[listIndex].AvrWorkTime);
              $trackoutManWorkTimeSum.val(recordList[listIndex].ManWorkTime);
              $trackoutManNum.val(recordList[listIndex].ManNum);
              $maxQty.val(recordList[listIndex].Qty);

              if($trackMoid.val()!=="") {
                $trackQty.add($trackDeviceTime).add($trackAvrTime).add($trackManNum).prop('disabled', false);
              }
            }
          }
          else
          {

            ui.modalView({
              modalHeader:"系統訊息",
              modalBody:output.SysMsg,
              modalFooter:"msgMode",
              alert:true,
              size:{w:350,h:200}
            });

            if(global.debuggerMode) {
              if(recordDetailTotal.Work){
                recordList[listIndex] = {};
                recordList[listIndex].Moid = moidStr;
                recordList[listIndex].Qty = 10;
                recordList[listIndex].ManWorkTime = recordDetailTotal.ManWorkTime;
                recordList[listIndex].AvrWorkTime = recordDetailTotal.AvrWorkTime;
                recordList[listIndex].WorkTime = 0;
                recordList[listIndex].ManNum = recordDetailTotal.ManNum;
                recordList[listIndex].Exception = [];
                recordList[listIndex].Work = recordDetailTotal.Work;
              }else{
                recordList[listIndex] = {};
                recordList[listIndex].Moid = moidStr;
                recordList[listIndex].Qty = 10;
                recordList[listIndex].ManWorkTime = 0;
                recordList[listIndex].AvrWorkTime = 0;
                recordList[listIndex].WorkTime = 0;
                recordList[listIndex].ManNum = 1;
                recordList[listIndex].Exception = [];
                recordList[listIndex].Work = [];
              }

              //auto jump to correct page by listIndex
              pagenavMain.jumpPageByIndex(listIndex);

              $trackoutQty.val(recordList[listIndex].Qty);
              $deviceTime.val(recordList[listIndex].WorkTime);
              $trackoutAvrWorkTime.val(recordList[listIndex].AvrWorkTime);
              $trackoutManWorkTimeSum.val(recordList[listIndex].ManWorkTime);
              $trackoutManNum.val(recordList[listIndex].ManNum);


              if($trackMoid.val()!=="") {
                $trackQty.add($trackDeviceTime).add($trackAvrTime).add($trackManNum).prop('disabled', false);
              }
            }else{
              self.val("");
            }
          }
        },
        error: function (error) {
          ui.modalView({
            modalHeader:"系統訊息",
            modalBody: error.statusText,
            modalFooter:"msgMode",
            alert:true,
            size:{w:350,h:200}
          });
          self.val("");
        }
      });
    });

    //----- Delete checked row
    $mainDeleteBtn.on('click',function(){
      //copy original recordList
      var recordListTmp = recordList.slice(0);
      var recordListTmpNew = [];

      //----- Record non-checked item and Delete
      $listRow.each(function(index,elm){
        var $moid = $(elm).find(".trackout-content");
        var $qty = $(elm).find(".trackout-num");
        var $maxQty = $(elm).find(".track-maxnum");
        var $manWorkTime = $(elm).find(".trackout-person-time-sum");
        var $avrWorkTime = $(elm).find(".trackout-person-time");
        var $deviceTime = $(elm).find(".trackout-device-time");
        var $manNum = $(elm).find(".trackout-person-num");

        //var list = $(this).closest('.list-row');
        //var employeeIndex = $tableEmList.find('.list-row').index(list);
        if($moid.val()!=="") {
          if(recordListTmp[index]) {
            //recordListTmp[index] = {};
            recordListTmp[index].Moid = $moid.val();
            recordListTmp[index].Qty = $qty.val();
            recordListTmp[index].MaxQty = $maxQty.val();
            recordListTmp[index].Delete = $(elm).hasClass('selected');
            recordListTmp[index].ManWorkTime = $manWorkTime.val();
            recordListTmp[index].AvrWorkTime = $avrWorkTime.val();
            recordListTmp[index].WorkTime = $deviceTime.val();
            recordListTmp[index].ManNum = $manNum.val();
          }
        }

        //clear all content and num value
        $moid.val("");
        $qty.val("");
        $manWorkTime.val("");
        $avrWorkTime.val("");
        $deviceTime.val("");
        $manNum.val("");
        $maxQty.val("");
      });

      for(var i= 0, len=recordListTmp.length;i<len;i+=1){
        if(recordListTmp[i]){
          if(!recordListTmp[i].Delete) {
            recordListTmpNew.push(recordListTmp[i]);
          }
        }
      }

      var max_num = recordListTmpNew.length;
      //auto jump to correct page by max_num
      pagenavMain.jumpPageByIndex(max_num-1);

      //---- Refill in table
      $listRow.each(function(index,elm){
        var $moid = $(elm).find(".trackout-content");
        var $qty = $(elm).find(".trackout-num");
        var $maxQty = $(elm).find(".track-maxnum");
        var $manWorkTime = $(elm).find(".trackout-person-time-sum");
        var $avrWorkTime = $(elm).find(".trackout-person-time");
        var $deviceTime = $(elm).find(".trackout-device-time");
        var $manNum = $(elm).find(".trackout-person-num");

        if(index < max_num) {
          $moid.val(recordListTmpNew[index].Moid);
          $qty.val(recordListTmpNew[index].Qty);
          $maxQty.val(recordListTmpNew[index].MaxQty);
          $manWorkTime.val(recordListTmpNew[index].ManWorkTime);
          $avrWorkTime.val(recordListTmpNew[index].AvrWorkTime);
          $deviceTime.val(recordListTmpNew[index].WorkTime);
          $manNum.val(recordListTmpNew[index].ManNum);
        }
        else
          return false;   // break
      });

      recordList = recordListTmpNew;

      //console.log(trackObj, "trackObj");
      pagenavMain.checknum = 0;
      pagenavMain.deleteBtn.addClass('hide');
      for(var i = 0; i<listRowSize; i+=1){
        checkObjs[i].iCheck('uncheck');
      }

    });



  }

  function checkQty() {
      var self = $(this);
      if (parseFloat(self.val()) == 0) {
          ui.modalView({
              modalHeader: "系統訊息",
              modalBody: "數量不可以為0 !",
              modalFooter: "msgMode",
              alert: true,
              onClose: function () { self.val(1); },
              size: { w: 350, h: 200 }
          });
      }
      else {
          var list = self.closest('.list-row');
          var $maxQty = list.find('.track-maxnum');
          if (parseFloat(self.val()) > parseFloat($maxQty.val())) {
              ui.modalView({
                  modalHeader: "系統訊息",
                  modalBody: "欲出站數量不可大於製令的可出站數量 !",
                  modalFooter: "msgMode",
                  alert: true,
                  size: { w: 350, h: 200 }
              });
              self.val($maxQty.val());
          }
      }
  }

  function bindPageBtnEvent(pagenavObj ,nextEmPage, prevEmPage){
    nextEmPage.on('click', {navObj:pagenavObj}, onNextPage);
    prevEmPage.on('click', {navObj:pagenavObj}, onPrevPage);
  }
  function onNextPage(event){
    event.preventDefault();
    var pagenavObj = event.data.navObj;
    pagenavObj.nextPageHandlr();
  }
  function onPrevPage(event){
    event.preventDefault();
    var pagenavObj = event.data.navObj;
    pagenavObj.prevPageHandlr();
  }

}(WA, this));