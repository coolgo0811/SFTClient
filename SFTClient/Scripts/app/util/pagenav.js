﻿/**
 * Created by seven on 15/5/28.
 */

WA.pagenav = WA.pagenav || {};

WA.pagenav = (function(){
  var Constr;

  Constr = function($tableList, $deleteBtn, $nextPageBtn, $prevPageBtn, visibleLines, itemPerPage, listRowSize){
    this.tableList = $tableList;
    this.nextPageBtn = $nextPageBtn;
    this.prevPageBtn = $prevPageBtn;
    this.deleteBtn = $deleteBtn;
    this.currentIndex = 0;
    this.checknum = 0;
    this.itemPerPage = itemPerPage;
    this.listRowSize = listRowSize;
    this.visibleLines = visibleLines;
    this.maxPage = Math.ceil(this.listRowSize/this.itemPerPage) - 1;
  }

  Constr.prototype = {
    init:function(){

    },

    showTablePage:function(){
      //debugger;
      var self = this;
      var begin = self.currentIndex*self.itemPerPage;
      var end = Math.min(begin + self.itemPerPage, self.listRowSize);

      $.each(self.visibleLines, function(index,value){
        value.hide();
      });

      self.visibleLines = [];

      self.tableList.find('tbody').find('tr:lt('+end+')').each(function(index,elm){
        if(index >= begin){
          $(elm).show();
          self.visibleLines.push($(elm));
        }
      });

      return self.visibleLines;
    },

    bindCheckEvent:function(checkObjs){
      var self = this;
      self.tableList.find('.list-row').find('input:checkbox').each(function(index, elm){
        checkObjs[index] = $(elm);
        //add or remove selected class on row
        $(elm).on('ifChecked', function(event){
          console.log("addcheck before checknum",self.checknum);
          self.checknum = self.checknum + 1;
          console.log("addcheck after checknum",self.checknum);

          $(this).closest('tr').addClass("selected");
          if(self.deleteBtn.hasClass('hide')){
            self.deleteBtn.removeClass('hide');
          }
        });
        $(elm).on('ifUnchecked', function(event){
          console.log("removecheck before checknum",self.checknum);

          self.checknum = self.checknum - 1;
          if(self.checknum < 0){
            self.checknum = 0;
          }
          $(this).closest('tr').removeClass("selected");
          if(self.checknum===0){
            self.deleteBtn.addClass('hide');
          }
          console.log("removecheck after checknum",self.checknum);

        });
      });


      return checkObjs;


    },

    navBtnDisplay:function(){
      //debugger;

      var self = this;
      if(self.currentIndex >= self.maxPage){
        self.nextPageBtn.hide();
      }else{
        self.nextPageBtn.show();
      }
      if(self.currentIndex <= 0){
        self.prevPageBtn.hide();
      }else{
        self.prevPageBtn.show();
      }
    },

    nextPageHandlr:function(){
      var self = this;
      self.currentIndex+=1;
      self.currentIndex = Math.min(self.currentIndex, self.maxPage);
      self.visibleLines = self.showTablePage();
      self.navBtnDisplay();
    },

    prevPageHandlr:function() {
      var self = this;
      self.currentIndex-=1;
      if(self.currentIndex<0){
        self.currentIndex = 0;
      }
      self.visibleLines = self.showTablePage();
      self.navBtnDisplay();
    },
    //auto jump to correct page by max_num
    jumpPageByIndex:function(index){
      var self = this;
      var currentPage = parseInt(index/self.itemPerPage);
      var maxPage = Math.ceil(self.listRowSize/self.itemPerPage)-1;
      if(currentPage>maxPage) currentPage = maxPage;
      self.currentIndex = currentPage;
      self.visibleLines = self.showTablePage();
      self.navBtnDisplay();
    }


  }

  return Constr;

}());