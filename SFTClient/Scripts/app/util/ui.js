﻿/**
 * Created by seven on 15/5/20.
 */

WA.ui = WA.ui || {};

WA.ui = (function() {
  function bindVirtualKB() {

    $('.qwerty').keyboard({layout: 'qwerty'});

    $('.qwerty-num').keyboard({
      layout: 'custom',
      restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
      preventPaste: true,  // prevent ctrl-v and right click
      autoAccept: true,
      display: {
        'b': '\u2190:Backspace',
        'bksp': 'Bksp:Backspace'
      },
      customLayout: {
        'default': [
          '{clear} {dec} {b}',
          '7 8 9',
          '4 5 6',
          '1 2 3',
          '0 {a} {c}'
        ]
      }
    });
  }

  function bindInputStyle() {
    $('input:checkbox').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
    $('input:radio').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
  }

  function showTablePage(index, $tableList, visibleLines, itemPerPage, listRowSize) {
    //debugger;

    $.each(visibleLines, function (index, value) {
      value.hide();
    });

    visibleLines = [];

    var begin = index * itemPerPage;
    var end = Math.min(begin + itemPerPage, listRowSize);
    $tableList.find('tbody').find('tr:lt(' + end + ')').each(function (index, elm) {
      if (index >= begin) {
        $(elm).show();
        visibleLines.push($(elm));
      }
    });

    return visibleLines;
  }


  function navBtnDisplay(currentIndex, nextPageBtn, prevPageBtn, maxPage) {
    if (currentIndex >= maxPage) {
      nextPageBtn.hide();
    } else {
      nextPageBtn.show();
    }
    if (currentIndex <= 0) {
      prevPageBtn.hide();
    } else {
      prevPageBtn.show();
    }
  }

  function modalView(modalObj) {
    var $modalView = $('.modal-view');
    var $modalFrame = $modalView.find(".modal-frame");
    var $mheader = $modalView.find('.modal-header');
    var $mbody = $modalView.find('.modal-body');
    var $mfooter = $modalView.find('.modal-footer');

    modalObj.autoClose = modalObj.hasOwnProperty("autoClose")?modalObj.autoClose:true;
    modalObj.modalHeader = modalObj.modalHeader || "";
    modalObj.modalBody = modalObj.modalBody || "";
    modalObj.modalFooter = modalObj.modalFooter || "submitMode";

    var footerHtml = "";
    switch (modalObj.modalFooter) {
      case "submitMode":
        footerHtml = '<div class="col-md-6"><input type="button" class="modal-btn cancel" value="取消"/></div>' +
        '<div class="col-md-6"><input type="button" class="modal-btn submit" value="確定"/></div>';
        break;
      case "msgMode":
        footerHtml = '<div class="col-md-12"><input type="button" class="modal-btn cancel" value="關閉"/></div>';
        break;
      default :
        footerHtml = modalObj.modalFooter;
        break;
    }

    if (modalObj.size) {
      var mW = modalObj.size.w || 500;
      var mH = modalObj.size.h || 250;
      $modalFrame.css({
        "margin-left": mW / 2 * -1,
        "width": mW,
        "height": mH
      });
      if (modalObj.size.h === "auto") {
        $mfooter.css({
          "position": "relative"
        });
      }
    }
    if(modalObj.alert){
      $modalFrame.css({
        "background-color":"#f57e76"
      });
      $mheader.css({
        'font-weight':"800"
      });
    }
    $mheader.text(modalObj.modalHeader);
    $mfooter.html(footerHtml);
    $mbody.html(modalObj.modalBody);

    this.bindVirtualKB();
    //this.bindInputStyle();


    if (modalObj.duration && modalObj.modalFooter==="msgMode") {
      $modalView.fadeIn(function(){
        setTimeout(function(){
          $modalView.fadeOut(function () {
            closeModal();
          });
        },modalObj.duration);
      });
    }else{
      $modalView.show();
    }

    var $mbtnCancel = $modalView.find('.modal-btn.cancel');
    var $mbnSubmit = $modalView.find('.modal-btn.submit');
    $mbtnCancel.on('click', function(){
      if(modalObj.onClose) {
        modalObj.onClose();
      }
      innerCloseModal();

    });
    $mbnSubmit.on('click', function(){
      if(modalObj.onSubmit) {
        modalObj.onSubmit();
      }
      if(modalObj.autoClose){
        innerCloseModal();
      }
    });
    function innerCloseModal(){
      $modalFrame.css({
        "margin-left": "",
        "width": "",
        "height": "",
        "background-color":""
      });
      $mheader.css({
        'color':"",
        'font-weight':""
      });
      $mbody.find('.input-group').find('.input-group-title').css({
        'color':""
      });
      $mfooter.css({
        "position": ""
      });

      $mheader.text("");
      $mbody.html("");
      $mfooter.html("");
      $modalView.hide();
    }
  }

  function closeModal($modalView, $modalFrame, $mheader, $mbody, $mfooter){
    $modalFrame.css({
      "margin-left": "",
      "width": "",
      "height": "",
      "background-color":""
    });
    $mheader.css({
      'color':"",
      'font-weight':""
    });
    $mbody.find('.input-group').find('.input-group-title').css({
      'color':""
    });
    $mfooter.css({
      "position": ""
    });

    $mheader.text("");
    $mbody.html("");
    $mfooter.html("");
    $modalView.hide();
  }



  return {
    bindVirtualKB:bindVirtualKB,
    bindInputStyle:bindInputStyle,
    showTablePage:showTablePage,
    navBtnDisplay:navBtnDisplay,
    modalView:modalView,
    closeModal:closeModal
  };

}());