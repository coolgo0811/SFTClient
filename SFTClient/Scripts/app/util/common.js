﻿
WA.common = WA.common || {};

WA.common = (function (wa) {
    var ui = wa.ui;

    function resetAutoLogoutTimeOut(input) {
        if (parseInt(input.timeout) > 0) {
            if (input.handler != null)
                clearTimeout(input.handler);

            input.handler = setTimeout(logout, input.timeout * 1000 * 60);   // minutes
        }
    }

    function autoLogout() {
        var handler;
        var paramValue = getSysParameter("AutoLogoutTime");
        var time = parseInt(paramValue) || 0;

        var input = { timeout: time, handler: handler };
        resetAutoLogoutTimeOut(input);

        $(document).on("keypress", function (e) { resetAutoLogoutTimeOut(input); });
        $(document).on("click", function (e) { resetAutoLogoutTimeOut(input); });

    }

    function logout() {
        var source = {};
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/SFTClient/api/Authority/Logout/",
            data: JSON.stringify(source),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () { ajaxBlockUI(); },
            complete: function () { $.unblockUI(); },
            success: function (data) {
                if (data.Result == "success") {
                    location.href = "/SFTClient/Home/Index";
                }
                else {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: data.SysMsg,
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                }
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
            }
        });
    }

    function ajaxBlockUI(msg) {
        var message;
        if (msg == null || msg.length == 0)
            message = "Loading";
        else
            message = msg;
        var path = "/SFTClient/Content/img/loading.gif";
        $.blockUI({
            message:
                '<img src=' + path + ' style="vertical-align:middle" />&nbsp;&nbsp;' + message,
            css: {
                border: 'none',
                padding: '15px'
            }
        });
    }

    function checkEquipIdFormat(obj) {
        var id = obj.val();
        if (id == "")
            return false;
        var index = id.indexOf("ESC006");
        if (index == 0) {
            obj.val(id.substr(6, id.length - 1));
        }
        else {
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: '條碼格式錯誤，請重新掃描　!',
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
            obj.val("");
            return false;
        }
        return true;
    }

    function checkMoIdFormat(obj) {
        var id = obj.val();
        if (id == "")
            return false;

        if (id.split(";").length != 4) {
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: "製令格式錯誤 !",
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
            obj.val("");
            return false;
        }
        return true;
    }

    function checkExceptionFormat(obj, type) {
        var id = obj.val();
        if (id == "")
            return false;
        var checkflag = false;
        var index;
        switch (type) {
            case "normal":
                index = id.indexOf("ESC009");
                if (index == 0) {
                    checkflag = true;
                    break;
                }
                index = id.indexOf("ESC010");
                if (index == 0) {
                    checkflag = true;
                    break;
                }
                index = id.indexOf("ESC011");
                if (index == 0) {
                    checkflag = true;
                    break;
                }
                break;
            case "rework":
                index = id.indexOf("ESC020");
                if (index == 0) {
                    checkflag = true;
                    break;
                }
                break;
        }

        if (!checkflag) {
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: "條碼格式錯誤，請重新掃描　!",
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
            obj.val("");
            return false;
        }
        else {
            id = id.substr(6, id.length - 1);
            obj.val(id);
            return true;
        }
    }

    function checkUserIdFormat(obj) {
        var id = obj.val();
        if (id == "")
            return false;

        var index = id.indexOf("ESC005");
        if (index == 0) {
            obj.val(id.substr(6, id.length - 1));
        }
        else {
            obj.val("");
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: "條碼格式錯誤，請重新掃描 !",
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
            return false;
        }
        return true;
    }
    
    function liveUpdate() {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "/SFTClient/api/SysSetting/LiveUpdate",
            contentType: "application/json; charset=utf-8",
            beforeSend: function () { ajaxBlockUI("更新中..."); },
            complete: function () { $.unblockUI(); },
            success: function (version) {
                if (version != null && version != "") {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: "版本 " + version + "更新完成 !",
                        modalFooter: "msgMode",
                        onClose: function () { location.href = "/SFTClient/Home/Index"; },
                        size: { w: 350, h: 200 }
                    });
                    return true;
                }
                else {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: "更新異常 !",
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                    return false;
                }
            },
            error: function () {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: "更新異常 !",
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
                return false;
            }
        });
    }

    function getSysParameter(paramName) {
        var paramValue = "";
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/SFTClient/api/SysSetting/SysParamValue/' + paramName,
            contentType: "application/json; charset=utf-8",
            async: false,
            timeout: 1000,
            success: function (value) {
                paramValue = value;
            },
            error: function () {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: "系統設定讀取錯誤 !",
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
            }
        });
        return paramValue;
    }

    function getUrlVariables(name) {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars[name];
    }

  return {
      autoLogout: autoLogout,
      logout: logout,
      ajaxBlockUI: ajaxBlockUI,
      checkEquipIdFormat: checkEquipIdFormat,
      checkMoIdFormat: checkMoIdFormat,
      checkExceptionFormat: checkExceptionFormat,
      checkUserIdFormat: checkUserIdFormat,
      liveUpdate: liveUpdate,
      getSysParameter: getSysParameter,
      getUrlVariables: getUrlVariables,
  };

}(WA));