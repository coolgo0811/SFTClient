﻿$(function (wa, global) {

wa.subpagehnd = function () { };

var ui = wa.ui;
var common = wa.common;

// sub pages
wa.subpagehnd.prototype.creatTableListAllEmployee =
function (mainPageinfo, submit) {

    var recordList = mainPageinfo.recordList;
    var bindPageBtnEvent = mainPageinfo.bindPageBtnEvent;
    var clearBarCodeStr = mainPageinfo.clearBarCodeStr;
    var setFocusPage = mainPageinfo.setFocusPage;
    var itemPerPage = mainPageinfo.itemPerPage;
    var listRowSize = mainPageinfo.listRowSize;
    var pagenavMain = mainPageinfo.pagenavMain;
    var checkObjs = mainPageinfo.checkObjs;
    var saveRecordDetailTotal = mainPageinfo.saveRecordDetailTotal;

    var visibleEmLines = [];
    var checkEmObjs = [];
    var $tableEmList = $('.table-list.employee');
    var $listEmRow = $tableEmList.find('.list-row');
    var $prevEmployeePage = $('#prevEmployeePage');
    var $nextEmployeePage = $('#nextEmployeePage');
    var $saveEmCloseBtn = $('#saveEmCloseBtn');
    var $emDeleteBtn = $('#emDeleteBtn');

    var $employeeName = $('.employee-name');
    var $employeeID = $('.employee-id')
    var $emTime = $('.single-work-time');

    var pagenavEmployee = new wa.pagenav($tableEmList, $emDeleteBtn, $nextEmployeePage, $prevEmployeePage, visibleEmLines, itemPerPage, listRowSize);

    //clear focus barcode string
    clearBarCodeStr();
    setFocusPage("worktime");

    //ui.bindVirtualKB();
    ui.bindInputStyle();
    checkEmObjs = pagenavEmployee.bindCheckEvent(checkEmObjs);


    //show hide page
    visibleEmLines = pagenavEmployee.showTablePage();
    pagenavEmployee.navBtnDisplay();
    bindPageBtnEvent(pagenavEmployee, $nextEmployeePage, $prevEmployeePage);

    $employeeID.add($employeeName).add($emTime).prop("disabled", true);

    //load recorded Employee Data
    $listEmRow.each(function (index, elm) {
        var $e_id = $(elm).find(".employee-id");
        var $e_name = $(elm).find(".employee-name");
        var $e_time = $(elm).find(".single-work-time");

        //clear when
        $e_id.val("");
        $e_name.val("");
        $e_time.val("");
    });

    //dynamic init keyboard when click input
    $emTime.on('mousedown', function () {
        var self = $(this);
        var list = $(this).closest('.list-row');
        var $singleId = list.find('.employee-id');
        if ($singleId.val() !== "") {
            self.keyboard({
                layout: 'custom',
                restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
                preventPaste: true,  // prevent ctrl-v and right click
                autoAccept: true,
                display: {
                    'b': '\u2190:Backspace',
                    'bksp': 'Bksp:Backspace'
                },
                customLayout: {
                    'default': [
                      '{clear} {dec} {b}',
                      '7 8 9',
                      '4 5 6',
                      '1 2 3',
                      '0 {a} {c}'
                    ]
                }
            });
        } else {
            self.prop('disabled', true);
        }
    });

    //----- employee Delete checked row
    $emDeleteBtn.on('click', function () {

        var workerListTmp = [];
        var workerListTmpNew = [];

        //----- Record non-checked item and delete
        $listEmRow.each(function (index, elm) {
            var $employeeID = $(elm).find(".employee-id");
            var $employeeName = $(elm).find(".employee-name");
            var $employeeTime = $(elm).find(".single-work-time");

            if ($employeeID.val() !== "") {
                workerListTmp[index] = {};
                workerListTmp[index].User = $employeeID.val();
                workerListTmp[index].Name = $employeeName.val();
                workerListTmp[index].Times = $employeeTime.val();
                workerListTmp[index].Delete = $(elm).hasClass('selected');
            }

            //clear all content and num value
            $employeeID.val("");
            $employeeName.val("");
            $employeeTime.val("");
        });

        // --- Delete selected object
        for (var i = 0, len = workerListTmp.length; i < len; i += 1) {
            if (workerListTmp[i]) {
                if (!workerListTmp[i].Delete) {
                    workerListTmpNew.push(workerListTmp[i]);
                }
            }
        }

        var max_num = workerListTmpNew.length;
        //auto jump to correct page by max_num
        pagenavEmployee.jumpPageByIndex(max_num - 1);

        //---- Refill in table
        $listEmRow.each(function (index, elm) {
            var $employeeID = $(elm).find(".employee-id");
            var $employeeName = $(elm).find(".employee-name");
            var $employeeTime = $(elm).find(".single-work-time");

            if (index < max_num) {
                $employeeID.val(workerListTmpNew[index].User);
                $employeeName.val(workerListTmpNew[index].Name);
                $employeeTime.val(workerListTmpNew[index].Times);
            }
        });

        //workerList = workerListTmpNew;
        pagenavEmployee.checknum = 0;
        pagenavEmployee.deleteBtn.addClass('hide');
        for (var i = 0; i < listRowSize; i += 1) {
            checkObjs[i].iCheck('uncheck');
        }
        for (var i = 0; i < listRowSize; i += 1) {
            checkEmObjs[i].iCheck('uncheck');
        }
    });


    // ----Update, Close employee page
    // and save data to front page
    $saveEmCloseBtn.on('click', function () {
        var workerList = [];
        var totalWorkTime = 0;
        var pp_avrWorkTime = 0;
        var pp_manWorkTime = 0;
        var pp_manNum = 0;

        $listEmRow.each(function (index, elm) {
            var $e_id = $(elm).find(".employee-id");
            var $e_name = $(elm).find(".employee-name");
            var $e_time = $(elm).find(".single-work-time");
            var e_id = $e_id.val();
            var e_name = $e_name.val();
            var e_time = $e_time.val();

            if (e_id !== "") {
                totalWorkTime += parseFloat(e_time);
                var w = {
                    User: e_id,
                    Name: e_name,
                    Times: e_time
                };
                workerList.push(w);
            }
        });

        //if inputbox has value
        if (workerList.length > 0) {
            pp_manNum = workerList.length;
            pp_avrWorkTime = (totalWorkTime / pp_manNum).toFixed(2);
            pp_manWorkTime = totalWorkTime;

            saveRecordDetailTotal( {
                AvrWorkTime: pp_avrWorkTime,
                ManWorkTime: pp_manWorkTime,
                ManNum: pp_manNum,
                Work: workerList
            });

            $listEmRow.each(function (index, elm) {
                if (recordList[index]) {
                    recordList[index].AvrWorkTime = pp_avrWorkTime;
                    recordList[index].ManWorkTime = pp_manWorkTime;
                    recordList[index].ManNum = pp_manNum;
                    recordList[index].Work = workerList;
                }
            });
        }

        //update data to trackout form
        if (workerList.length > 0) {
            submit();
        }

        pagenavEmployee.checknum = 0;
        pagenavEmployee.deleteBtn.addClass('hide');
        for (var i = 0; i < listRowSize; i += 1) {
            checkEmObjs[i].iCheck('uncheck');
        }

        //Clear All Content
        $('.content.subpage').html("");
        $('.content.main').show();

        //clear focus barcode string
        clearBarCodeStr();
        setFocusPage("moid");

        //Rebind iCheck Event for main form
        checkObjs = pagenavMain.bindCheckEvent(checkObjs);
    });

    // ------Get employee name
    $employeeID.on('change', function () {
        //get row index
        var self = $(this);
        var list = $(this).closest('.list-row');
        var employeeIndex = $tableEmList.find('.list-row').index(list);
        //var $singleEmName = list.find('.employee-name');
        var $singleTime = list.find(".single-work-time");

        if (!common.checkUserIdFormat($(this)))
            return;

        var employeeStr = $(this).val();

        var repeatFlag = true;
        $listEmRow.each(function (index, elm) {
            if (index !== employeeIndex) {
                var $emid = $(elm).find(".employee-id");
                if (employeeStr === $emid.val()) {
                    self.val("");
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: "重複資料，請重新輸入 !",
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                    repeatFlag = false;
                }
            }
        });
        if (!repeatFlag) {
            return;
        }

        var source = { Code: employeeStr };

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/SFTClient/api/Check/EmployeeCheck/",
            data: JSON.stringify(source),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () { WA.common.ajaxBlockUI(); },
            complete: function () { $.unblockUI(); },
            success: function (data) {
                if (data.Result == "success") {
                    //auto jump to correct page by Index
                    pagenavEmployee.jumpPageByIndex(employeeIndex);

                    list.find(".employee-name").val(data.Name);
                    $singleTime.val("0");

                    $emTime.prop("disabled", false);
                }
                else {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: data.SysMsg,
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                    if (debuggerMode) {
                        //auto jump to correct page by Index
                        pagenavEmployee.jumpPageByIndex(employeeIndex);

                        list.find(".employee-name").val("Steven");
                        $singleTime.val("0");

                        $emTime.prop("disabled", false);
                    } else {
                        self.val("");
                    }
                }
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
                self.val("");
            }
        });

    });

}

wa.subpagehnd.prototype.creatTableListEmployee =
function (detailIndex, mainPageinfo, submit) {
    //debugger;

    var recordList = mainPageinfo.recordList;
    var bindPageBtnEvent = mainPageinfo.bindPageBtnEvent;
    var clearBarCodeStr = mainPageinfo.clearBarCodeStr;
    var setFocusPage = mainPageinfo.setFocusPage;
    var itemPerPage = mainPageinfo.itemPerPage; 
    var listRowSize = mainPageinfo.listRowSize; 
    var pagenavMain = mainPageinfo.pagenavMain;
    var checkObjs = mainPageinfo.checkObjs;
    var enableProp = mainPageinfo.enableProp;

    //pageEmployeeIndex = 0;
    var dIndex = detailIndex;
    var workerList = [];
    /// load default data to workerList
    if (recordList[dIndex]) {
        workerList = recordList[dIndex].hasOwnProperty("Work") ? recordList[dIndex].Work : [];
    }
    var visibleEmLines = [];
    var checkEmObjs = [];
    var $tableEmList = $('.table-list.employee');
    var $listEmRow = $tableEmList.find('.list-row');
    var $prevEmployeePage = $('#prevEmployeePage');
    var $nextEmployeePage = $('#nextEmployeePage');
    var $saveEmCloseBtn = $('#saveEmCloseBtn');
    var $emDeleteBtn = $('#emDeleteBtn');

    var $employeeName = $('.employee-name');
    var $employeeID = $('.employee-id');
    var $emTime = $('.single-work-time');

    var pagenavEmployee = new wa.pagenav($tableEmList, $emDeleteBtn, $nextEmployeePage, $prevEmployeePage, visibleEmLines, itemPerPage, listRowSize);

    //clear focus barcode string
    clearBarCodeStr();
    setFocusPage( "worktime" );

    //ui.bindVirtualKB();
    ui.bindInputStyle();
    checkEmObjs = pagenavEmployee.bindCheckEvent(checkEmObjs);

    //show hide page
    visibleEmLines = pagenavEmployee.showTablePage();
    pagenavEmployee.navBtnDisplay();
    bindPageBtnEvent(pagenavEmployee, $nextEmployeePage, $prevEmployeePage);

    //load recorded Employee Data
    if (workerList.length !== 0) {
        //loadEmployeeData();
        $listEmRow.each(function (index, elm) {
            var $e_id = $(elm).find(".employee-id");
            var $e_name = $(elm).find(".employee-name");
            var $e_time = $(elm).find(".single-work-time");
            if (workerList[index]) {
                $e_id.val(workerList[index].User);
                $e_name.val(workerList[index].Name);
                $e_time.val(workerList[index].Times);
            } else {
                $e_id.add($e_name).add($e_time).prop("disabled", true);
            }
        });
    } else {
        $employeeID.add($employeeName).add($emTime).prop("disabled", true);
    }

    //dynamic init keyboard when click input
    $emTime.on('mousedown', function () {
        var self = $(this);
        var list = $(this).closest('.list-row');
        var $singleId = list.find('.employee-id');
        if ($singleId.val() !== "") {
            self.keyboard({
                layout: 'custom',
                restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
                preventPaste: true,  // prevent ctrl-v and right click
                autoAccept: true,
                display: {
                    'b': '\u2190:Backspace',
                    'bksp': 'Bksp:Backspace'
                },
                customLayout: {
                    'default': [
                      '{clear} {dec} {b}',
                      '7 8 9',
                      '4 5 6',
                      '1 2 3',
                      '0 {a} {c}'
                    ]
                }
            });
        } else {
            self.prop('disabled', true);
        }
    });


    //----- employee Delete checked row
    $emDeleteBtn.on('click', function () {

        var workerListTmp = [];
        var workerListTmpNew = [];

        //----- Record non-checked item and delete
        $listEmRow.each(function (index, elm) {

            var $employeeID = $(elm).find(".employee-id");
            var $employeeName = $(elm).find(".employee-name");
            var $employeeTime = $(elm).find(".single-work-time");


            //var list = $(this).closest('.list-row');
            //var employeeIndex = $tableEmList.find('.list-row').index(list);
            if ($employeeID.val() !== "") {
                workerListTmp[index] = {};
                workerListTmp[index].User = $employeeID.val();
                workerListTmp[index].Name = $employeeName.val();
                workerListTmp[index].Times = $employeeTime.val();
                workerListTmp[index].Delete = $(elm).hasClass('selected');
            }

            //clear all content and num value
            $employeeID.val("");
            $employeeName.val("");
            $employeeTime.val("");
        });

        // --- Delete selected object


        for (var i = 0, len = workerListTmp.length; i < len; i += 1) {
            if (workerListTmp[i]) {
                if (!workerListTmp[i].Delete) {
                    workerListTmpNew.push(workerListTmp[i]);
                }
            }
        }

        var max_num = workerListTmpNew.length;
        //auto jump to correct page by max_num
        pagenavEmployee.jumpPageByIndex(max_num - 1);

        //---- Refill in table
        $listEmRow.each(function (index, elm) {
            var $employeeID = $(elm).find(".employee-id");
            var $employeeName = $(elm).find(".employee-name");
            var $employeeTime = $(elm).find(".single-work-time");

            if (index < max_num) {
                $employeeID.val(workerListTmpNew[index].User);
                $employeeName.val(workerListTmpNew[index].Name);
                $employeeTime.val(workerListTmpNew[index].Times);
            }
        });

        workerList = workerListTmpNew;

        if (max_num == 0)
        {
            if (recordList[dIndex].Moid !== "") {
                //console.log("no input detail, but moid has value, give Work[] empty array")
                recordList[dIndex].Work = [];

                //enable front from input
                //$trackAvrTime.add($trackManNum).prop('disabled', false);
                if (enableProp) enableProp(true);
            }
        }

        pagenavEmployee.checknum = 0;
        pagenavEmployee.deleteBtn.addClass('hide');
        for (var i = 0; i < listRowSize; i += 1) {
            checkEmObjs[i].iCheck('uncheck');
        }
    });

    // ----Update, Close employee page
    // and save data to front page
    $saveEmCloseBtn.on('click', function () {
        //clear
        workerList = [];
        var totalWorkTime = 0;

        $listEmRow.each(function (index, elm) {
            var $e_id = $(elm).find(".employee-id");
            var $e_name = $(elm).find(".employee-name");
            var $e_time = $(elm).find(".single-work-time");

            var e_id = $e_id.val();
            var e_name = $e_name.val();
            var e_time = $e_time.val();
            if ($e_name.val() !== "") {
                totalWorkTime += parseFloat(e_time);

                //debugger;
                var w = {
                    User: e_id,
                    Name: e_name,
                    Times: e_time
                };
                workerList.push(w);
            }
        });

        //if inputbox has value
        if (workerList.length > 0) {
            if (recordList[dIndex]) {
                recordList[dIndex].AvrWorkTime = totalWorkTime / workerList.length;
                recordList[dIndex].ManWorkTime = totalWorkTime;
                recordList[dIndex].ManNum = workerList.length;
                recordList[dIndex].Work = workerList;
            }
        } else {

            //workerList.length == 0
            //maybe deleted all items
            //only make Work clear
            if (recordList[dIndex].Moid !== "") {
                //console.log("no input detail, but moid has value, give Work[] empty array")
                recordList[dIndex].Work = [];

                //enable front from input
                //$trackAvrTime.add($trackManNum).prop('disabled', false);
                if (enableProp) enableProp(true);
            }
       }

        //update data to trackout form
        if (recordList.length > 0) {
            if (submit)
                submit();
        }

        //Clear All Content
        $('.content.subpage').html("");
        $('.content.main').show();

        //clear focus barcode string
        clearBarCodeStr();
        setFocusPage("moid");

        //Rebind iCheck Event for main form
        checkObjs = pagenavMain.bindCheckEvent(checkObjs);

    });

    // ------Get employee name
    $employeeID.on('change', function () {
//        console.log('$employeeID.on("change"');
        //get row index
        var self = $(this);
        var list = $(this).closest('.list-row');
        var employeeIndex = $tableEmList.find('.list-row').index(list);
        var $singleEmName = list.find('.employee-name');
        var $singleTime = list.find(".single-work-time");
        
        if (!common.checkUserIdFormat($(this)))
            return;

        var employeeStr = $(this).val();

        var repeatFlag = true;
        $listEmRow.each(function (index, elm) {
            if (index !== employeeIndex) {
                var $emid = $(elm).find(".employee-id");
                if (employeeStr === $emid.val()) {
                    self.val("");
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: "重複資料，請重新輸入 !",
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                    repeatFlag = false;
                }
            }
        });
        if (!repeatFlag) {
            return;
        }

        var source = { Code: employeeStr };

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/SFTClient/api/Check/EmployeeCheck/",
            data: JSON.stringify(source),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () { WA.common.ajaxBlockUI(); },
            complete: function () { $.unblockUI(); },
            success: function (data) {
                if (data.Result == "success") {
                    //auto jump to correct page by Index
                    pagenavEmployee.jumpPageByIndex(employeeIndex);

                    $singleEmName.val(data.Name);
                    //set current avr worktime default
                    $singleTime.val(recordList[dIndex].AvrWorkTime);

                    $emTime.prop("disabled", false);
                }
                else {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: data.SysMsg,
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                    if (global.debuggerMode) {
                        //auto jump to correct page by Index
                        pagenavEmployee.jumpPageByIndex(employeeIndex);

                        $singleEmName.val("Seven");
                        //set current avr worktime default
                        $singleTime.val(recordList[dIndex].AvrWorkTime);

                        $emTime.prop("disabled", false);
                    } else {
                        self.val("");
                    }
                }
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
                self.val("");
            }
        });

    });

}

wa.subpagehnd.prototype.creatTableListException =
function (detailIndex, mainPageinfo) {
    //debugger;

    var recordList = mainPageinfo.recordList;
    var bindPageBtnEvent = mainPageinfo.bindPageBtnEvent;
    var clearBarCodeStr = mainPageinfo.clearBarCodeStr;
    var setFocusPage = mainPageinfo.setFocusPage;
    var itemPerPage = mainPageinfo.itemPerPage;
    var listRowSize = mainPageinfo.listRowSize;
    var pagenavMain = mainPageinfo.pagenavMain;
    var checkObjs = mainPageinfo.checkObjs;

    var dIndex = detailIndex;
    var exCodeList = [];
    /// load default data to workerList
    if (recordList[dIndex]) {
        exCodeList = recordList[dIndex].hasOwnProperty("Exception") ? recordList[dIndex].Exception : [];
    }
    var visibleExLines = [];
    var checkExObjs = [];
    var $tableExList = $('.table-list.exception');
    var $listExRow = $tableExList.find('.list-row');
    var $prevExceptionPage = $('#prevExceptionPage');
    var $nextExceptionPage = $('#nextExceptionPage');
    var $saveExCloseBtn = $('#saveExCloseBtn');
    var $exDeleteBtn = $('#exDeleteBtn');

    var $exceptionCode = $('.exception-code');
    var $exceptionName = $('.exception-name');
    var $exceptionQty = $('.exception-qty');

    var pagenavException = new wa.pagenav($tableExList, $exDeleteBtn, $nextExceptionPage, $prevExceptionPage, visibleExLines, itemPerPage, listRowSize);

    //clear focus barcode string
    clearBarCodeStr();
    setFocusPage("exception");

    //ui.bindVirtualKB();
    ui.bindInputStyle();
    checkExObjs = pagenavException.bindCheckEvent(checkExObjs);

    //--- init Obj- show hide row
    visibleExLines = pagenavException.showTablePage();
    pagenavException.navBtnDisplay();
    bindPageBtnEvent(pagenavException, $nextExceptionPage, $prevExceptionPage);

    //--- init Obj- load recorded Data
    if (exCodeList.length !== 0) {
        //loadEmployeeData();
        $listExRow.each(function (index, elm) {
            var $exCode = $(elm).find(".exception-code");
            var $exName = $(elm).find(".exception-name");
            var $exQty = $(elm).find(".exception-qty");
            if (exCodeList[index]) {
                $exCode.val(exCodeList[index].Code);
                $exName.val(exCodeList[index].Name);
                $exQty.val(exCodeList[index].Qty);
            } else {
                $exCode.add($exName).add($exQty).prop("disabled", true);
            }
        });
    } else {
        $exceptionCode.add($exceptionName).add($exceptionQty).prop("disabled", true);
    }

    //dynamic init keyboard when click input
    $exceptionQty.on('mousedown', function () {
        var self = $(this);
        var list = $(this).closest('.list-row');
        var $singleCode = list.find('.exception-code');
        if ($singleCode.val() !== "") {
            self.keyboard({
                layout: 'custom',
                restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
                preventPaste: true,  // prevent ctrl-v and right click
                autoAccept: true,
                display: {
                    'b': '\u2190:Backspace',
                    'bksp': 'Bksp:Backspace'
                },
                customLayout: {
                    'default': [
                      '{clear} {dec} {b}',
                      '7 8 9',
                      '4 5 6',
                      '1 2 3',
                      '0 {a} {c}'
                    ]
                }
            });
        } else {
            self.prop('disabled', true);
        }
    });


    //----- employee Delete checked row
    $exDeleteBtn.on('click', function () {

        var exCodeListTmp = [];
        var exCodeListTmpNew = [];
        //----- Record non-checked item and delete
        $listExRow.each(function (index, elm) {
            var $exCode = $(elm).find(".exception-code");
            var $exName = $(elm).find(".exception-name");
            var $exQty = $(elm).find(".exception-qty");

            if ($exCode.val() !== "") {
                exCodeListTmp[index] = {};
                exCodeListTmp[index].Code = $exCode.val();
                exCodeListTmp[index].Name = $exName.val();
                exCodeListTmp[index].Qty = $exQty.val();
                exCodeListTmp[index].Delete = $(elm).hasClass('selected');
            }

            //clear item value
            $exCode.val("");
            $exName.val("");
            $exQty.val("0");
        });
        // --- Delete selected object, select item Delete property is false
        for (var i = 0, len = exCodeListTmp.length; i < len; i += 1) {
            if (exCodeListTmp[i]) {
                if (!exCodeListTmp[i].Delete) {
                    exCodeListTmpNew.push(exCodeListTmp[i]);
                }
            }
        }

        var max_num = exCodeListTmpNew.length;
        //auto jump to correct page by max_num
        pagenavException.jumpPageByIndex(max_num - 1);

        //---- Refill in table
        $listExRow.each(function (index, elm) {
            var $exCode = $(elm).find(".exception-code");
            var $exName = $(elm).find(".exception-name");
            var $exQty = $(elm).find(".exception-qty");

            if (index < max_num) {
                $exCode.val(exCodeListTmpNew[index].Code);
                $exName.val(exCodeListTmpNew[index].Name);
                $exQty.val(exCodeListTmpNew[index].Qty);
            }
        });

        exCodeList = exCodeListTmpNew;


        pagenavException.checknum = 0;
        pagenavException.deleteBtn.addClass('hide');
        for (var i = 0; i < listRowSize; i += 1) {
            checkExObjs[i].iCheck('uncheck');
        }
    });

    // ----Update, hide exception page
    // and save data to front page
    $saveExCloseBtn.on('click', function () {

        //clear
        exCodeList = [];

        $listExRow.each(function (index, elm) {
            var $exCode = $(elm).find(".exception-code");
            var $exName = $(elm).find(".exception-name");
            var $exQty = $(elm).find(".exception-qty");

            if ($exCode.val() !== "") {
                var ex = {
                    Code: $exCode.val(),
                    Name: $exName.val(),
                    Qty: $exQty.val()
                };
                exCodeList.push(ex);
            }
        });

        //if inputbox has value
        if (exCodeList.length > 0) {
            if (recordList[dIndex]) {
                recordList[dIndex].Exception = exCodeList;
            }
        }
        else {
            if (recordList[dIndex].Moid !== "") {
                recordList[dIndex].Exception = [];
            }
        }

        pagenavException.checknum = 0;
        pagenavException.deleteBtn.addClass('hide');
        for (var i = 0; i < listRowSize; i += 1) {
            checkExObjs[i].iCheck('uncheck');
        }

        //Clear All Content
        $('.content.subpage').html("");
        $('.content.main').show();

        //clear focus barcode string
        clearBarCodeStr();
        setFocusPage("moid");

        //Rebind iCheck Event for main form
        checkObjs = pagenavMain.bindCheckEvent(checkObjs);
    });

    // ------ Get exception name
    $exceptionCode.on('change', function () {
        //get row index
        var self = $(this);
        var list = $(this).closest('.list-row');
        var exIndex = $tableExList.find('.list-row').index(list);
        var $singleExName = list.find('.exception-name');
        //var eID = $(this).val();        

        if (!common.checkExceptionFormat($(this), "normal"))
            return;

        var codeStr = $(this).val();

        var repeatFlag = true;
        $listExRow.each(function (index, elm) {
            if (index !== exIndex) {
                var $excode = $(elm).find(".exception-code");
                if (codeStr === $excode.val()) {
                    self.val("");
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: "重複資料，請重新輸入 !",
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                    repeatFlag = false;
                }
            }
        });
        if (!repeatFlag) {
            return;
        }

        var source = { CheckType: '001', Code: codeStr };

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/SFTClient/api/Check/ExceptionCheck/",
            data: JSON.stringify(source),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () { WA.common.ajaxBlockUI(); },
            complete: function () { $.unblockUI(); },
            success: function (data) {
                if (data.Result == "success") {
                    //auto jump to correct page by Index
                    pagenavException.jumpPageByIndex(exIndex);

                    $singleExName.val(data.Name);
                    $exceptionQty.prop("disabled", false);
                }
                else {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: data.SysMsg,
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                    if (debuggerMode) {
                        //auto jump to correct page by Index
                        pagenavException.jumpPageByIndex(exIndex);
                        $singleExName.val("冷卻系統故障");
                        $exceptionQty.prop("disabled", false);
                    } else {
                        self.val("");
                    }
                }
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
                self.val("");
            }
        });
    });
}

}(WA, this));