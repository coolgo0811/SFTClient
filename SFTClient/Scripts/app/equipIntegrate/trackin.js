﻿
$(function (wa, global) {
    var ui = wa.ui;
    var common = wa.common;

    var $trackMoid = $('.trackMoid');
    var $trackQty = $('.trackQty');
    var $checkinQty = $('.checkinQty');
    var $checkoutQty = $('.checkoutQty');
    var $btnTrackin = $('#btnTrackin');
    var $btnTrackout = $('#btnTrackout');

    var barCodeStr = "";

    init();
    bindEvent();

    function init() {
        common.autoLogout();
      
    }
    function bindEvent() {
        //--autofocus
        $(document).on("keypress", function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                $trackMoid.val(barCodeStr);
                barCodeStr = "";
                $trackMoid.change();
            } else {
                barCodeStr = barCodeStr + String.fromCharCode(keyCode);
            }
        });

        $trackQty.on('mousedown', function () {
            var self = $(this);

            if ($trackMoid.val() == "") {
                self.prop('disabled', true);
            } else {
                self.prop('disabled', false);
                self.keyboard({
                    layout: 'custom',
                    restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
                    preventPaste: true,  // prevent ctrl-v and right click
                    autoAccept: true,
                    display: {
                        'b': '\u2190:Backspace',
                        'bksp': 'Bksp:Backspace'
                    },
                    customLayout: {
                        'default': [
                          '{clear} {dec} {b}',
                          '7 8 9',
                          '4 5 6',
                          '1 2 3',
                          '0 {a} {c}'
                        ]
                    }
                });
            }
        });

        $trackQty.on('change', function () {
            var self = $(this);
            if (parseInt(self.val()) == 0) {
                $btnTrackin.addClass('hidden');
                $btnTrackout.removeClass('hidden');
            }
            else {
                $btnTrackin.removeClass('hidden');
                $btnTrackout.addClass('hidden');

                if (parseFloat(self.val()) > parseFloat($checkinQty.val())) {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: "欲進站數量不可大於製令的可進站數量 !",
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                    self.val($checkinQty.val());
                }
            }
        });

        $trackMoid.on('change', function () {
            if ($trackMoid.val() == "")
                return;

            var self = $(this);
            var source = {
                Moid: self.val()
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "/SFTClient/api/Check/TrackMoCheck/",
                data: JSON.stringify(source),
                contentType: "application/json; charset=utf-8",
                beforeSend: function () { common.ajaxBlockUI(); },
                complete: function () { $.unblockUI(); },
                success: function (output) {
                    if (output.Result == "success") {
                        $trackQty.val(output.CheckInQty);
                        $checkinQty.val(output.CheckInQty);
                        $checkoutQty.val(output.CheckOutQty);

                        if (self.val() !== "") {
                            $trackQty.prop('disabled', false);
                        }

                        if (parseInt(output.CheckInQty) == 0) {
                            $btnTrackin.addClass('hidden');
                            $btnTrackout.removeClass('hidden');
                        }
                        else {
                            $btnTrackin.removeClass('hidden');
                            $btnTrackout.addClass('hidden');
                        }
                    }
                    else {
                        ui.modalView({
                            modalHeader: "系統訊息",
                            modalBody: output.SysMsg,
                            modalFooter: "msgMode",
                            alert: true,
                            size: { w: 350, h: 200 }
                        });
                        clear();
                    }
                },
                error: function (error) {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: error.statusText,
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                    clear();
                }
            });
        });

        $("#btnLogout").on('click', common.logout);

        //----- Submit
        $btnTrackin.on('click', function () {
            var submitObj = {
                RecordList: [{ Moid: $trackMoid.val(), Qty: $trackQty.val() }]
            };

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "/SFTClient/api/WorkReport/Trackin/",
                data: JSON.stringify(submitObj),
                contentType: "application/json; charset=utf-8",
                beforeSend: function () { WA.common.ajaxBlockUI(); },
                complete: function () { $.unblockUI(); },
                success: function (output) {
                    if (output.Result == "success") {
                        ui.modalView({
                            modalHeader: "系統訊息",
                            modalBody: output.SysMsg,
                            modalFooter: "msgMode",
                            onClose: function () { location.href = "/SFTClient/EquipIntegrate/Trackout" + location.search + "&moid=" + $trackMoid.val(); },
                            size: { w: 350, h: 200 }
                        });
                    }
                    else {
                        ui.modalView({
                            modalHeader: "系統訊息",
                            modalBody: output.SysMsg,
                            modalFooter: "msgMode",
                            alert: true,
                            size: { w: 350, h: 200 }
                        });
                    }
                },
                error: function (error) {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: error.statusText,
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                }
            });
        });

        $btnTrackout.on('click', function () {
            // no trackin, just redirect to trackout page
            location.href = "/SFTClient/EquipIntegrate/Trackout" + location.search + "&moid=" + $trackMoid.val();
        });
    }

    function clear() {
        $trackMoid.val("");
        $trackQty.val("");
        $checkinQty.val("");
        $checkoutQty.val("");
    }
}(WA, this));