﻿
$(function (wa, global) {
    var ui = wa.ui;
    var common = wa.common;

    var $txtEquipment = $('#txtEquipment');
    var barCodeStr = "";

    init();
    bindEvent();

    function init() {
        common.autoLogout();

        if ($txtEquipment.val() != "") {
            location.href = "/SFTClient/EquipIntegrate/Trackin/?equipment=" + $txtEquipment.val();
        }
    }
    function bindEvent(){
        //--autofocus
        $(document).on("keypress", function (e) {
            var keyCode = e.keyCode || e.which;
            if(keyCode === 13){
                $txtEquipment.val("");
                $txtEquipment.val(barCodeStr);
                barCodeStr = "";
                $txtEquipment.change();
            }else{
                barCodeStr = barCodeStr + String.fromCharCode(keyCode);
            }
        });

        $txtEquipment.on('change', function () {
            if (common.checkEquipIdFormat($(this))) {
                $("#btnTrackin").click();
            }
        });

        $("#btnTrackin").on('click', function () {
            location.href = "/SFTClient/EquipIntegrate/Trackin/?equipment=" + $txtEquipment.val();
        });
        $("#btnTrackout").on('click', function () {
            location.href = "/SFTClient/EquipIntegrate/Trackout/?equipment=" + $txtEquipment.val();
        });

        $("#btnLogout").on('click', common.logout);
    }
}(WA, this));