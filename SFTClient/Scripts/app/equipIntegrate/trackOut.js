﻿/// <reference path="../util/common.js" />
$(function (wa, global) {

var $processingTask = $('.table-list.processing');
var $queueTasks = $('.table-list.queue');
var $btnPrev = $('#prevPage');
var $btnNext = $('#nextPage');
var $btnDelete = $('#btnDelete');
var $listRow = $queueTasks.find('.list-row');
var $listRowFirst = $queueTasks.find('.list-row').eq(0);

// current trackout info
var $trackMoid = $(".trackout-content");
var $trackQty = $('.trackout-num');
var $trackMaxQty = $('.trackout-maxnum');
var $trackSumTime = $(".trackout-person-time-sum");
var $trackAvrTime = $(".trackout-person-time");
var $trackDeviceTime = $(".trackout-device-time");
var $trackManNum = $('.trackout-person-num');
var $curCount = $('.current-count');

// next trackout info
var $nextTackMoid = $(".next-trackout-content");
var $nextTackQty = $(".next-trackout-num");
var $nextTackMaxQty = $(".next-trackout-maxnum");

var $signal = $('#signal');

var listRowSize = $listRow.size();
var itemPerPage = 5;
var visibleLines = [];
var recordDetailTotal = {};
var barCodeStr = "";
var focusPage = "moid";
var moidIndex = 0;

var checkObjs = [];
var checknum = 0;

var trackObj = {
    RecordList: []
};
var recordList = trackObj.RecordList;

var ui = wa.ui;
var common = wa.common;
var pagenavMain = new wa.pagenav($queueTasks, $btnDelete, $btnNext, $btnPrev, visibleLines, itemPerPage, listRowSize);

document.write("<OBJECT NAME=ViewObj CLASSID=CLSID:5C2A52BD-2250-4F6B-A4D2-D1D00FCD748C WIDTH=0 HEIGHT=0 STYLE=position:absolute;display:none>");
document.write("</OBJECT>");

var viewHwnd = $('#hwnd').text();

var varEquipId = common.getUrlVariables("equipment");
var varMoid = common.getUrlVariables("moid");

// sub-page setting /////////
var subPageHnd = new wa.subpagehnd();
var mainPageinfo = {
    recordList:recordList,
    bindPageBtnEvent:bindPageBtnEvent,
    clearBarCodeStr:clearBarCodeStr, 
    setFocusPage:setFocusPage, 
    itemPerPage:itemPerPage, 
    listRowSize:listRowSize, 
    pagenavMain:pagenavMain, 
    checkObjs: checkObjs,
    enableProp: enableProp,
    saveRecordDetailTotal: saveRecordDetailTotal
};
function saveRecordDetailTotal(r) { recordDetailTotal = r; }
function clearBarCodeStr() { barCodeStr = ""; }
function setFocusPage(val) { focusPage = val; }
function enableProp(enable) { $trackAvrTime.add($trackManNum).prop('disabled', !enable); }
function employeePageSubmit() {
    var index = 0;
    if (recordList[index]) {
        $trackSumTime.val(recordList[index].ManWorkTime);
        $trackAvrTime.val(recordList[index].AvrWorkTime);
        $trackManNum.val(recordList[index].ManNum);
    }
}
// sub-page setting /////////

init();
bindEvent();

if (varMoid !== "") {
    $trackMoid.val(varMoid);
    $trackMoid.change();
}

function init() {
    pagenavMain.navBtnDisplay();

    //show hide page
    visibleLines = pagenavMain.showTablePage();
    //bind iCheck Event for main form
    checkObjs = pagenavMain.bindCheckEvent(checkObjs);

    if (viewHwnd !== "")
        getEquipmentCount();
}

function curTrackFinished()
{
    var isOver = false;
    var elm = $listRow.eq(0);
    var moid = "", qty = "";

    while (true) {
        moid = $(elm).find(".next-trackout-content").val();
        qty = $(elm).find(".next-trackout-num").val();

        if (moid == "") {
            isOver = true;
            break;
        }

        deleteListRow(deleteListRowFirstItem);

        if (trackin(moid, qty)) {
            $trackMoid.val(moid).change();
            break;
        }
    }
    if (isOver)
        common.logout();
}

function bindEvent() {
    bindInputForm();
    //--autofocus
    bindAutoFocusEvent();
    bindPageBtnEvent(pagenavMain, $btnNext, $btnPrev);

    $('.carry-in-curcount').on('click', function () {
        if ($signal.hasClass('cr-red'))
            return;

        $trackQty.val($curCount.val());
        $signal.removeClass('cr-green');
        $signal.addClass('cr-red');

        resetEquipmentCount();
    });


    // the man time value changed event
    $trackAvrTime.on('change', function () {
        var self = $(this);
        var listIndex = 0;
        var time = parseFloat(self.val());
        if (time == 0) {
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: "不可為0 !",
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
            self.val(recordList[listIndex].AvrWorkTime);   // recover value
            return;
        }

        recordList[listIndex].ManWorkTime = time * recordList[listIndex].ManNum;
        recordList[listIndex].AvrWorkTime = time;
        $trackSumTime.val(recordList[listIndex].ManWorkTime);
    });

    // the man number value changed event
    $trackManNum.on('change', function () {
        var self = $(this);
        var numStr = self.val();
        var listIndex = 0;
        if (numStr.indexOf(".") > -1)
            self.val(parseInt(numStr));
        var num = parseInt(self.val());
        if (num == 0) {
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: "不可為0 !",
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
            self.val(recordList[listIndex].ManNum);   // recover value
            return;
        }

        recordList[listIndex].ManWorkTime = recordList[listIndex].AvrWorkTime * num;
        recordList[listIndex].ManNum = num;
        $trackSumTime.val(recordList[listIndex].ManWorkTime);
    });

    $('#btnEquipStatus').on('click', function () {
        $.ajax({
            url: "/SFTClient/EquipStatus/Index?equipment=" + varEquipId,
            method: "GET",
            dataType: "html",
            success: function (data) {
                $('.content.main').hide();
                $('.content.subpage').html(data);
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
            }
        });
    });

    $('#btnWipHold').on('click', function () {
        $.ajax({
            url: "/SFTClient/WipHold/Index?moid=" + $trackMoid.val(),
            method: "GET",
            dataType: "html",
            success: function (data) {
                $('.content.main').hide();
                $('.content.subpage').html(data);
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
            }
        });
    });

    /// ---- Click Work Time Detail Button
    $('.detailBtn').on('click', function () {
        if ($trackMoid.val() == "") {
            return;
        }
        
        var listIndex = 0;

        $.ajax({
            url: "/SFTClient/Trackinout/TableListEmployee",
            method: "GET",
            dataType: "html",
            success: function (data) {                
                pagenavMain.checknum = 0;
                pagenavMain.deleteBtn.addClass('hide');
                for (var i = 0; i < listRowSize; i += 1) {
                    checkObjs[i].iCheck('uncheck');
                }

                $('.content.main').hide();
                $('.content.subpage').html(data);

                recordList[listIndex].Moid = $trackMoid.val();
                recordList[listIndex].Qty = $trackQty.val();
                recordList[listIndex].ManWorkTime = $trackSumTime.val();
                recordList[listIndex].AvrWorkTime = $trackAvrTime.val();
                recordList[listIndex].WorkTime = $trackDeviceTime.val();
                recordList[listIndex].ManNum = $trackManNum.val();
                recordList[listIndex].Work = recordList[listIndex].hasOwnProperty("Work") ? recordList[listIndex].Work : [];

                subPageHnd.creatTableListEmployee(listIndex, mainPageinfo, employeePageSubmit);
//                creatTableListEmployee(listIndex);
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
            }
        }
        );
    });

    /// ---- Click Work Time Detail Button
    $('.errorDetailBtn').on('click', function () {
        var $list = $(this).closest('.list-row3');
        if ($list.find('.running-content').val() == "") {
            return;
        }
        var listIndex = 0;

        $.ajax({
            url: "/SFTClient/Trackinout/TableListException",
            method: "GET",
            dataType: "html",
            success: function (data) {
                pagenavMain.checknum = 0;
                pagenavMain.deleteBtn.addClass('hide');
                for (var i = 0; i < listRowSize; i += 1) {
                    checkObjs[i].iCheck('uncheck');
                }
                $('.content.main').hide();
                $('.content.subpage').html(data);
                subPageHnd.creatTableListException(listIndex, mainPageinfo);
                //creatTableListException(listIndex);
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
            }
        }
        );

    });

    /// ----- Submit
    $("#btnTrackout").on('click', function () { trackout('3', '1') });
    $("#btnStockin").on('click', function () { trackout('3', '2') });

    $trackQty.on('change', function () {
        checkTrackQty($(this), parseFloat($trackMaxQty.val()), 2);
    });
    $nextTackQty.on('change', function () {
        var listIndex = $nextTackQty.index($(this));
        var maxQty = $nextTackMaxQty.eq(listIndex).val();
        checkTrackQty($(this), parseFloat(maxQty), 1);
    });

    $('#btnEquipLogout').on('click', function () {
        if ($trackMoid.val() != "") {
            var onsubmit = function () { common.logout(); };
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: "製令欄位不為空，是否確認登出?",
                modalFooter: "submitMode",
                onSubmit: onsubmit,
                size: { w: 350, h: 200 }
            });
        }
        else
            common.logout();
    });

    curdAction();
}

function curdAction() {
    //----- Get  row
    $trackMoid.on('change', function () {

        var self = $(this);
        if (self.val() === "")
            return;

        //get row index
        var moidStr = self.val();
        var listIndex = 0;
        // check format
        if (common.checkMoIdFormat(self) == false)
            return;

        var source = {
            Moid: moidStr,
            CheckQtyType: '2'
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/SFTClient/api/Check/MoCheck/",
            data: JSON.stringify(source),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () { WA.common.ajaxBlockUI(); },
            complete: function () { $.unblockUI(); },
            success: function (output) {
                if (output.Result == "success") {
                    if (parseFloat(output.CheckQty) == 0) {
                        ui.modalView({
                            modalHeader: "系統訊息",
                            modalBody: '檢查失敗, 無待出批量 !',
                            modalFooter: "msgMode",
                            alert: true,
                            size: { w: 350, h: 200 }
                        });

                        self.val("");
                    }
                    else {
                        if (recordDetailTotal.Work) {
                            recordList[listIndex] = {};
                            recordList[listIndex].Moid = moidStr;
                            recordList[listIndex].Qty = output.CheckQty;
                            recordList[listIndex].ManWorkTime = recordDetailTotal.ManWorkTime;
                            recordList[listIndex].AvrWorkTime = recordDetailTotal.AvrWorkTime;
                            recordList[listIndex].WorkTime = output.WorkTime;
                            recordList[listIndex].ManNum = recordDetailTotal.ManNum;
                            recordList[listIndex].Exception = [];
                            recordList[listIndex].Work = recordDetailTotal.Work;
                        } else {
                            recordList[listIndex] = {};
                            recordList[listIndex].Moid = moidStr;
                            recordList[listIndex].Qty = output.CheckQty;
                            recordList[listIndex].ManWorkTime = output.ManWorkTime;
                            recordList[listIndex].AvrWorkTime = output.ManWorkTime;//the same as ManWorkTime
                            recordList[listIndex].WorkTime = output.WorkTime;
                            recordList[listIndex].ManNum = 1;
                            recordList[listIndex].Exception = [];
                            recordList[listIndex].Work = [];
                        }

                        $trackQty.val(recordList[listIndex].Qty);
                        $trackMaxQty.val(recordList[listIndex].Qty);
                        $trackSumTime.val(recordList[listIndex].ManWorkTime);
                        $trackAvrTime.val(recordList[listIndex].AvrWorkTime);
                        $trackDeviceTime.val(recordList[listIndex].WorkTime);
                        $trackManNum.val(recordList[listIndex].ManNum);

                        $trackQty.add($trackDeviceTime).add($trackAvrTime).add($trackManNum).prop('disabled', false);
                    }
                }
                else {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: output.SysMsg,
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                    self.val("");
                }
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
                self.val("");                
            }
        });
    });

    $nextTackMoid.on('change', function () {

        var self = $(this);
        if (self.val() == "")
            return;

        //get row index
        var moidStr = self.val();
        var list = $(this).closest('.list-row');
        var listIndex = $listRow.index(list);
        // check format
        if (common.checkMoIdFormat(self) == false)
            return;
        
        // check duplicate with current trackout mo
        var moidBuff = moidStr.split(";");
        if (moidBuff[0] == $trackMoid.val().split(";")[0]) {
            self.val("");
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: "重複的製令 !",
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
            return;
        }

        var repeatFlag = true;
        
        $listRow.each(function (index, elm) {
            if (index !== listIndex) {
                var $moid = $(elm).find(".next-trackout-content");
                if ($moid.val() == "")
                    return false;
                
                if (moidBuff[0] === $moid.val().split(";")[0]) {
                    self.val("");
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: "重複的製令 !",
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                    repeatFlag = false;
                    return false;
                }
            }
        });
        if (!repeatFlag) {
            return;
        }

        var source = {
            Moid: moidStr,
            CheckQtyType: '1'
        };

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/SFTClient/api/Check/MoCheck/",
            data: JSON.stringify(source),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () { WA.common.ajaxBlockUI(); },
            complete: function () { $.unblockUI(); },
            success: function (output) {
                if (output.Result == "success") {
                    if (parseFloat(output.CheckQty) == 0) {
                        ui.modalView({
                            modalHeader: "系統訊息",
                            modalBody: '檢查失敗, 無待出批量 !',
                            modalFooter: "msgMode",
                            alert: true,
                            size: { w: 350, h: 200 }
                        });

                        self.val("");
                    }
                    else {
                        var list = self.closest('.list-row');
                        listIndex = $listRow.index(list);
                        $nextTackQty.eq(listIndex).val(output.CheckQty);
                        $nextTackMaxQty.eq(listIndex).val(output.CheckQty);
                        //auto jump to correct page by listIndex
                        pagenavMain.jumpPageByIndex(listIndex);
                    }
                }
                else {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: output.SysMsg,
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                    self.val("");
                }
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
                self.val("");
            }
        });
    });

    //----- Delete checked row
    $btnDelete.on('click', function () {
        deleteListRow(deleteListRowByCheckObj);
    });
}

function checkTrackQty(self, maxQty, type) {
    if (parseFloat(self.val()) == 0) {
        ui.modalView({
            modalHeader: "系統訊息",
            modalBody: "數量不可以為0 !",
            modalFooter: "msgMode",
            alert: true,
            onClose: function () { self.val(1); },
            size: { w: 350, h: 200 }
        });
    }
    else if (parseFloat(self.val()) > maxQty) {
        var msg = "";
        if (type == 1)
            msg = "欲進站數量不可大於製令的可進站數量 !";
        else if (type == 2)
            msg = "欲出站數量不可大於製令的可出站數量 !";
        ui.modalView({
            modalHeader: "系統訊息",
            modalBody: msg,
            modalFooter: "msgMode",
            alert: true,
            onClose: function () { self.val(maxQty); },
            size: { w: 350, h: 200 }
        });
    }
}

function trackin(moid, qty) {
    var source = {
        RecordList: [{ Moid: moid, Qty: qty }]
    };
    var result = false;
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/SFTClient/api/WorkReport/Trackin/",
        data: JSON.stringify(source),
        contentType: "application/json; charset=utf-8",
        async: false,
        beforeSend: function () { WA.common.ajaxBlockUI(); },
        complete: function () { $.unblockUI(); },
        success: function (output) {
            if (output.Result == "success") {
                result = true;
            }
            else {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: output.SysMsg,
                    modalFooter: "msgMode",
                    alert: true,
                    onClose: function () {
                        result = false;
                    },
                    size: { w: 350, h: 200 }
                });
            }
        },
        error: function (error) {
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: error.statusText,
                modalFooter: "msgMode",
                alert: true,
                onClose: function () {
                    result = false;
                },
                size: { w: 350, h: 200 }
            });
        }
    });

    return result;
};

function trackout(checkOutMode, checkOutType) {
    var index = 0;
    if (recordList[index]) {
        recordList[index].Moid = $trackMoid.val();
        recordList[index].Qty = $trackQty.val();
        recordList[index].ManWorkTime = $trackSumTime.val();
        recordList[index].AvrWorkTime = $trackAvrTime.val();
        recordList[index].WorkTime = $trackDeviceTime.val();
        recordList[index].ManNum = $trackManNum.val();
        recordList[index].Exception = recordList[index].hasOwnProperty("Exception") ? recordList[index].Exception : [];
        recordList[index].Work = recordList[index].hasOwnProperty("Work") ? recordList[index].Work : [];
    }

    var source = {
        CheckOutMode: checkOutMode,
        CheckOutType: checkOutType,
        RecordList: []
    };

    source.RecordList = recordList;

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/SFTClient/api/WorkReport/Trackout/",
        data: JSON.stringify(source),
        contentType: "application/json; charset=utf-8",
        beforeSend: function () { WA.common.ajaxBlockUI(); },
        complete: function () { $.unblockUI(); },
        success: function (data) {
            if (data.Result == "success") {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: data.SysMsg,
                    modalFooter: "msgMode",
                    onClose: function () {
                        if (recordList[index].Qty == parseFloat($trackMaxQty.val())) {
                            curTrackFinished();
                        }
                        else {
                            $trackMoid.val(recordList[index].Moid).change();
                        }

                        $signal.removeClass('cr-red');
                        $signal.addClass('cr-green');
                    },
                    size: { w: 350, h: 200 }
                });
            }
            else {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: data.SysMsg,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
            }
        },
        error: function (error) {
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: error.statusText,
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
        }
    });
}

function deleteListRowByCheckObj(index, elm) { return $(elm).hasClass('selected'); }
function deleteListRowFirstItem(index, elm) { return index == 0 ? true : false; }
function deleteListRow(deleteRule)
{
    var recordListTmp = [];
    var recordListTmpNew = [];

    //----- Record non-checked item and Delete
    $listRow.each(function (index, elm) {
        var $moid = $(elm).find(".next-trackout-content");
        var $qty = $(elm).find(".next-trackout-num");
        var $maxQty = $(elm).find(".next-trackout-maxnum");

        if ($moid.val() !== "") {
            recordListTmp[index] = {};
            recordListTmp[index].Moid = $moid.val();
            recordListTmp[index].Qty = $qty.val();
            recordListTmp[index].MaxQty = $maxQty.val();
            recordListTmp[index].Delete = deleteRule(index, elm);   //$(elm).hasClass('selected');
        }
        else
            return false;   //break

        //clear all content and num value
        $moid.val("");
        $qty.val("");    
    });

    for (var i = 0, len = recordListTmp.length; i < len; i += 1) {
        if (recordListTmp[i]) {
            if (!recordListTmp[i].Delete) {
                recordListTmpNew.push(recordListTmp[i]);
            }
        }
    }

    var max_num = recordListTmpNew.length;
    //auto jump to correct page by max_num
    pagenavMain.jumpPageByIndex(max_num - 1);

    //---- Refill in table
    $listRow.each(function (index, elm) {
        var $moid = $(elm).find(".next-trackout-content");
        var $qty = $(elm).find(".next-trackout-num");
        var $maxQty = $(elm).find(".next-trackout-maxnum");

        if (index < max_num) {
            $moid.val(recordListTmpNew[index].Moid);
            $qty.val(recordListTmpNew[index].Qty);
            $maxQty.val(recordListTmpNew[index].MaxQty);
            $maxQty.val();
        }
        else
            return false;   // break
    });

    pagenavMain.checknum = 0;
    pagenavMain.deleteBtn.addClass('hide');
    for (var i = 0; i < listRowSize; i += 1) {
        checkObjs[i].iCheck('uncheck');
    }
}

function bindInputForm() {
    $trackQty.add($trackAvrTime)
             .add($trackDeviceTime)
             .add($trackManNum)
             .add($nextTackQty)
             .on('mousedown', function () {
        //         console.log('css class: ' + $(this).attr('class'));
        var id = $(this).attr('class');
        var sel_row = "";
        var sel_moid = "";

        if (id.indexOf("next") > -1)
        {
            sel_row = ".list-row";
            sel_moid = ".next-trackout-content";
        }
        else
        {
            sel_row = ".list-row3";
            sel_moid = ".trackout-content";
        }

        var self = $(this);
        var list = $(this).closest(sel_row);
        var $singleMoid = list.find(sel_moid);
        if ($singleMoid.val() !== "") {
            //self.prop('disabled', false);
            self.keyboard({
                layout: 'custom',
                restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
                preventPaste: true,  // prevent ctrl-v and right click
                autoAccept: true,
                display: {
                    'b': '\u2190:Backspace',
                    'bksp': 'Bksp:Backspace'
                },
                customLayout: {
                    'default': [
                      '{clear} {dec} {b}',
                      '7 8 9',
                      '4 5 6',
                      '1 2 3',
                      '0 {a} {c}'
                    ]
                }
            });
        } else {
            self.prop('disabled', true);
        }
    });
}


function bindAutoFocusEvent()
{
    $(document).on("keypress", function (e) {        
        var keyCode = e.keyCode || e.which;
        var saveNextIndex = 0;
        if (keyCode === 13) {
            if (barCodeStr == "")
                return;
            //fill value in input
            switch (focusPage) {
                case "moid":
                    $nextTackMoid.each(function (index, elm) {
                        if ($(elm).val() !== "") {
                            saveNextIndex += 1;
                        }
                    });

                    if (saveNextIndex < listRowSize) {
                        $nextTackMoid.eq(saveNextIndex).val(barCodeStr).change();
                    }
                    barCodeStr = "";
                    break;
                case "worktime":
                    var listEmRow = $('.table-list.employee').find('.list-row');
                    listEmRow.each(function (index, elm) {
                        var $e_id = $(elm).find(".employee-id");
                        if ($e_id.val() !== "") {
                            saveNextIndex += 1;
                        }
                    });
                    if (saveNextIndex < listRowSize) {
                        $('.employee-id').eq(saveNextIndex).val(barCodeStr).change();
                    }
                    console.log(focusPage + ":" + barCodeStr);
                    barCodeStr = "";
                    break;
                case "exception":
                    var listExRow = $('.table-list.exception').find('.list-row');
                    listExRow.each(function (index, elm) {
                        var $exCode = $(elm).find(".exception-code");
                        if ($exCode.val() !== "") {
                            saveNextIndex += 1;
                        }
                    });
                    if (saveNextIndex < listRowSize) {
                        $('.exception-code').eq(saveNextIndex).val(barCodeStr).change();
                    }
                    console.log(focusPage + ":" + barCodeStr);
                    barCodeStr = "";
                    break;
                default:
                    break;
            }

        } else {
            barCodeStr = barCodeStr + String.fromCharCode(keyCode);
       //     console.log('key-code: '+keyCode+' char: '  + String.fromCharCode(keyCode));
        }
    });
}
function bindPageBtnEvent(pagenavObj, nextEmPage, prevEmPage) {
    nextEmPage.on('click', { navObj: pagenavObj }, onNextPage);
    prevEmPage.on('click', { navObj: pagenavObj }, onPrevPage);
}
function onNextPage(event) {
    event.preventDefault();
    var pagenavObj = event.data.navObj;
    pagenavObj.nextPageHandlr();
}
function onPrevPage(event) {
    event.preventDefault();
    var pagenavObj = event.data.navObj;
    pagenavObj.prevPageHandlr();
}

function getEquipmentCount() {
    if (document.ViewObj) {
        $('#currentCount').val(document.ViewObj.GetValue("Count", viewHwnd));
    }
    setTimeout(getEquipmentCount, 1000);
}

function resetEquipmentCount() {
    if (document.ViewObj) {
        document.ViewObj.SetValue("Reset", "1", viewHwnd);
    }
}

}(WA, this));