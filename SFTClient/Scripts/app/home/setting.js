﻿$(function (wa, global) {
    var ui = wa.ui;

    var $tab = $('.tab');

    $tab.find('li').on('click',function(e){
        $tab.find('li').removeClass('active');
        $(this).addClass('active');
    });

    GetSetting();
    function GetSetting() {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "/SFTClient/api/SysSetting/Get/",
            beforeSend: function () { WA.common.ajaxBlockUI(); },
            complete: function () { $.unblockUI(); },
            success: function (data) {
                if (data.IsDHCP == true){
                    $('#dhcp').addClass("active");
                    $('#static').removeClass("active");
                }
                else {
                    $('#static').addClass("active");
                    $('#dhcp').removeClass("active");
                }
                if (data.LocalIP) {
                    var buff = data.LocalIP.split(".");
                    if (buff.length == 4) {
                        $('#localIP1').val(buff[0]);
                        $('#localIP2').val(buff[1]);
                        $('#localIP3').val(buff[2]);
                        $('#localIP4').val(buff[3]);
                    }
                }
                if (data.LocalMask) {
                    buff = data.LocalMask.split(".");
                    if (buff.length == 4) {
                        $('#localMask1').val(buff[0]);
                        $('#localMask2').val(buff[1]);
                        $('#localMask3').val(buff[2]);
                        $('#localMask4').val(buff[3]);
                    }
                }
                if (data.LocalGateway) {
                    buff = data.LocalGateway.split(".");
                    if (buff.length == 4) {
                        $('#localGateway1').val(buff[0]);
                        $('#localGateway2').val(buff[1]);
                        $('#localGateway3').val(buff[2]);
                        $('#localGateway4').val(buff[3]);
                    }
                }
                if (data.ServerIP) {
                    buff = data.ServerIP.split(".");
                    if (buff.length == 4) {
                        $('#serverIP1').val(buff[0]);
                        $('#serverIP2').val(buff[1]);
                        $('#serverIP3').val(buff[2]);
                        $('#serverIP4').val(buff[3]);
                    }
                }
                $('#serverPort').val(data.ServerPort);
                $('#serverTimeout').val(data.ServerTimeout);
                $('#username').val(data.UserName);
                $('#password').val(data.Password);
                $('#autoLogoutTime').val(data.AutoLogoutTime);
                $('#company').val(data.CompanyId);
                $('#equipment').val(data.Equipment);
                $('#equipDataCollection').val(data.EquipDataCollection);
                $('#systemModel').val(data.SystemModel);

                data.LiveUpdate ? $('#liveUpdate').iCheck('check') : $('#liveUpdate').iCheck('uncheck');
                data.EquipTrackinoutModel ? $('#equipTrackinoutModel').iCheck('check') : $('#equipTrackinoutModel').iCheck('uncheck');
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
            }
        });
    }

    $('#btnSumit').on('click', function () {
        // check
        if ($('#username').val() != "" && $('#password').val() == "") {
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: "密碼不可以為空 !",
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
            return;
        }
        if ($('#equipTrackinoutModel').prop('checked') && $('#equipment').val() == "") {
            ui.modalView({
                modalHeader: "系統訊息",
                modalBody: "預設機台代碼不可以為空 !",
                modalFooter: "msgMode",
                alert: true,
                size: { w: 350, h: 200 }
            });
            return;
        }

        SetSetting();
    });

    function SetSetting() {
        var aryLocalIP = [$('#localIP1').val(), $('#localIP2').val(), $('#localIP3').val(), $('#localIP4').val()];
        var aryLocalMask = [$('#localMask1').val(), $('#localMask2').val(), $('#localMask3').val(), $('#localMask4').val()];
        var aryLocalGateway = [$('#localGateway1').val(), $('#localGateway2').val(), $('#localGateway3').val(), $('#localGateway4').val()];
        var aryServerIP = [$('#serverIP1').val(), $('#serverIP2').val(), $('#serverIP3').val(), $('#serverIP4').val()];
        var source = {
            'IsDHCP': $('#dhcp').hasClass("active"),
            'LocalIP': aryLocalIP.join("."),
            'LocalMask': aryLocalMask.join("."),
            'LocalGateway': aryLocalGateway.join("."),
            'ServerIP': aryServerIP.join("."),
            'ServerPort': $('#serverPort').val(),
            'ServerTimeout': $('#serverTimeout').val(),
            'UserName': $('#username').val(),
            'Password': $('#password').val(),
            'AutoLogoutTime': $('#autoLogoutTime').val(),
            'CompanyId': $('#company').val(),
            'Equipment': $('#equipment').val(),
            'EquipDataCollection': $('#equipDataCollection').val(),
            'SystemModel': $('#systemModel').val(),
            'LiveUpdate': $('#liveUpdate').prop('checked'),
            'EquipTrackinoutModel': $('#equipTrackinoutModel').prop('checked')
        }
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/SFTClient/api/SysSetting/Set/",
            data: JSON.stringify(source),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () { WA.common.ajaxBlockUI(); },
            complete: function () { $.unblockUI(); },
            success: function () {
                location.href = "/SFTClient/Home/Index";
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
            }
        });
    }
}(WA, this));