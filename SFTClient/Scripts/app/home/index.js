﻿
$(function (wa, global) {
    var ui = wa.ui;
    var common = wa.common;

    init();
    bindEvent();

    function init() {
        layout();
        checkOnline();
        isServConneted();
    }

    function bindEvent() {
        // system setting portal
        $(".top-settings").on("click", function () {
            var modalheader = "登入";
            var modalbody = '<div class="input-group">' +
              '<span class="input-group-title cell">帳號:</span>' +
              '<input class="modal-form-id form-control w60 cell last qwerty" type="text" />' +
              '</div>' +
              '<div class="input-group">' +
              '<span class="input-group-title cell">密碼:</span>' +
              '<input class="modal-form-pw form-control w60 cell last qwerty" type="password" >' +
              '</div>' +
              '<div class="input-group" style="margin-bottom: 0;margin-top:0px;">' +
              '<span class="input-group-title cell" style="line-height: 25px;height:25px;"></span>' +
              '<span id="errorMsg" style="color:#ff6e7d"></span>' +
              '</div>';
            var modalfooter = "submitMode";
            var modalsize = { h: "auto" };
            var onsubmit = function () {
                //validation id and password
                if ($(".modal-form-id").val() == "si" && $(".modal-form-pw").val() == "si") {
                    var $modalView = $('.modal-view');
                    var $modalFrame = $modalView.find(".modal-frame");
                    var $mheader = $modalView.find('.modal-header');
                    var $mbody = $modalView.find('.modal-body');
                    var $mfooter = $modalView.find('.modal-footer');
                    ui.closeModal($modalView, $modalFrame, $mheader, $mbody, $mfooter);
                    location.href = "/SFTClient/Home/Settings";
                }
                else {
                    $('#errorMsg').text("帳號密碼錯誤 !");
                }
            };

            var modalObj = {
                modalHeader: modalheader,
                modalBody: modalbody,
                modalFooter: modalfooter,
                size: modalsize,
                autoClose: false,
                onSubmit: onsubmit
            };
            ui.modalView(modalObj);
        });

        $("#btnShutdown").on("click", function () {
            $.ajax({
                type: "GET",
                dataType: "json",
                url: "/SFTClient/api/SysSetting/Shutdown/",
                contentType: "application/json; charset=utf-8",
                beforeSend: function () { common.ajaxBlockUI(); },
                complete: function () { $.unblockUI(); },
                success: function (output) {
                    if (output != "") {
                        ui.modalView({
                            modalHeader: "系統訊息",
                            modalBody: output,
                            modalFooter: "msgMode",
                            size: { w: 350, h: 200 }
                        });
                    }
                },
                error: function (error) {
                    ui.modalView({
                        modalHeader: "系統訊息",
                        modalBody: error.statusText,
                        modalFooter: "msgMode",
                        alert: true,
                        size: { w: 350, h: 200 }
                    });
                }
            });
        });
    }

    // layout by system setting
    function layout() {
        /*var paramValue = common.getSysParameter("EquipTrackinoutModel");
        var model = paramValue === 'True';
        if (model) {
            $('#trackinout').addClass('hide');
            $('#simpleTrack').addClass('hide');
            $('#none').removeClass('hide');
            $('#equipIntegrate').removeClass('hide');
        }
        else {
            $('#trackinout').removeClass('hide');
            $('#simpleTrack').removeClass('hide');
            $('#none').addClass('hide');
            $('#equipIntegrate').addClass('hide');
        }*/
    }
 
    function isServConneted() {
        if (location.pathname.indexOf("Settings") != -1)
            return;

        $.ajax({
            type: "GET",
            dataType: "json",
            url: "/SFTClient/api/SysSetting/ServStatus/",
            contentType: "application/json; charset=utf-8",
            beforeSend: function () { common.ajaxBlockUI(); },
            complete: function () { $.unblockUI(); },
            success: function (output) {
                if (output == false) {
                    if (location.pathname.indexOf("Home/Index") != -1 && $('#network').find('img.offline').hasClass('hide')) {  // online -> offline
                        ui.modalView({
                            modalHeader: "系統訊息",
                            modalBody: "伺服器連線異常 !",
                            modalFooter: "msgMode",
                            alert: true,
                            size: { w: 350, h: 200 }
                        });
                    }

                    if (location.pathname.indexOf("Home/Index") == -1) {
                        location.href = "/SFTClient/Home/Index";
                    }

                    $('#network').find('img.hide').removeClass('hide');
                    $('#network').find('img.online').addClass('hide');
                }
                else {
                    $('#network').find('img.hide').removeClass('hide');
                    $('#network').find('img.offline').addClass('hide');
                }
            },
            error: function (error) {
                ui.modalView({
                    modalHeader: "系統訊息",
                    modalBody: error.statusText,
                    modalFooter: "msgMode",
                    alert: true,
                    size: { w: 350, h: 200 }
                });
                if (location.pathname.indexOf("Home/Index") == -1) {
                    location.href = "/SFTClient/Home/Index";
                }

                $('#network').find('img.hide').removeClass('hide');
                $('#network').find('img.online').addClass('hide');
            }
        });
        setTimeout(isServConneted, 1000 * 60 * 3);    // 3mins
    }

    function checkOnline() {
        if (typeof (navigator.onLine) != "undefined") {
            networkHandler();
            $(window).on("online offline", networkHandler);
        }
        else
            alert("browser isn't support");
    }
    function networkHandler() {
        var status = navigator.onLine;
        if (status) {
            //ONLINE
            $('#network').find('img.hide').removeClass('hide');
            $('#network').find('img.offline').addClass('hide');
        }
        else {
            //OffLINE
            $('#network').find('img.hide').removeClass('hide');
            $('#network').find('img.online').addClass('hide');
        }
    }
}(WA, this));
