<%
dim tMsgObj, projName, nodeName
dim actPage
dim tObj, WebAccessServerPath
dim hwnd
%>
<!--#include file="../include/gChkCook.asp"-->
<!--#include file="../include/strdef.asp"-->
<!--#include file="../include/gChkEventTbl.asp"-->
<!--#include file="../include/gDbRec1.asp"-->
<%
ChkCookie
%>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <title>SFT Client</title>
    <script type="text/javascript" src="vendor/jquery-1.8.3.js"></script>
</head>
<body>
    <%
    On Error Resume Next

    set tObj = Server.CreateObject("webdobj.webdraw")
	WebAccessServerPath = tObj.GetServerPath
	set tObj = nothing

	projName = Session("ProjName")
    nodeName = Session("NodeName")

    if Len(projName)=0 then
        'projName = CStr(Request.QueryString("proj"))
        projName = Server.UrlEncode(Request.QueryString("proj"))
    end if
    if Len(nodeName)=0 then
        'nodeName = CStr(Request.QueryString("scada"))
        nodeName = Server.UrlEncode(Request.QueryString("scada"))
	    if "" = nodeName then
		    'nodeName = CStr(Request.QueryString("node"))
            nodeName = Server.UrlEncode(Request.QueryString("node"))
        end if
    end if

    hwnd = CStr(Request.QueryString("hwnd"))

    actPage = "/SFTClient/Home/Index?proj=" & Server.UrlEncode(projName) & "&node=" & Server.UrlEncode(nodeName) & "&hwnd=" & hwnd

    %>
    <form name="toaspx" id="toaspx" action="<%=actPage %>" method="post">
    <input type="hidden" name="projName" value="<%=projName %>" />
    <input type="hidden" name="nodeName" value="<%=nodeName %>" />
    <input type="hidden" name="waPath" value="<%=WebAccessServerPath %>" />
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            document.forms['toaspx'].submit();
        });
    </script>
</body>
</html>