﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgReworkIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public ReworkInData Data;

        public MsgReworkIn()
        {
            base.DoAction = "080";
            base.DoCase = "011";
            Data = new ReworkInData();
        }
    }

    public class ReworkInData
    {
        [XmlElement( "FormHead" )]
        public ReworkInHead FormHead;
        [XmlElement( "FormBody" )]
        public ReworkInBody FormBody;

        public ReworkInData()
        {
            FormHead = new ReworkInHead();
            FormBody = new ReworkInBody();
        }
    }

    public class ReworkInHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public ReworkInHeadRecord RecordList;

        public ReworkInHead()
        {
            TableName = string.Empty;
            RecordList = new ReworkInHeadRecord();
        }
    }

    public class ReworkInBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "ReworkType" )]
        public string ReworkType;
        [XmlElement( "RecordList")]
        public List<ReworkInBodyRecord> RecordList;

        public ReworkInBody()
        {
            TableName = string.Empty;
            ReworkType = string.Empty;
            RecordList = new List<ReworkInBodyRecord>();
        }
    }

    public class ReworkInHeadRecord
    {
        public ReworkInHeadRecord()
        {
        }
    }

    public class ReworkInBodyRecord
    {
        [XmlElement( "Equiptment" )]
        public string Equipment;
        [XmlElement( "Moid" )]
        public string MoId;
        [XmlElement( "Opseq" )]
        public string OpSeq;
        [XmlElement( "Opid" )]
        public string OpId;
        [XmlElement( "Wsid" )]
        public string WsId;
        [XmlElement( "Qty" )]
        public string Qty;
        [XmlElement( "UpdateTime" )]
        public string UpdateTime;
        [XmlElement( "ReworkException" )]
        public string ReworkException;
        [XmlElement( "ReworkWorkTime" )]
        public string ReworkWorkTime;
        [XmlElement( "Exception" )]
        public ReworkInException Exception;
    
        public ReworkInBodyRecord()
        {
            Equipment = string.Empty;
            MoId = string.Empty;
            OpSeq = string.Empty;
            OpId = string.Empty;
            WsId = string.Empty;
            Qty = string.Empty;
            UpdateTime = string.Empty;
            ReworkException = string.Empty;
            ReworkWorkTime = string.Empty;
            Exception = new ReworkInException();
        }
    }

    public class ReworkInException
    {
        [XmlElement( "Record" )]
        public List<ReworkInExceptionRecord> ExRecordList;

        public ReworkInException()
        {
            ExRecordList = new List<ReworkInExceptionRecord>();
        }
    }

    public class ReworkInExceptionRecord
    {
        [XmlElement( "Code" )]
        public string Code;
        [XmlElement( "Qty" )]
        public string Qty;

        public ReworkInExceptionRecord()
        {
            Code = string.Empty;
            Qty = string.Empty;
        }
    }
}