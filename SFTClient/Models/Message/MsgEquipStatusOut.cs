﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgEquipStatusOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public EquipStatusOutData Data;

        public MsgEquipStatusOut()
        {
            Data = new EquipStatusOutData();
        }
    }

    public class EquipStatusOutData
    {
         [XmlElement( "RecordList" )]
        public EquipStatusDataRecord RecodeList;

        public EquipStatusOutData()
        {
            RecodeList = new EquipStatusDataRecord();
        }
    }

    public class EquipStatusDataRecord
    {
        [XmlElement( "Status" )]
        public string Status;

        public EquipStatusDataRecord()
        {
            Status = string.Empty;
        }
    }
}