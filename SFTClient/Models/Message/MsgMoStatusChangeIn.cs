﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgMoStatusChangeIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public MoStatusChangeInData Data;

        public MsgMoStatusChangeIn()
        {
            base.DoAction = "060";
            base.DoCase = "020";
            Data = new MoStatusChangeInData();
        }
    }

    public class MoStatusChangeInData
    {
        [XmlElement( "FormHead" )]
        public MoStatusChangeHead FormHead;
        [XmlElement( "FormBody" )]
        public MoStatusChangeBody FormBody;

        public MoStatusChangeInData()
        {
            FormHead = new MoStatusChangeHead();
            FormBody = new MoStatusChangeBody();
        }
    }

    public class MoStatusChangeHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public MoStatusChangeHeadRecord RecordList;

        public MoStatusChangeHead()
        {
            TableName = string.Empty;
            RecordList = new MoStatusChangeHeadRecord();
        }
    }

    public class MoStatusChangeBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public List<MoStatusChangeBodyRecord> RecordList;

        public MoStatusChangeBody()
        {
            TableName = string.Empty;
            RecordList = new List<MoStatusChangeBodyRecord>();
        }
    }

    public class MoStatusChangeHeadRecord
    {
        [XmlElement( "Status" )]
        public string Status;
        [XmlElement( "ChangeType" )]
        public string ChangeType;
        [XmlElement( "Reason" )]
        public string Reason;
      
        public MoStatusChangeHeadRecord()
        {
            Status = string.Empty;
            ChangeType = string.Empty;
            Reason = string.Empty;
        }
    }

    public class MoStatusChangeBodyRecord
    {
        [XmlElement( "Equiptment" )]
        public string Equipment;
        [XmlElement( "Moid" )]
        public string MoId;
        [XmlElement( "Opseq" )]
        public string OpSeq;
        [XmlElement( "Opid" )]
        public string OpId;
        [XmlElement( "Wsid" )]
        public string WsId;

        public MoStatusChangeBodyRecord()
        {
            Equipment = string.Empty;
            MoId = string.Empty;
            OpSeq = string.Empty;
            OpId = string.Empty;
            WsId = string.Empty;
        }
    }
}