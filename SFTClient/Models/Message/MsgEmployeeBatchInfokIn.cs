﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgEmployeeBatchInfoIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public EmployeeBatchInfoInData Data;

        public MsgEmployeeBatchInfoIn()
        {
            base.DoAction = "070";
            base.DoCase = "030";
            Data = new EmployeeBatchInfoInData();
        }
    }

    public class EmployeeBatchInfoInData
    {
        [XmlElement( "FormHead" )]
        public EmployeeBatchInfoHead FormHead;
        [XmlElement( "FormBody" )]
        public EmployeeBatchInfoBody FormBody;

        public EmployeeBatchInfoInData()
        {
            FormHead = new EmployeeBatchInfoHead();
            FormBody = new EmployeeBatchInfoBody();
        }
    }

    public class EmployeeBatchInfoHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public EmployeeBatchInfoHeadRecord RecordList;

        public EmployeeBatchInfoHead()
        {
            TableName = string.Empty;
            RecordList = new EmployeeBatchInfoHeadRecord();
        }
    }

    public class EmployeeBatchInfoBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public EmployeeBatchInfoBodyRecord RecordList;

        public EmployeeBatchInfoBody()
        {
            TableName = string.Empty;
            RecordList = new EmployeeBatchInfoBodyRecord();
        }
    }

    public class EmployeeBatchInfoHeadRecord
    {
        public EmployeeBatchInfoHeadRecord()
        {
        }
    }

    public class EmployeeBatchInfoBodyRecord
    {
        public EmployeeBatchInfoBodyRecord()
        {
        }
    }
}