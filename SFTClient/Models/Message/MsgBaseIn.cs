﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.ComponentModel;

namespace SFTClient
{
    public class MsgBaseIn
    {
        [XmlElement( "Companyid" )]
        public string CompanyId;
        [XmlElement( "Userid" )]
        public string UserId;
        [XmlElement( "DoAction" )]
        public string DoAction;
        [XmlElement( "Docase" )]
        public string DoCase;
        [XmlElement( "Locale" )]
        public string Locale;
   
        public MsgBaseIn()
        {
            CompanyId = string.Empty;
            UserId = string.Empty;
            DoAction = string.Empty;
            DoCase = string.Empty;
            Locale = string.Empty;
        }
    }
}