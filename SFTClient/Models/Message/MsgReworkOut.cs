﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgReworkOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public ReworkOutData Data;

        public MsgReworkOut()
        {
            Data = new ReworkOutData();
        }
    }

    public class ReworkOutData
    {
        [XmlElement( "RecordList" )]
        public ReworkOutDataRecord RecordList;

        public ReworkOutData()
        {
            RecordList = new ReworkOutDataRecord();
        }
    }

    public class ReworkOutDataRecord
    {
        public ReworkOutDataRecord()
        {
        }
    }
}