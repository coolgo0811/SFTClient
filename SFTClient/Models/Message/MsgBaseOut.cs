﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.ComponentModel;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgBaseOut
    {
        [XmlElement( "Companyid" )]
        public string CompanyId;
        [XmlElement( "Userid" )]
        public string UserId;
        [XmlElement( "DoAction" )]
        public string DoAction;
        [XmlElement( "Docase" )]
        public string DoCase;
        [XmlElement( "Result" )]
        public string Result;
        [XmlElement( "Description" )]
        public string Description;
        [XmlElement( "Exception" )]
        public ExceptionInfo Exception;
        [XmlElement( "Message" )]
        public string Message;

        public MsgBaseOut()
        {
            CompanyId = string.Empty;
            UserId = string.Empty;
            DoAction = string.Empty;
            DoCase = string.Empty;
            Result = Const.FAIL;
            Description = string.Empty;
            Exception = new ExceptionInfo();
            Message = string.Empty;
        }
    }


    public class ExceptionInfo
    {
        [XmlElement( "Code" )]
        public string Code;
        [XmlElement( "Sysmsg" )]
        public string SysMsg;
        [XmlElement( "Mesmsg" )]
        public string MesMsg;
        [XmlElement( "Stack" )]
        public string Stack;

        public ExceptionInfo()
        {
            Code = string.Empty;
            SysMsg = ServiceMsg.DEFAULT_SYSMSG;
            MesMsg = ServiceMsg.DEFAULT_SYSMSG;
            Stack = string.Empty;
        }
    }
}