﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgEquipStatusChangeIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public EquipStatusChangeInData Data;

        public MsgEquipStatusChangeIn()
        {
            base.DoAction = "060";
            base.DoCase = "010";
            Data = new EquipStatusChangeInData();
        }
    }

    public class EquipStatusChangeInData
    {
        [XmlElement( "FormHead" )]
        public EquipStatusChangeHead FormHead;
        [XmlElement( "FormBody" )]
        public EquipStatusChangeBody FormBody;

        public EquipStatusChangeInData()
        {
            FormHead = new EquipStatusChangeHead();
            FormBody = new EquipStatusChangeBody();
        }
    }

    public class EquipStatusChangeHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public EquipStatusChangeHeadRecord RecordList;

        public EquipStatusChangeHead()
        {
            TableName = string.Empty;
            RecordList = new EquipStatusChangeHeadRecord();
        }
    }

    public class EquipStatusChangeBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public EquipStatusChangeBodyRecord RecordList;

        public EquipStatusChangeBody()
        {
            TableName = string.Empty;
            RecordList = new EquipStatusChangeBodyRecord();
        }
    }

    public class EquipStatusChangeHeadRecord
    {
        [XmlElement( "Equiptment" )]
        public string Equipment;
        [XmlElement( "Status" )]
        public string Status;
      
        public EquipStatusChangeHeadRecord()
        {
            Equipment = string.Empty;
            Status = string.Empty;
        }
    }

    public class EquipStatusChangeBodyRecord
    {
        public EquipStatusChangeBodyRecord()
        {
        }
    }
}