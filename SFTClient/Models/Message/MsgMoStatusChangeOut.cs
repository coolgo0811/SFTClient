﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgMoStatusChangeOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public MoStatusChangeOutData Data;

        public MsgMoStatusChangeOut()
        {
            Data = new MoStatusChangeOutData();
        }
    }

    public class MoStatusChangeOutData
    {
         [XmlElement( "RecordList" )]
        public MoStatusChangeDataRecord RecodeList;

        public MoStatusChangeOutData()
        {
            RecodeList = new MoStatusChangeDataRecord();
        }
    }

    public class MoStatusChangeDataRecord
    {
        [XmlElement( "Moid" )]
        public string MoId;
        [XmlElement( "Opseq" )]
        public string OpSeq;
        [XmlElement( "Opid" )]
        public string OpId;
        [XmlElement( "Wsid" )]
        public string WsId;
        [XmlElement( "Result" )]
        public string Result;
        [XmlElement( "Reason" )]
        public string Reason;

        public MoStatusChangeDataRecord()
        {
            MoId = string.Empty;
            OpSeq = string.Empty;
            OpId = string.Empty;
            WsId = string.Empty;
            Result = string.Empty;
            Reason = string.Empty;
        }
    }
}