﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgPrecheckOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public PrecheckOutData Data;

        public MsgPrecheckOut()
        {
            Data = new PrecheckOutData();
        }
    }

    public class PrecheckOutData
    {
        [XmlElement( "RecordList" )]
        public List<PrecheckDataRecord> RecodeList;

        public PrecheckOutData()
        {
            RecodeList = new List<PrecheckDataRecord>();
        }
    }

    public class PrecheckDataRecord
    {
        [XmlElement( "checkType" )]
        public string CheckType;
        [XmlElement( "checkMsg" )]
        public string CheckMsg;

        public PrecheckDataRecord()
        {
            CheckType = string.Empty;
            CheckMsg = string.Empty;
        }
    }
}