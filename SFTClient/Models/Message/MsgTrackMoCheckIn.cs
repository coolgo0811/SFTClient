﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgTrackMoCheckIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public TrackMoCheckInData Data;

        public MsgTrackMoCheckIn()
        {
            base.DoAction = "040";
            base.DoCase = "014";
            Data = new TrackMoCheckInData();
        }
    }

    public class TrackMoCheckInData
    {
        [XmlElement( "FormHead" )]
        public TrackMoCheckHead FormHead;
        [XmlElement( "FormBody" )]
        public TrackMoCheckBody FormBody;

        public TrackMoCheckInData()
        {
            FormHead = new TrackMoCheckHead();
            FormBody = new TrackMoCheckBody();
        }
    }

    public class TrackMoCheckHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public TrackMoCheckHeadRecord RecordList;

        public TrackMoCheckHead()
        {
            TableName = string.Empty;
            RecordList = new TrackMoCheckHeadRecord();
        }
    }

    public class TrackMoCheckBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public TrackMoCheckBodyRecord RecordList;

        public TrackMoCheckBody()
        {
            TableName = string.Empty;
            RecordList = new TrackMoCheckBodyRecord();
        }
    }

    public class TrackMoCheckHeadRecord
    {
        public TrackMoCheckHeadRecord()
        {
        }
    }

    public class TrackMoCheckBodyRecord
    {
        [XmlElement( "Equiptment" )]
        public string Equipment;
        [XmlElement( "Moid" )]
        public string MoId;
        [XmlElement( "Opseq" )]
        public string OpSeq;
        [XmlElement( "Opid" )]
        public string OpId;
        [XmlElement( "Wsid" )]
        public string WsId;

        public TrackMoCheckBodyRecord()
        {
            Equipment = string.Empty;
            MoId = string.Empty;
            OpSeq = string.Empty;
            OpId = string.Empty;
            WsId = string.Empty;
        }
    }
}