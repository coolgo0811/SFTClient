﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgEmployeeBatchInfoOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public EmployeeBatchInfoOutData Data;

        public MsgEmployeeBatchInfoOut()
        {
            Data = new EmployeeBatchInfoOutData();
        }
    }

    public class EmployeeBatchInfoOutData
    {
         [XmlElement( "RecordList" )]
        public List<EmployeeBatchInfoDataRecord> RecodeList;

        public EmployeeBatchInfoOutData()
        {
            RecodeList = new List<EmployeeBatchInfoDataRecord>();
        }
    }

    public class EmployeeBatchInfoDataRecord
    {
        [XmlElement( "Code" )]
        public string Code;
        [XmlElement( "Name" )]
        public string Name;

        public EmployeeBatchInfoDataRecord()
        {
            Code = string.Empty;
            Name = string.Empty;
        }
    }
}