﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgTrackinOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public TrackinOutData Data;

        public MsgTrackinOut()
        {
            Data = new TrackinOutData();
        }
    }

    public class TrackinOutData
    {
        [XmlElement( "RecordList" )]
        public TrackinOutDataRecord RecordList;

        public TrackinOutData()
        {
            RecordList = new TrackinOutDataRecord();
        }
    }

    public class TrackinOutDataRecord
    {
        public TrackinOutDataRecord()
        {
        }
    }
}