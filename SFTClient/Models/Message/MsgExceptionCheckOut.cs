﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgExceptionCheckOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public ExceptionCheckOutData Data;

        public MsgExceptionCheckOut()
        {
            Data = new ExceptionCheckOutData();
        }
    }

    public class ExceptionCheckOutData
    {
         [XmlElement( "RecordList" )]
        public ExceptionCheckDataRecord RecodeList;

        public ExceptionCheckOutData()
        {
            RecodeList = new ExceptionCheckDataRecord();
        }
    }

    public class ExceptionCheckDataRecord
    {
        [XmlElement( "Code" )]
        public string Code;
        [XmlElement( "Type" )]
        public string Type;
        [XmlElement( "Name" )]
        public string Name;

        public ExceptionCheckDataRecord()
        {
            Code = string.Empty;
            Type = string.Empty;
            Name = string.Empty;
        }
    }
}