﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgEmployeeCheckOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public EmployeeCheckOutData Data;

        public MsgEmployeeCheckOut()
        {
            Data = new EmployeeCheckOutData();
        }
    }

    public class EmployeeCheckOutData
    {
         [XmlElement( "RecordList" )]
        public EmployeeCheckDataRecord RecodeList;

        public EmployeeCheckOutData()
        {
            RecodeList = new EmployeeCheckDataRecord();
        }
    }

    public class EmployeeCheckDataRecord
    {
        [XmlElement( "Code" )]
        public string Code;
        [XmlElement( "Name" )]
        public string Name;

        public EmployeeCheckDataRecord()
        {
            Code = string.Empty;
            Name = string.Empty;
        }
    }
}