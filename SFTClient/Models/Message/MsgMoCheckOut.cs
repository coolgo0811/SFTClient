﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgMoCheckOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public MoCheckOutData Data;

        public MsgMoCheckOut()
        {
            Data = new MoCheckOutData();
        }
    }

    public class MoCheckOutData
    {
         [XmlElement( "RecordList" )]
        public MoCheckDataRecord RecodeList;

        public MoCheckOutData()
        {
            RecodeList = new MoCheckDataRecord();
        }
    }

    public class MoCheckDataRecord
    {
        [XmlElement( "checkQtyType" )]
        public string CheckQtyType;
        [XmlElement( "checkQty" )]
        public string CheckQty;
        [XmlElement( "beforeOPSEQ" )]
        public string BeforeOpSeq;
        [XmlElement( "QTY" )]
        public string Qty;
        [XmlElement( "WORKTIME" )]
        public string WorkTime;
        [XmlElement( "MANWORKTIME" )]
        public string ManWorkTime;
        [XmlElement( "NOWDATETIME" )]
        public string NowDateTime;

        public MoCheckDataRecord()
        {
            CheckQtyType = string.Empty;
            CheckQty = string.Empty;
            BeforeOpSeq = string.Empty;
            Qty = string.Empty;
            WorkTime = string.Empty;
            ManWorkTime = string.Empty;
            NowDateTime = string.Empty;
        }
    }
}