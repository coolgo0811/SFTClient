﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgExceptionCheckIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public ExceptionCheckInData Data;

        public MsgExceptionCheckIn()
        {
            base.DoAction = "040";
            base.DoCase = "031";
            Data = new ExceptionCheckInData();
        }
    }

    public class ExceptionCheckInData
    {
        [XmlElement( "FormHead" )]
        public ExceptionCheckHead FormHead;
        [XmlElement( "FormBody" )]
        public ExceptionCheckBody FormBody;

        public ExceptionCheckInData()
        {
            FormHead = new ExceptionCheckHead();
            FormBody = new ExceptionCheckBody();
        }
    }

    public class ExceptionCheckHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public ExceptionCheckHeadRecord RecordList;

        public ExceptionCheckHead()
        {
            TableName = string.Empty;
            RecordList = new ExceptionCheckHeadRecord();
        }
    }

    public class ExceptionCheckBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public ExceptionCheckBodyRecord RecordList;

        public ExceptionCheckBody()
        {
            TableName = string.Empty;
            RecordList = new ExceptionCheckBodyRecord();
        }
    }

    public class ExceptionCheckHeadRecord
    {
        public ExceptionCheckHeadRecord()
        {
        }
    }

    public class ExceptionCheckBodyRecord
    {
        [XmlElement( "CheckType" )]
        public string CheckType;
        [XmlElement( "Code" )]
        public string Code;
      
        public ExceptionCheckBodyRecord()
        {
            CheckType = string.Empty;
            Code = string.Empty;
        }
    }
}