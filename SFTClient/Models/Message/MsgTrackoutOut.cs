﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgTrackoutOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public TrackoutOutData Data;

        public MsgTrackoutOut()
        {
            Data = new TrackoutOutData();
        }
    }

    public class TrackoutOutData
    {
        [XmlElement( "RecordList" )]
        public TrackoutOutDataRecord RecordList;

        public TrackoutOutData()
        {
            RecordList = new TrackoutOutDataRecord();
        }
    }

    public class TrackoutOutDataRecord
    {
        public TrackoutOutDataRecord()
        {
        }
    }
}