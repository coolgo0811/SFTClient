﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgEmployeeCheckIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public EmployeeCheckInData Data;

        public MsgEmployeeCheckIn()
        {
            base.DoAction = "040";
            base.DoCase = "030";
            Data = new EmployeeCheckInData();
        }
    }

    public class EmployeeCheckInData
    {
        [XmlElement( "FormHead" )]
        public EmployeeCheckHead FormHead;
        [XmlElement( "FormBody" )]
        public EmployeeCheckBody FormBody;

        public EmployeeCheckInData()
        {
            FormHead = new EmployeeCheckHead();
            FormBody = new EmployeeCheckBody();
        }
    }

    public class EmployeeCheckHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public EmployeeCheckHeadRecord RecordList;

        public EmployeeCheckHead()
        {
            TableName = string.Empty;
            RecordList = new EmployeeCheckHeadRecord();
        }
    }

    public class EmployeeCheckBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public EmployeeCheckBodyRecord RecordList;

        public EmployeeCheckBody()
        {
            TableName = string.Empty;
            RecordList = new EmployeeCheckBodyRecord();
        }
    }

    public class EmployeeCheckHeadRecord
    {
        public EmployeeCheckHeadRecord()
        {
        }
    }

    public class EmployeeCheckBodyRecord
    {
        [XmlElement( "Code" )]
        public string Code;

        public EmployeeCheckBodyRecord()
        {
            Code = string.Empty;
        }
    }
}