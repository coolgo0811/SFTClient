﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgMoStatusOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public MoStatusOutData Data;

        public MsgMoStatusOut()
        {
            Data = new MoStatusOutData();
        }
    }

    public class MoStatusOutData
    {
         [XmlElement( "RecordList" )]
        public MoStatusDataRecord RecodeList;

        public MoStatusOutData()
        {
            RecodeList = new MoStatusDataRecord();
        }
    }

    public class MoStatusDataRecord
    {
        [XmlElement( "Status" )]
        public string Status;

        public MoStatusDataRecord()
        {
            Status = string.Empty;
        }
    }
}