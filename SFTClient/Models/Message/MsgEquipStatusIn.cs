﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgEquipStatusIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public EquipStatusInData Data;

        public MsgEquipStatusIn()
        {
            base.DoAction = "070";
            base.DoCase = "010";
            Data = new EquipStatusInData();
        }
    }

    public class EquipStatusInData
    {
        [XmlElement( "FormHead" )]
        public EquipStatusHead FormHead;
        [XmlElement( "FormBody" )]
        public EquipStatusBody FormBody;

        public EquipStatusInData()
        {
            FormHead = new EquipStatusHead();
            FormBody = new EquipStatusBody();
        }
    }

    public class EquipStatusHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public EquipStatusHeadRecord RecordList;

        public EquipStatusHead()
        {
            TableName = string.Empty;
            RecordList = new EquipStatusHeadRecord();
        }
    }

    public class EquipStatusBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public EquipStatusBodyRecord RecordList;

        public EquipStatusBody()
        {
            TableName = string.Empty;
            RecordList = new EquipStatusBodyRecord();
        }
    }

    public class EquipStatusHeadRecord
    {
        [XmlElement( "Equiptment" )]
        public string Equipment;

        public EquipStatusHeadRecord()
        {
            Equipment = string.Empty;
        }
    }

    public class EquipStatusBodyRecord
    {
        public EquipStatusBodyRecord()
        {
        }
    }
}