﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgTrackinIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public TrackinInData Data;

        public MsgTrackinIn()
        {
            base.DoAction = "020";
            base.DoCase = "030";
            Data = new TrackinInData();
        }
    }

    public class TrackinInData
    {
        [XmlElement( "FormHead" )]
        public TrackinInHead FormHead;
        [XmlElement( "FormBody" )]
        public TrackinInBody FormBody;

        public TrackinInData()
        {
            FormHead = new TrackinInHead();
            FormBody = new TrackinInBody();
        }
    }

    public class TrackinInHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public TrackinInHeadRecord RecordList;

        public TrackinInHead()
        {
            TableName = string.Empty;
            RecordList = new TrackinInHeadRecord();
        }
    }

    public class TrackinInBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public List<TrackinInBodyRecord> RecordList;

        public TrackinInBody()
        {
            TableName = string.Empty;
            RecordList = new List<TrackinInBodyRecord>();
        }
    }

    public class TrackinInHeadRecord
    {
        public TrackinInHeadRecord()
        {
        }
    }

    public class TrackinInBodyRecord
    {
        [XmlElement( "UpdateTime" )]
        public string UpdateTime;
        [XmlElement( "Equiptment" )]
        public string Equipment;
        [XmlElement( "Moid" )]
        public string MoId;
        [XmlElement( "Opseq" )]
        public string OpSeq;
        [XmlElement( "Opid" )]
        public string OpId;
        [XmlElement( "Wsid" )]
        public string WsId;
        [XmlElement( "Qty" )]
        public string Qty;

        public TrackinInBodyRecord()
        {
            UpdateTime = string.Empty;
            Equipment = string.Empty;
            MoId = string.Empty;
            OpSeq = string.Empty;
            OpId = string.Empty;
            WsId = string.Empty;
            Qty = string.Empty;
        }
    }
}