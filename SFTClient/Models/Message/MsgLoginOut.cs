﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgLoginOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public LoginOutData Data;

        public MsgLoginOut()
        {
            Data = new LoginOutData();
        }
    }

    public class LoginOutData
    {
        [XmlElement( "RecordList" )]
        public LoginOutDataRecord RecordList;

        public LoginOutData()
        {
            RecordList = new LoginOutDataRecord();
        }
    }

    public class LoginOutDataRecord
    {
        public LoginOutDataRecord()
        {
        }
    }
}