﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgEquipStatusChangeOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public EquipStatusChangeOutData Data;

        public MsgEquipStatusChangeOut()
        {
            Data = new EquipStatusChangeOutData();
        }
    }

    public class EquipStatusChangeOutData
    {
         [XmlElement( "RecordList" )]
        public EquipStatusChangeDataRecord RecodeList;

        public EquipStatusChangeOutData()
        {
            RecodeList = new EquipStatusChangeDataRecord();
        }
    }

    public class EquipStatusChangeDataRecord
    {
        public EquipStatusChangeDataRecord()
        {
        }
    }
}