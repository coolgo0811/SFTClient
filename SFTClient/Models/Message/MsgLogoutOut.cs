﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgLogoutOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public LogoutOutData Data;

        public MsgLogoutOut()
        {
            Data = new LogoutOutData();
        }
    }

    public class LogoutOutData
    {
        [XmlElement( "RecordList" )]
        public LogoutOutDataRecord RecordList;

        public LogoutOutData()
        {
            RecordList = new LogoutOutDataRecord();
        }
    }

    public class LogoutOutDataRecord
    {
        public LogoutOutDataRecord()
        {
        }
    }
}