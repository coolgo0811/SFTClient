﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgMoCheckIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public MoCheckInData Data;

        public MsgMoCheckIn()
        {
            base.DoAction = "040";
            base.DoCase = "013";
            Data = new MoCheckInData();
        }
    }

    public class MoCheckInData
    {
        [XmlElement( "FormHead" )]
        public MoCheckHead FormHead;
        [XmlElement( "FormBody" )]
        public MoCheckBody FormBody;

        public MoCheckInData()
        {
            FormHead = new MoCheckHead();
            FormBody = new MoCheckBody();
        }
    }

    public class MoCheckHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public MoCheckHeadRecord RecordList;

        public MoCheckHead()
        {
            TableName = string.Empty;
            RecordList = new MoCheckHeadRecord();
        }
    }

    public class MoCheckBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public MoCheckBodyRecord RecordList;

        public MoCheckBody()
        {
            TableName = string.Empty;
            RecordList = new MoCheckBodyRecord();
        }
    }

    public class MoCheckHeadRecord
    {
        public MoCheckHeadRecord()
        {
        }
    }

    public class MoCheckBodyRecord
    {
        [XmlElement( "Equiptment" )]
        public string Equipment;
        [XmlElement( "Moid" )]
        public string MoId;
        [XmlElement( "Opseq" )]
        public string OpSeq;
        [XmlElement( "Opid" )]
        public string OpId;
        [XmlElement( "Wsid" )]
        public string WsId;
        [XmlElement( "checkQtyType" )]
        public string CheckQtyType;

        public MoCheckBodyRecord()
        {
            Equipment = string.Empty;
            MoId = string.Empty;
            OpSeq = string.Empty;
            OpId = string.Empty;
            WsId = string.Empty;
            CheckQtyType = string.Empty;
        }
    }
}