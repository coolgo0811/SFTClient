﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgLoginIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public LoginInData Data;

        public MsgLoginIn()
        {
            base.DoAction = "010";
            Data = new LoginInData();
        }
    }

    public class LoginInData
    {
        [XmlElement( "FormHead" )]
        public LoginInHead FormHead;
        [XmlElement( "FormBody" )]
        public LoginInBody FormBody;

        public LoginInData()
        {
            FormHead = new LoginInHead();
            FormBody = new LoginInBody();
        }
    }

    public class LoginInHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public LoginInHeadRecord RecordList;

        public LoginInHead()
        {
            TableName = string.Empty;
            RecordList = new LoginInHeadRecord();
        }
    }

    public class LoginInBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public LoginInBodyRecord RecordList;

        public LoginInBody()
        {
            TableName = string.Empty;
            RecordList = new LoginInBodyRecord();
        }
    }

    public class LoginInHeadRecord
    {
        [XmlElement( "EqMode" )]
        public string EqMode;
        [XmlElement( "Password" )]
        public string Password;
        [XmlElement( "IP" )]
        public string IP;

        public LoginInHeadRecord()
        {
            EqMode = string.Empty;
            Password = string.Empty;
            IP = string.Empty;
        }
    }

    public class LoginInBodyRecord
    {
        public LoginInBodyRecord()
        {
        }
    }
}