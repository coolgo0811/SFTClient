﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgPrecheckIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public PrecheckInData Data;

        public MsgPrecheckIn()
        {
            base.DoAction = "040";
            base.DoCase = "020";
            Data = new PrecheckInData();
        }
    }

    public class PrecheckInData
    {
        [XmlElement( "FormHead" )]
        public PrecheckInHead FormHead;
        [XmlElement( "FormBody" )]
        public PrecheckInBody FormBody;

        public PrecheckInData()
        {
            FormHead = new PrecheckInHead();
            FormBody = new PrecheckInBody();
        }
    }

    public class PrecheckInHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public PrecheckInHeadRecord RecordList;

        public PrecheckInHead()
        {
            TableName = string.Empty;
            RecordList = new PrecheckInHeadRecord();
        }
    }

    public class PrecheckInBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "checkType" )]
        public string CheckType;
        [XmlElement( "RecordList" )]
        public List<PrecheckInBodyRecord> RecordList;

        public PrecheckInBody()
        {
            TableName = string.Empty;
            CheckType = string.Empty;
            RecordList = new List<PrecheckInBodyRecord>();
        }
    }

    public class PrecheckInHeadRecord
    {
        public PrecheckInHeadRecord()
        {
        }
    }

    public class PrecheckInBodyRecord
    {
        [XmlElement( "Equiptment" )]
        public string Equipment;
        [XmlElement( "Moid" )]
        public string MoId;
        [XmlElement( "Opseq" )]
        public string OpSeq;
        [XmlElement( "Opid" )]
        public string OpId;
        [XmlElement( "Wsid" )]
        public string WsId;
        [XmlElement( "Qty" )]
        public string Qty;

        public PrecheckInBodyRecord()
        {
            Equipment = string.Empty;
            MoId = string.Empty;
            OpSeq = string.Empty;
            OpId = string.Empty;
            WsId = string.Empty;
            Qty = string.Empty;
        }
    }
}