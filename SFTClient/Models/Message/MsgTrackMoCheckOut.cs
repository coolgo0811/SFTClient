﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgTrackMoCheckOut : MsgBaseOut
    {
        [XmlElement( "Data" )]
        public TrackMoCheckOutData Data;

        public MsgTrackMoCheckOut()
        {
            Data = new TrackMoCheckOutData();
        }
    }

    public class TrackMoCheckOutData
    {
         [XmlElement( "RecordList" )]
        public TrackMoCheckDataRecord RecodeList;

        public TrackMoCheckOutData()
        {
            RecodeList = new TrackMoCheckDataRecord();
        }
    }

    public class TrackMoCheckDataRecord
    {
        [XmlElement( "checkInQty" )]
        public string CheckInQty;
        [XmlElement( "checkOutQty" )]
        public string CheckOutQty;
        [XmlElement( "beforeOPSEQ" )]
        public string BeforeOpSeq;
        [XmlElement( "QTY" )]
        public string Qty;
        [XmlElement( "WORKTIME" )]
        public string WorkTime;
        [XmlElement( "MANWORKTIME" )]
        public string ManWorkTime;
        [XmlElement( "NOWDATETIME" )]
        public string NowDateTime;

        public TrackMoCheckDataRecord()
        {
            CheckInQty = string.Empty;
            CheckOutQty = string.Empty;
            BeforeOpSeq = string.Empty;
            Qty = string.Empty;
            WorkTime = string.Empty;
            ManWorkTime = string.Empty;
            NowDateTime = string.Empty;
        }
    }
}