﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgMoStatusIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public MoStatusInData Data;

        public MsgMoStatusIn()
        {
            base.DoAction = "070";
            base.DoCase = "020";
            Data = new MoStatusInData();
        }
    }

    public class MoStatusInData
    {
        [XmlElement( "FormHead" )]
        public MoStatusHead FormHead;
        [XmlElement( "FormBody" )]
        public MoStatusBody FormBody;

        public MoStatusInData()
        {
            FormHead = new MoStatusHead();
            FormBody = new MoStatusBody();
        }
    }

    public class MoStatusHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public MoStatusHeadRecord RecordList;

        public MoStatusHead()
        {
            TableName = string.Empty;
            RecordList = new MoStatusHeadRecord();
        }
    }

    public class MoStatusBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public MoStatusBodyRecord RecordList;

        public MoStatusBody()
        {
            TableName = string.Empty;
            RecordList = new MoStatusBodyRecord();
        }
    }

    public class MoStatusHeadRecord
    {
        [XmlElement( "Equiptment" )]
        public string Equipment;
        [XmlElement( "Moid" )]
        public string MoId;
        [XmlElement( "Opseq" )]
        public string OpSeq;
        [XmlElement( "Opid" )]
        public string OpId;
        [XmlElement( "Wsid" )]
        public string WsId;

        public MoStatusHeadRecord()
        {
            Equipment = string.Empty;
            MoId = string.Empty;
            OpSeq = string.Empty;
            OpId = string.Empty;
            WsId = string.Empty;
        }
    }

    public class MoStatusBodyRecord
    {
        public MoStatusBodyRecord()
        {
        }
    }
}