﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgLogoutIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public LogoutInData Data;

        public MsgLogoutIn()
        {
            base.DoAction = "011";
            Data = new LogoutInData();
        }
    }

    public class LogoutInData
    {
        [XmlElement( "FormHead" )]
        public LogoutInHead FormHead;
        [XmlElement( "FormBody" )]
        public LogoutInBody FormBody;

        public LogoutInData()
        {
            FormHead = new LogoutInHead();
            FormBody = new LogoutInBody();
        }
    }

    public class LogoutInHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public LogoutInHeadRecord RecordList;

        public LogoutInHead()
        {
            TableName = string.Empty;
            RecordList = new LogoutInHeadRecord();
        }
    }

    public class LogoutInBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public LogoutInBodyRecord RecordList;

        public LogoutInBody()
        {
            TableName = string.Empty;
            RecordList = new LogoutInBodyRecord();
        }
    }

    public class LogoutInHeadRecord
    {
        [XmlElement( "EqMode" )]
        public string EqMode;
        [XmlElement( "IP" )]
        public string IP;

        public LogoutInHeadRecord()
        {
            EqMode = string.Empty;
            IP = string.Empty;
        }
    }

    public class LogoutInBodyRecord
    {
        public LogoutInBodyRecord()
        {
        }
    }
}