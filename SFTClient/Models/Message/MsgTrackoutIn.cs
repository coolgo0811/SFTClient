﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SFTClient
{
    [XmlRoot( "STD_IN" )]
    public class MsgTrackoutIn : MsgBaseIn
    {
        [XmlElement( "Data" )]
        public TrackoutInData Data;

        public MsgTrackoutIn()
        {
            base.DoAction = "030";
            base.DoCase = "030";
            Data = new TrackoutInData();
        }
    }

    public class TrackoutInData
    {
        [XmlElement( "FormHead" )]
        public TrackoutInHead FormHead;
        [XmlElement( "FormBody" )]
        public TrackoutInBody FormBody;

        public TrackoutInData()
        {
            FormHead = new TrackoutInHead();
            FormBody = new TrackoutInBody();
        }
    }

    public class TrackoutInHead
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "RecordList" )]
        public TrackoutInHeadRecord RecordList;

        public TrackoutInHead()
        {
            TableName = string.Empty;
            RecordList = new TrackoutInHeadRecord();
        }
    }

    public class TrackoutInBody
    {
        [XmlElement( "TableName" )]
        public string TableName;
        [XmlElement( "CheckOutMode" )]
        public string CheckOutMode;
        [XmlElement( "CheckOutType" )]
        public string CheckOutType;
        [XmlElement( "RecordList")]
        public List<TrackoutInBodyRecord> RecordList;

        public TrackoutInBody()
        {
            TableName = string.Empty;
            CheckOutMode = string.Empty;
            CheckOutType = string.Empty;
            RecordList = new List<TrackoutInBodyRecord>();
        }
    }

    public class TrackoutInHeadRecord
    {
        public TrackoutInHeadRecord()
        {
        }
    }

    public class TrackoutInBodyRecord
    {
        [XmlElement( "Equiptment" )]
        public string Equipment;
        [XmlElement( "Moid" )]
        public string MoId;
        [XmlElement( "Opseq" )]
        public string OpSeq;
        [XmlElement( "Opid" )]
        public string OpId;
        [XmlElement( "Wsid" )]
        public string WsId;
        [XmlElement( "Qty" )]
        public string Qty;
        [XmlElement( "UpdateOutTime" )]
        public string UpdateOutTime;
        [XmlElement( "Exception" )]
        public TrackoutInException Exception;
        [XmlElement( "ManWorkTime" )]
        public string ManWorkTime;
        [XmlElement( "WorkTime" )]
        public string WorkTime;
        [XmlElement( "Work" )]
        public TrackoutInWork Work;

        public TrackoutInBodyRecord()
        {
            UpdateOutTime = string.Empty;
            Equipment = string.Empty;
            MoId = string.Empty;
            OpSeq = string.Empty;
            OpId = string.Empty;
            WsId = string.Empty;
            Qty = string.Empty;
            Exception = new TrackoutInException();
            ManWorkTime = string.Empty;
            WorkTime = string.Empty;
            Work = new TrackoutInWork();
        }
    }

    public class TrackoutInException
    {
        [XmlElement( "Record" )]
        public List<TrackoutInExceptionRecord> ExRecordList;

        public TrackoutInException()
        {
            ExRecordList = new List<TrackoutInExceptionRecord>();
        }
    }

    public class TrackoutInExceptionRecord
    {
        [XmlElement( "Code" )]
        public string Code;
        [XmlElement( "Qty" )]
        public string Qty;

        public TrackoutInExceptionRecord()
        {
            Code = string.Empty;
            Qty = string.Empty;
        }
    }

    public class TrackoutInWork
    {
        [XmlElement( "Number" )]
        public string Number;
        [XmlElement( "Record" )]
        public List<TrackoutInWorkRecord> WorkRecordList;

        public TrackoutInWork()
        {
            Number = "1";   // default 1
            WorkRecordList = new List<TrackoutInWorkRecord>();
        }
    }

    public class TrackoutInWorkRecord
    {
        [XmlElement( "User" )]
        public string User;
        [XmlElement( "Times" )]
        public string Times;

        public TrackoutInWorkRecord()
        {
            User = string.Empty;
            Times = string.Empty;
        }
    }
}