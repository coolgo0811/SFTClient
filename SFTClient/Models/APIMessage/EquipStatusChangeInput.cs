﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class EquipStatusChangeInput : BaseInput
    {
        public string Equipment;
        public string Status;
    }
}