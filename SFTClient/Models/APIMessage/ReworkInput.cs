﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class ReworkInput : BaseInput
    {
        public string ReworkType;

        public List<ReworkRecord> RecordList;

        public ReworkInput()
        {
            RecordList = new List<ReworkRecord>();
        }
    }

    public class ReworkRecord
    {
        public string MoId;
        public string Qty;
        public string ReworkWorkTime;
        public List<ReworkExceptionRecord> Exception;

        public ReworkRecord()
        {
            Exception = new List<ReworkExceptionRecord>();
        }
    }

    public class ReworkExceptionRecord
    {
        public string Code;
        public string Qty;
    }
}