﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class PrecheckOutput : BaseOutput
    {
        public List<PrecheckOutRecord> RecordList;

        public PrecheckOutput()
        {
            RecordList = new List<PrecheckOutRecord>();
        }
    }

    public class PrecheckOutRecord
    {
        public string CheckType;
        public string CheckMsg;
    }
}