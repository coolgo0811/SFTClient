﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class MoStatusChangeOutput : BaseOutput
    {
        public string MoId;
        public string ChangeResult;
        public string ChangeReason;
    }
}