﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class SysSettingInput
    {
        public string LocalIP;
        public string LocalMask;
        public string LocalGateway;
        public bool IsDHCP;
        public string ServerIP;
        public string ServerPort;
        public string ServerTimeout;
        public string UserName;
        public string Password;
        public string AutoLogoutTime;
        public string CompanyId;
        public string Equipment;
        public int EquipDataCollection;
        public int SystemModel;

        public bool LiveUpdate;
        public bool EquipTrackinoutModel;
    }
}