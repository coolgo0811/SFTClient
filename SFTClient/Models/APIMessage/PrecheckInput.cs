﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class PrecheckInput : BaseInput
    {
        public string CheckType;
        public List<PrecheckInRecord> RecordList;

        public PrecheckInput()
        {
            RecordList = new List<PrecheckInRecord>();
        }
    }

    public class PrecheckInRecord
    {
        public string MoId;
        public string Qty;
    }
}