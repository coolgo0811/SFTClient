﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class MoStatusChangeInput : BaseInput
    {
        public string Status;
        public string ChangeType;
        public string Reason;

        public List<MoStatusChangeRecord> RecordList;

        public MoStatusChangeInput()
        {
            RecordList = new List<MoStatusChangeRecord>();
        }
    }

    public class MoStatusChangeRecord
    {
        public string MoId;

        public MoStatusChangeRecord()
        {
        }
    }
}