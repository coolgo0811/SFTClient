﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class TrackoutInput : BaseInput
    {
        public string CheckOutMode;
        public string CheckOutType;

        public List<TrackoutRecord> RecordList;

        public TrackoutInput()
        {
            RecordList = new List<TrackoutRecord>();
        }
    }

    public class TrackoutRecord
    {
        public string MoId;
        public string Qty;
        public List<TrackoutExceptionRecord> Exception;
        public string ManWorkTime;
        public string WorkTime;
        public List<TrackoutWorkRecord> Work;

        public TrackoutRecord()
        {
            Exception = new List<TrackoutExceptionRecord>();
            Work = new List<TrackoutWorkRecord>();
        }
    }

    public class TrackoutExceptionRecord
    {
        public string Code;
        public string Qty;
    }

    public class TrackoutWorkRecord
    {
        public string User;
        public string Times;
    }

}