﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class MoCheckOutput : BaseOutput
    {
        public string CheckQtyType;
        public string CheckQty;
        public string BeforeOpSeq;
        public string Qty;
        public string WorkTime; // equipment times
        public string ManWorkTime;  // man times
    }
}