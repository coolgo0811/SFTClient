﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class TrackinInput : BaseInput
    {
        public List<TrackinRecord> RecordList;

        public TrackinInput()
        {
            RecordList = new List<TrackinRecord>();
        }
    }

    public class TrackinRecord
    {
        public string MoId;
        public string Qty;
    }
}