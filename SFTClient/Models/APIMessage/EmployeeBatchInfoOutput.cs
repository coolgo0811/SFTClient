﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class EmployeeBatchInfoOutput : BaseOutput
    {
        public List<EmployeeBatchInfoRecord> RecordList;

        public EmployeeBatchInfoOutput()
        {
            RecordList = new List<EmployeeBatchInfoRecord>();
        }
    }

    public class EmployeeBatchInfoRecord
    {
        public string Code;
        public string Name;
    }
}