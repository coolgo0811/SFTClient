﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class TrackMoCheckOutput : BaseOutput
    {
        public string CheckInQty;
        public string CheckOutQty;
        public string BeforeOpSeq;
        public string Qty;
        public string WorkTime; // equipment times
        public string ManWorkTime;  // man times
    }
}