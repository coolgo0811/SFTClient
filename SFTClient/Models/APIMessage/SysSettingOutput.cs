﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class SysSettingOutput
    {
        public string LocalIP;
        public string LocalMask;
        public string LocalGateway;
        public bool IsDHCP;
        public string ServerIP;
        public string ServerPort;
        public string ServerTimeout;
        public string UserName;
        public string Password;
        public string AutoLogoutTime;
        public string CompanyId;
        public string Equipment;
        public int EquipDataCollection;
        public int SystemModel;

        // checkbox
        public bool LiveUpdate;
        public bool EquipTrackinoutModel;

        public string SysMsg;

        public SysSettingOutput()
        {
            LocalIP = string.Empty;
            LocalMask = string.Empty;
            LocalGateway = string.Empty;
            IsDHCP = false;
            ServerIP = string.Empty;
            ServerPort = string.Empty;
            ServerTimeout = string.Empty;
            UserName = string.Empty;
            Password = string.Empty;
            AutoLogoutTime = string.Empty;
            CompanyId = string.Empty;
            Equipment = string.Empty;
            EquipDataCollection = 0;
            SystemModel = 0;

            LiveUpdate = false;
            EquipTrackinoutModel = false;

            SysMsg = ServiceMsg.DEFAULT_SYSMSG;
        }
    }
}