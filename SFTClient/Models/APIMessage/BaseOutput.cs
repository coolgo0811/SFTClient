﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFTClient
{
    public class BaseOutput
    {
        public string Result;   // result: success or fail
        public string Code;     // result code
        public string SysMsg;   // system message
        public string MesMsg;   // mes message
        public string Stack;    // system error information

        public BaseOutput()
        {
            Result = Const.FAIL;
            SysMsg = ServiceMsg.DEFAULT_SYSMSG;
        }
    }
}